const dotenv = require('dotenv')

const isProduction = require('shared/lib/isProduction')

const dotEnvFilePath = isProduction
  ? './config/.env'
  : './config/.env.development'
dotenv.config({ path: dotEnvFilePath })

module.exports = {
  apps: [
    {
      env: {
        DB_URL: process.env.DB_URL,
        GA_TRACKING_ID: process.env.GA_TRACKING_ID,
        KOA_SECRET: process.env.KOA_SECRET,
        LOGENTRIES_BUNYAN_TOKEN: process.env.LOGENTRIES_BUNYAN_TOKEN,
        MAINTENANCE_MODE: process.env.MAINTENANCE_MODE,
        NODE_ENV: process.env.NODE_ENV,
        REDIS_HOST: process.env.REDIS_HOST,
        S3_ACCESS_KEY_ID: process.env.S3_ACCESS_KEY_ID,
        S3_BUCKET_DIR: process.env.S3_BUCKET_DIR,
        S3_BUCKET_NAME: process.env.S3_BUCKET_NAME,
        S3_BUCKET_REGION: process.env.S3_BUCKET_REGION,
        S3_SECRET_ACCESS_KEY: process.env.S3_SECRET_ACCESS_KEY,
        SENTRY_PROJECT_ID: process.env.SENTRY_PROJECT_ID,
        SENTRY_PUBLIC_KEY: process.env.SENTRY_PUBLIC_KEY,
        SENTRY_SECRET_KEY: process.env.SENTRY_SECRET_KEY,
        TWITTER_LINK: process.env.TWITTER_LINK,
      },
      ignore_watch: [
        'node_modules/.cache',
        'client',
        '.git',
        'nginx',
        'dist',
        'docker',
        'media',
      ],
      name: 'backend',
      script: './server/index.js',
    },
    {
      env_test: {
        DB_URL: 'mongodb://localhost:27017/test',
        NODE_ENV: 'test',
        REDIS_HOST: 'redis',
      },
      name: 'test',
      script: 'start npm run test',
      watch: false,
    },
  ],
}
