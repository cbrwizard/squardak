##
# A production dockerfile. Compiles frontend once, runs backend
# in the non-watch mode, copies all files from the local folder once.
# TODO: improve security by creating a different user.
# TODO: resolve an issue when images sometimes are created with '_#', which breaks the links
# TODO: try phusion/baseimage.
FROM debian:jessie

# Taken as latest
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 7.10.0

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Run updates and install deps.
# These should come together to ensure that packages are up-to-date together.
# TODO: remove python?
RUN \
  apt-get update && apt-get install -y -q --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    g++ \
    gcc \
    git \
    make \
    python2.7\
    sudo\
    curl\
    wget &&\
  rm -rf /var/lib/apt/lists/* &&\
  apt-get -y autoclean

# # allow installing mongodb tools
RUN sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv EA312927
RUN echo "deb http://repo.mongodb.org/apt/ubuntu xenial/mongodb-org/3.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-3.2.list
RUN apt-get update && apt-get install -y -q mongodb-org

# Create a dir for nvm.
RUN mkdir /usr/local/nvm

# Set an env variable for HOME.
RUN mkdir /home/app
ENV HOME=/home/app

# Install nvm with node and npm
# TODO: host this file locally.
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# Add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Create a working directory so it's usable later.
RUN mkdir -p $HOME/squardak

# Change current directory to a project's one.
WORKDIR $HOME/squardak

# Copy package-related files before others for optimization.
COPY package.json yarn.lock $HOME/squardak/

# Install yarn.
RUN curl -o- -L https://yarnpkg.com/install.sh | bash

# For production instead of using a volume copy all files at once.
ADD . $HOME/squardak

# Install production packages before copying other files for optimization.
# That path is needed because of the way we install yarn.
# TODO: try with --production
RUN $HOME/.yarn/bin/yarn install

# Install pm2 so we can run our application, webpack for assets compilation
RUN npm i -g pm2 webpack

# Compile frontend
# TODO: make it run here?
# RUN ["npm", "run", "frontend:production"]

# Run the script to start the production server.
CMD ["npm", "run", "backend:production"]

# Expose 3000 to the world.
EXPOSE 3000
