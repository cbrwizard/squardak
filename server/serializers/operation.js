import { pick } from 'ramda'

import { findByIds } from 'server/queries/squares'

const serializeOperation = async (operation) => {
  const squares = await findByIds(operation.squares)
  const squarePositions = squares.map(square => square.position)
  return {
    ...pick(['_user', 'affectedUsers'], operation),
    squarePositions,
  }
}

export default serializeOperation
