import { pick } from 'ramda'

const serializeSquare = square => pick(['column', 'position', 'row', '_user'], square)

export default serializeSquare
