import currentTurnsLeft from 'server/policies/currentTurnsLeft'
import squaresToCaptureMax from 'server/policies/squaresToCaptureMax'
import positionCameraAtSquarePosition
  from 'server/policies/positionCameraAtSquarePosition'
import { numberOfSquares } from '../queries/squares'

/**
 * Is responsible for returning the data which should be accessed by frontend
 * on game start.
 */
const serializeGameSession = async user => ({
  currentTurnsLeft: await currentTurnsLeft(user),
  positionCameraAtSquarePosition: await positionCameraAtSquarePosition(user),
  squaresCount: await numberOfSquares(),
  squaresToCaptureMax: await squaresToCaptureMax(user),
  userId: user._id,
})

export default serializeGameSession
