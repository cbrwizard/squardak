import copiedImageUUID from '../policies/copiedImageUUID'

const serializeTerritory = async (squares, user) => ({
  color: user.color,
  copiedImageUUID: await copiedImageUUID(user),
  imageUUID: user.imageUUID,
  linkText: user.linkText,
  linkUrl: user.linkUrl,
  nick: user.nick,
  squarePositions: squares.map(square => square.position),
  userId: user._id,
})

export default serializeTerritory
