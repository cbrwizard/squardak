import { pick } from 'ramda'

import copiedImageUUID from '../policies/copiedImageUUID'
import copyImageFromUserNick from '../policies/copyImageFromUserNick'

const serializeUser = async user => ({
  ...pick(
    [
      '_id',
      'color',
      'email',
      'guest',
      'linkText',
      'linkUrl',
      'imageUUID',
      'isPatreon',
      'nick',
      'passedTutorial',
    ],
    user
  ),
  copiedImageUUID: await copiedImageUUID(user),
  copyImageFromUserNick: await copyImageFromUserNick(user),
})

export const serializeUserForSquare = async user => ({
  ...pick(
    [
      '_id',
      'color',
      'imageUUID',
      'linkText',
      'linkUrl',
      'nick',
    ],
    user
  ),
  copiedImageUUID: await copiedImageUUID(user),
  copyImageFromUserNick: await copyImageFromUserNick(user),
})

export default serializeUser
