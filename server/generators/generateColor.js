import faker from 'faker'

import colors from 'shared/constants/colors'

const generateColor = () =>
  colors[faker.random.number(colors.length - 1)]

export default generateColor
