import faker from 'faker'

/**
  Generates something like Architect Bacon Michigan-45541
  @see https://cdn.rawgit.com/Marak/faker.js/master/examples/browser/index.html
  @note doesn't check for uniqueness of a nick before generating, so validation
    errors are possible.
**/
const generateNick = () =>
  `${faker.name.jobType()} ${faker.commerce.product()} ${faker.random.word()}-${faker.random.number()}`

export default generateNick
