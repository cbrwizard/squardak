import currentTurnsLeft from 'server/policies/currentTurnsLeft'

/**
 * Is responsible for determining if a user can perform any operation.
 */
const canUserPerformOperation = async (user) => {
  const turnsLeft = await currentTurnsLeft(user)
  return turnsLeft > 0
}

export default canUserPerformOperation
