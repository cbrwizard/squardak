import { findById } from 'server/queries/users'

/**
 * Is responsible for returning a copyImageFromUserNick if a user copied an image.
 * TODO: unite with copiedImageUUID.
 */
const copyImageFromUserNick = async (user) => {
  if (user.copyImageFromUser) {
    const userToCopyImageFrom = await findById(user.copyImageFromUser)
    return userToCopyImageFrom.nick
  }
  return null
}

export default copyImageFromUserNick
