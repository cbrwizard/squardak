import {
  MAX_TURNS_PER_DAY,
  MAX_TURNS_PER_DAY_FOR_PATREONS,
} from 'shared/constants/game'
import { numberOfOperationsByUserToday } from 'server/queries/operations'

/**
 * Is responsible for returning a number of turns left a user can do today.
 */
const currentTurnsLeft = async (user) => {
  const turnsToday = await numberOfOperationsByUserToday(user)
  const maxTurns = user.isPatreon
    ? MAX_TURNS_PER_DAY_FOR_PATREONS
    : MAX_TURNS_PER_DAY
  return maxTurns - turnsToday
}

export default currentTurnsLeft
