import { GAME_SESSION_STARTED } from 'server/constants/metricNames'
import doesMetricExistForUser from 'server/policies/doesMetricExistForUser'

/**
 * Is responsible for telling should a gameSessionStarted metric
 * be created for a user.
 * TODO: add a test.
 */
const shouldCreateGameSessionStartedMetric = async (userId) => {
  // A metric of this type with any status means that
  // this metric is already measured.
  const metricExists = await doesMetricExistForUser(
    GAME_SESSION_STARTED,
    userId
  )

  return !metricExists
}

export default shouldCreateGameSessionStartedMetric
