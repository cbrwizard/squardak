import { metricOfNameAndUser } from 'server/queries/metrics'

/**
 * Is responsible for telling if a metric record of certain name exists
   for a user.
 */
const doesMetricExistForUser = async (metricName, userOrUserId) => {
  const metricRecord = await metricOfNameAndUser(metricName, userOrUserId)

  return !!metricRecord
}

export default doesMetricExistForUser
