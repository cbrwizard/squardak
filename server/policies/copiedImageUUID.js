import { findById } from 'server/queries/users'

/**
 * Is responsible for returning a copiedImageUUID if a user copied an image.
 */
const copiedImageUUID = async (user) => {
  if (user.copyImageFromUser) {
    const userToCopyImageFrom = await findById(user.copyImageFromUser)
    return userToCopyImageFrom.imageUUID
  }
  return null
}

export default copiedImageUUID
