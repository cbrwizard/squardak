import { min } from 'ramda'

import { numberOfUserSquares } from 'server/queries/squares'
import { MAX_SQUARES_TO_CAPTURE } from 'shared/constants/game'

/**
 * Is responsible for returning a number of squares a user can currently capture.
 */
const squaresToCaptureMax = async (user) => {
  const userSquaresCount = await numberOfUserSquares(user)
  return min(userSquaresCount, MAX_SQUARES_TO_CAPTURE)
}

export default squaresToCaptureMax
