import { numberOfUserSquares } from 'server/queries/squares'

/**
 * Is responsible for determining if a user has any squares.
 * TODO: try one-line.
 */
const hasUserSquares = async (user) => {
  const numberOfSquares = await numberOfUserSquares(user)
  return !!numberOfSquares
}

export default hasUserSquares
