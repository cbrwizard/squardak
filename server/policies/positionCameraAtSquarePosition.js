import { randomCurrentUserSquare } from 'server/queries/squares'

/**
 * Is responsible for returning a square's position to position the camera to.
 */
const positionCameraAtSquarePosition = async (user) => {
  const square = await randomCurrentUserSquare(user)
  return square.position
}

export default positionCameraAtSquarePosition
