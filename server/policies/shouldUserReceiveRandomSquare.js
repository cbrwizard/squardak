import hasUserSquares from 'server/policies/hasUserSquares'

/**
 * Is responsible for determining is a user eligible for receiving a random square.
 * In the future will also check for banned.
 */
const shouldUserReceiveRandomSquare = async (user) => {
  const hasSquares = await hasUserSquares(user)
  return !hasSquares
}

export default shouldUserReceiveRandomSquare
