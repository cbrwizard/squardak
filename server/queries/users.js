import User from 'server/models/User'

export const allButOne = excludeUserId =>
  User.where({ _id: { $ne: excludeUserId } }).lean().exec()

export const create = attributes => new User(attributes).save()

export const findById = id => User.findById(id).exec()

// .save is used instead of .update to handle 'this' in validation
export const update = async (user, attributes) => user.set(attributes).save()

export const findByEmail = email => User.findOne({ email }).lean().exec()
export const findByNick = nick => User.findOne({ nick }).lean().exec()
