import Metric from 'server/models/Metric'

export const metricOfNameAndUser = (metricName, userOrUserId) =>
  Metric.findOne({ _user: userOrUserId, name: metricName }).lean().exec()

export const create = attributes => new Metric(attributes).save()

export const updateOrCreate = (attributesToFind, newAttributes) =>
  Metric.update(attributesToFind, newAttributes, {
    runValidators: true,
    upsert: true,
  }).exec()

export const update = (metric, attributes) =>
  Metric.findByIdAndUpdate(metric._id, attributes, {
    runValidators: true,
  }).exec()
