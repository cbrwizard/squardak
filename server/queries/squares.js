import Square from 'server/models/Square'

export const findByIds = ids => Square.where({ _id: { $in: ids } }).exec()

export const findByPositions = positions =>
  Square.where({ position: { $in: positions } }).exec()

export const findById = id => Square.findById(id).exec()

export const largestPositioned = () =>
  Square.findOne({})
    .sort({ column: -1, row: -1 })
    .lean()
    .exec()

export const randomSquare = async () => {
  const count = await Square.count()
  if (count === 0) {
    throw new Error('0 squares, build more!')
  }

  const random = Math.floor(Math.random() * count)

  return Square.findOne()
    .skip(random)
    .exec()
}

export const randomCurrentUserSquare = async (userOrUserId) => {
  const count = await Square.count({ _user: userOrUserId })
  const random = Math.floor(Math.random() * count)

  return Square.findOne({ _user: userOrUserId })
    .skip(random)
    .exec()
}

export const all = () =>
  Square.find({})
    .sort({ column: 1, row: 1 })
    .populate('_user', 'color nick')
    .lean()
    .exec()

export const numberOfSquares = () => Square.count()

export const numberOfUserSquares = userOrUserId =>
  Square.where({ _user: userOrUserId }).count()

export const userSquares = userOrUserId =>
  Square.where({ _user: userOrUserId })
    .lean()
    .exec()

// .save is used instead of .update to handle 'this' in validation
// TODO: consider using update with runValidators
export const update = async (square, attributes) =>
  square.set(attributes).save()

export const updateById = async (id, attributes) => {
  const square = await findById(id)
  return update(square, attributes)
}

export const create = attributes => new Square(attributes).save()

export const populate = async record =>
  Square.findOne({ _id: record._id })
    .populate('_user', 'color nick email')
    .exec()
