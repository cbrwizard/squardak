import Operation from 'server/models/Operation'

export const numberOfOperationsByUserToday = (userOrUserId) => {
  const now = new Date()
  const todayStart = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate())
  const tomorrowStart = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() + 1)

  return Operation.where({
    _user: userOrUserId,
    createdAt: {
      $gte: todayStart,
      $lt: tomorrowStart,
    },
  }).count()
}

export const create = attributes =>
  new Operation(attributes).save()

// TODO: find a way to do it in 1 request.
export const createAndPopulate = async (attributes) => {
  const createdRecord = await create(attributes)
  return Operation
    .findOne({ _id: createdRecord._id })
    .populate('_user', 'color nick')
    .exec()
}
