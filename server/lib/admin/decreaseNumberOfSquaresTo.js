import isPerfectSquare from 'shared/lib/isPerfectSquare'
import Square from 'server/models/Square'
import setUpRavenClient from 'server/lib/setUpRavenClient'
import getLogger from 'server/lib/getLogger'

const logger = getLogger()
const Raven = setUpRavenClient()
/**
 * Is responsible for decreasing the number of Square records in the db.
 */
const decreaseNumberOfSquaresTo = async (newNumberOfSquares) => {
  if (!isPerfectSquare(newNumberOfSquares)) {
    throw new Error('Number of squares must be a perfect square')
  }
  const currentNumberOfSquares = await Square.count()

  if (newNumberOfSquares >= currentNumberOfSquares) {
    throw new Error('New number of squares must be smaller than the existing')
  }

  Square.remove({
    $or: [
      {
        column: {
          $gt: Math.sqrt(newNumberOfSquares),
        },
      },
      {
        row: {
          $gt: Math.sqrt(newNumberOfSquares),
        },
      },
    ],
  })
    .then((removed) => {
      console.log(`removed ${removed}`)
    })
    .catch((err) => {
      Raven.captureException(err)
      logger.error({ err, stack: err.stack })
    })
}

module.exports = decreaseNumberOfSquaresTo
