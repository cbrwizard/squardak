import isPerfectSquare from 'shared/lib/isPerfectSquare'
import Square from 'server/models/Square'
import { create, largestPositioned } from 'server/queries/squares'

/**
 * Is responsible for increasing the number of Square records in the db.
 */
const increaseNumberOfSquaresTo = async (newNumberOfSquares) => {
  if (!isPerfectSquare(newNumberOfSquares)) {
    throw new Error('Number of squares must be a perfect square')
  }
  const currentNumberOfSquares = await Square.count()

  if (newNumberOfSquares <= currentNumberOfSquares) {
    throw new Error('New number of squares must be bigger than the existing')
  }

  // console.log('currentNumberOfSquares')
  // console.log(currentNumberOfSquares)
  const numberOfSquaresToCreate = newNumberOfSquares - currentNumberOfSquares
  // console.log('numberOfSquaresToCreate')
  // console.log(numberOfSquaresToCreate)

  if (numberOfSquaresToCreate === 1) {
    return
  } // don't support this case

  const lastSquare = await largestPositioned()
  // console.log('lastSquare')
  // console.log(lastSquare)

  // Always start with the first row.
  let currentRow = 1

  // Start with the last available column or the first one.
  const initialColumn = (lastSquare && lastSquare.row + 1) || 1

  let currentColumn = initialColumn
  let numberOfSquaresCreated = 0

  const createSquaresOnARow = async () => {
    // console.log('calling createSquaresOnARow')

    // if reached the most right column, stop and reset the column or if done creating.
    if (
      numberOfSquaresCreated === numberOfSquaresToCreate ||
      currentColumn > Math.sqrt(newNumberOfSquares)
    ) {
      // console.log('done with this column and row, returning from createSquaresOnARow and increasing the currentRow')
      return
    }

    // console.log(`not yet reached the sqrt, will keep using this column. sqrt: ${Math.sqrt(newNumberOfSquares)}`)
    // console.log(`creating a square on row ${currentRow} and column ${currentColumn}`)
    // console.log('currentColumn should be less than 3, column:', currentColumn)
    // TODO: use an adapter.
    await create({
      column: currentColumn,
      position: `${currentColumn}-${currentRow}`,
      row: currentRow,
    })

    // console.log('noted that we have created a square')
    // console.log('numberOfSquaresCreated before increase')
    // console.log(numberOfSquaresCreated)
    numberOfSquaresCreated += 1
    // console.log('numberOfSquaresCreated after increase')
    // console.log(numberOfSquaresCreated)

    currentColumn += 1
    console.log('currentColumn is increased now to because we want to try another column with the same row, column:', currentColumn)

    await createSquaresOnARow()
  }

  const createSquaresOnEachRow = async () => {
    // console.log('calling createSquaresOnAEachRow')
    // console.log(`currentColumn ${currentColumn}`)
    // console.log(`currentRow ${currentRow}`)
    if (numberOfSquaresCreated === numberOfSquaresToCreate) {
      // console.log('finished, numbers are eq, returning everything')
      return
    }
    // console.log(`numberOfSquaresCreated must be less than 5 here: ${numberOfSquaresCreated}`)
    // console.log('its less so lets create some squares')

    await createSquaresOnARow()

    // console.log('increasing row and resetting column, since reached the sqrt')
    currentRow += 1

    // If we have reached the totally new row, we need to start from the first column again.
    if (currentRow >= initialColumn) {
      currentColumn = 1
    } else {
      currentColumn = initialColumn
    }

    // console.log('now lets try with the next row and initialColumn')
    await createSquaresOnEachRow()
  }

  await createSquaresOnEachRow()
}

module.exports = increaseNumberOfSquaresTo
