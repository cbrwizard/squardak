import Square from 'server/models/Square'

const addPositionToSquareIfNeeded = async (square) => {
  if (!square.position) {
    await square.set({ position: `${square.column}-${square.row}` }).save()
  }
}

/**
 * Is responsible for adding a position attribute for all squares.
 * TODO: use an adapter.
 */
const addPositionToAllSquares = async () => {
  const allSquares = await Square.find({})

  const processPromises = allSquares.map(async (square) => {
    await addPositionToSquareIfNeeded(square)
  })
  await Promise.all(processPromises)
}

module.exports = addPositionToAllSquares
