import bcrypt from 'bcryptjs'

/**
 * Is responsible for telling if the passed password matches the one stored
 * of a passed user.
 */
const comparePassword = (user, candidatePassword) =>
  new Promise((resolve, reject) =>
    bcrypt.compare(candidatePassword, user.password, (err, isMatch) => {
      if (err) { reject(new Error(err)) }

      resolve(isMatch)
    })
  )

export default comparePassword
