import { pick } from 'ramda'
import { createUser, updateUserById } from 'server/services/users'
import {
  setCreatedResponse,
  setFailedResponse,
  setUpdatedResponse,
} from 'server/lib/responses'
import { findByNick } from '../queries/users'
import { handleClickedBecomePatronMetric } from '../services/metrics'

const filterPutParams = unfilteredParams =>
  pick(
    [
      'color',
      'email',
      'nick',
      'linkText',
      'linkUrl',
      'imageUUID',
      'passedTutorial',
      'password',
    ],
    unfilteredParams
  )

const filterPostParams = unfilteredParams =>
  pick(['email', 'password'], unfilteredParams)

const handleCopyImageParams = async (body) => {
  const filteredParams = filterPutParams(body)

  let copyImageFromUser

  if (body.copyImageFromUserNick) {
    copyImageFromUser = await findByNick(body.copyImageFromUserNick)
  }

  return { ...filteredParams, copyImageFromUser }
}

const post = async (ctx) => {
  try {
    const filteredParams = filterPostParams(ctx.request.body)
    const saveResult = await createUser(filteredParams)

    ctx.login(saveResult, (err) => {
      if (err) {
        setFailedResponse(ctx, err)
      } else {
        setCreatedResponse(ctx, saveResult)
      }
    })
  } catch (err) {
    setFailedResponse(ctx, err)
  }
}

const put = async (ctx) => {
  try {
    const id = ctx.state.user._id.toString()
    if (ctx.params.id !== id) {
      return setFailedResponse(ctx)
    }
    const paramsWithCopyImageFromUser = await handleCopyImageParams(
      ctx.request.body
    )

    if (ctx.request.body.isPatreon) {
      // No need to wait.
      handleClickedBecomePatronMetric(id)
    }

    await updateUserById(id, paramsWithCopyImageFromUser, ctx.io)

    return setUpdatedResponse(ctx)
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  post,
  put,
}
