import { pick } from 'ramda'

import { setFailedResponse, setUnauthorizedResponse } from 'server/lib/responses'
import createPerformPlanJob from 'server/jobs/creators/createPerformPlanJob'

const filterPostParams = unfilteredParams =>
  pick(['selectedSquaresPositions'], unfilteredParams)

const post = async (ctx) => {
  try {
    if (!ctx.isAuthenticated()) { return setUnauthorizedResponse(ctx) }

    const userId = ctx.state.user._id.toString()
    const filteredParams = filterPostParams(ctx.request.body)

    await createPerformPlanJob(userId, filteredParams.selectedSquaresPositions)

    ctx.status = 201
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  post,
}
