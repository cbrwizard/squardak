import { allButOne, findById } from 'server/queries/users'
import { userSquares } from 'server/queries/squares'
import {
  setFailedResponse,
  setUnauthorizedResponse,
} from 'server/lib/responses'
import serializeTerritory from 'server/serializers/territory'

/**
 * Is responsible for returning the current user's territory.
 */
const current = async (ctx) => {
  try {
    if (!ctx.isAuthenticated()) {
      return setUnauthorizedResponse(ctx)
    }

    const userId = ctx.state.user._id.toString()
    const user = await findById(userId)
    const squares = await userSquares(user)

    const territory = await serializeTerritory(squares, user)

    ctx.body = { territory }
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

/**
 * Is responsible for returning the territories which don't belong to a
 * current user.
 */
const others = async (ctx) => {
  try {
    if (!ctx.isAuthenticated()) {
      return setUnauthorizedResponse(ctx)
    }

    const userId = ctx.state.user._id.toString()
    const users = await allButOne(userId)
    const processPromises = users.map(async (user) => {
      const squares = await userSquares(user)
      if (squares.length) {
        return serializeTerritory(squares, user)
      }
    })

    const territories = await Promise.all(processPromises)

    // Removing empty values
    ctx.body = { territories: territories.filter(Boolean) }
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  current,
  others,
}
