import { all } from 'server/queries/squares'
import {
  setFailedResponse,
  setUnauthorizedResponse,
} from 'server/lib/responses'
import serializeSquare from 'server/serializers/square'

const get = async (ctx) => {
  try {
    if (!ctx.isAuthenticated()) {
      return setUnauthorizedResponse(ctx)
    }

    const squares = await all()
    const serializedSquares = squares.map(serializeSquare)

    ctx.body = { squares: serializedSquares }
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  get,
}
