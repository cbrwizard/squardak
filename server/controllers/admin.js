import {
  setFailedResponse,
  setUnauthorizedResponse,
} from 'server/lib/responses'
import Square from 'server/models/Square'
import increaseNumberOfSquaresTo
  from 'server/lib/admin/increaseNumberOfSquaresTo'
import decreaseNumberOfSquaresTo
  from 'server/lib/admin/decreaseNumberOfSquaresTo'
import addPositionToAllSquares
  from 'server/lib/admin/addPositionToAllSquares'

/**
 * Come up with a more secure solution ffs
 */
const isAdmin = ctx =>
  ctx.isAuthenticated() &&
  ['uuker@yandex.ru', 'cbrwizard@gmail.com', 'sajuukh@gmail.com'].includes(ctx.state.user.email)

const getIncreaseNumberOfSquaresTo = async (ctx) => {
  try {
    if (!isAdmin(ctx)) {
      return setUnauthorizedResponse(ctx)
    }

    const { number } = ctx.params
    await increaseNumberOfSquaresTo(number)
    const allSquares = await Square.find({})

    ctx.body = allSquares
    ctx.status = 201
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

const getDecreaseNumberOfSquaresTo = async (ctx) => {
  try {
    if (!isAdmin(ctx)) {
      return setUnauthorizedResponse(ctx)
    }

    const { number } = ctx.params
    await decreaseNumberOfSquaresTo(number)
    const allSquares = await Square.find({})

    ctx.body = allSquares
    ctx.status = 201
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

const getAddPositionToAllSquares = async (ctx) => {
  try {
    if (!isAdmin(ctx)) {
      return setUnauthorizedResponse(ctx)
    }

    await addPositionToAllSquares()
    const allSquares = await Square.find({})

    ctx.body = allSquares
    ctx.status = 201
    return true
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  getAddPositionToAllSquares,
  getIncreaseNumberOfSquaresTo,
  getDecreaseNumberOfSquaresTo,
}
