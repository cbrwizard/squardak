import { pick } from 'ramda'

import { createUser, updateUser } from 'server/services/users'
import {
  setSuccessResponse,
  setFailedResponse,
  setUnauthorizedResponse,
} from 'server/lib/responses'
import { initializeGame } from 'server/services/games'
import serializeGameSession from 'server/serializers/game'
import ensureMongooseObject from '../lib/mongoose/ensureMongooseObject'
import User from '../models/User'

const filterPostParams = unfilteredParams =>
  pick(['guest', 'nick'], unfilteredParams)

/*
  Is responsible for telling if a user has new params. If not - we shouldn't
  call update in a db.
 */
const shouldUpdateUser = (user, params) =>
  Object.keys(params).some(paramKey => user[paramKey] !== params[paramKey])

const handleUser = async (ctx) => {
  const filteredParams = filterPostParams(ctx.request.body)
  const loggedInUser = ctx.state.user

  if (!loggedInUser && filteredParams.guest) {
    return createUser(filteredParams, {
      handleEmptyColor: true,
      handleEmptyNick: true,
    })
  }

  if (shouldUpdateUser(loggedInUser, filteredParams)) {
    return updateUser(
      await ensureMongooseObject(loggedInUser, User),
      filteredParams
    )
  }

  return loggedInUser
}

const get = async (ctx) => {
  try {
    if (!ctx.isAuthenticated()) {
      return setUnauthorizedResponse(ctx)
    }

    const userInGameSession = await handleUser(ctx)
    const game = await serializeGameSession(userInGameSession)

    ctx.body = game
    return setSuccessResponse(ctx)
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

const post = async (ctx) => {
  try {
    // No need to check for a logged in user, since at this point we
    // might want to create a new user.
    const userInGameSession = await handleUser(ctx)

    const game = await initializeGame(userInGameSession)

    ctx.login(userInGameSession, (err) => {
      if (err) {
        return setFailedResponse(ctx, err)
      }
      ctx.body = game
      return setSuccessResponse(ctx)
    })
  } catch (err) {
    return setFailedResponse(ctx, err)
  }
}

export default {
  get,
  post,
}
