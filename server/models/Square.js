import timestamps from 'mongoose-timestamp'

import getMongoose from 'server/lib/getMongoose'
import validateUniqueness from 'server/lib/validateUniqueness'

const { mongoose, db } = getMongoose()
const { Schema } = mongoose
const ObjectId = Schema.Types.ObjectId

const squareSchema = new Schema({
  _user: {
    index: true,
    ref: 'User',
    required: false,
    type: ObjectId,
  },
  column: {
    min: 1,
    required: true,
    type: Number,
  },
  // TODO: add tests.
  position: {
    min: 3,
    required: true,
    type: String,
  },
  row: {
    min: 1,
    required: true,
    type: Number,
  },
})

squareSchema.plugin(timestamps)
validateUniqueness(squareSchema, 'column', 'Square', 'row')

const Square = db.model('Square', squareSchema)

export default Square
