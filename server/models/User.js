import timestamps from 'mongoose-timestamp'
import 'mongoose-type-email'
import sanitizerPlugin from 'mongoose-sanitizer'
import bcrypt from 'bcryptjs'
import 'mongoose-type-url'

import getMongoose from 'server/lib/getMongoose'
import validateUniqueness from 'server/lib/validateUniqueness'

const SALT_WORK_FACTOR = 10
const { mongoose, db } = getMongoose()
const { Schema } = mongoose
const ObjectId = Schema.Types.ObjectId

/**
 * Is responsible for encrypting a password.
 */
function encryptPassword(next) {
  const user = this

  // only hash the password if it has been modified and exists
  // (guests can have no passwords).
  if (!user.password || !user.isModified('password')) return next()

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, (saltGenerationError, salt) => {
    if (saltGenerationError) return next(saltGenerationError)

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, (err, hash) => {
      if (err) return next(err)

      // override the cleartext password with the hashed one
      user.password = hash
      next()
    })
  })
}

// TODO: add an optional validation for password length (which allows null)
// TODO: test imageUUID
// TODO: test copyImageFromUser
// TODO: test link
// TODO: test isPatreon
const userSchema = new Schema({
  color: {
    minlength: 2,
    type: String,
  },
  copyImageFromUser: {
    index: true,
    ref: 'User',
    required: false,
    type: ObjectId,
  },
  email: {
    required() {
      return this.guest !== undefined && !this.guest
    },
    type: String,
    // TODO: uncomment when https://github.com/konsumer/mongoose-type-email/issues/6 is fixed
    // type: mongoose.SchemaTypes.Email,
  },
  guest: {
    default: false,
    type: Boolean,
  },
  isPatreon: {
    default: false,
    type: Boolean,
  },
  imageUUID: {
    type: String,
  },
  linkText: {
    minlength: 2,
    type: String,
  },
  linkUrl: {
    minlength: 5,
    type: mongoose.SchemaTypes.Url,
  },
  nick: {
    minlength: 2,
    type: String,
  },
  passedTutorial: {
    default: false,
    type: Boolean,
  },
  password: {
    required() {
      return this.guest !== undefined && !this.guest
    },
    type: String,
  },
})

userSchema.pre('save', encryptPassword)

validateUniqueness(userSchema, 'email', 'User')
validateUniqueness(userSchema, 'nick', 'User')

userSchema.plugin(timestamps)
userSchema.plugin(sanitizerPlugin)

const User = db.model('User', userSchema)

export default User
