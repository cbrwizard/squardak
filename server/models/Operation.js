import timestamps from 'mongoose-timestamp'

import getMongoose from 'server/lib/getMongoose'

const { mongoose, db } = getMongoose()
const { Schema } = mongoose
const ObjectId = Schema.Types.ObjectId

const operationSchema = new Schema({
  _user: {
    index: true,
    ref: 'User',
    required: true,
    type: ObjectId,
  },
  // TODO: add a test
  affectedUsers: [{
    ref: 'User',
    required: false,
    type: ObjectId,
  }],
  squares: [{
    ref: 'Square',
    required: true,
    type: ObjectId,
  }],
})

operationSchema.plugin(timestamps)

const Operation = db.model('Operation', operationSchema)

export default Operation
