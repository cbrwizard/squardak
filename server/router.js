import Router from 'koa-router'

import adminController from 'server/controllers/admin'
import gamesController from 'server/controllers/games'
import pagesController from 'server/controllers/pages'
import messagesController from 'server/controllers/messages'
import plansController from 'server/controllers/plans'
import sessionsController from 'server/controllers/sessions'
import squaresController from 'server/controllers/squares'
import territoriesController from 'server/controllers/territories'
import usersController from 'server/controllers/users'

const router = new Router()

router
  .get('/', pagesController.get)
  .get(
    '/admin/increaseNumberOfSquaresTo/:number',
    adminController.getIncreaseNumberOfSquaresTo
  )
  .get(
    '/admin/decreaseNumberOfSquaresTo/:number',
    adminController.getDecreaseNumberOfSquaresTo
  )
  .get(
    '/admin/addPositionToAllSquares',
    adminController.getAddPositionToAllSquares
  )
  .get('/games', gamesController.get)
  .post('/games', gamesController.post)
  .get('/messages', messagesController.get)
  .post('/messages', messagesController.post)
  .post('/plans', plansController.post)
  .del('/sessions', sessionsController.del)
  .get('/sessions', sessionsController.get)
  .post('/sessions', sessionsController.post)
  .get('/sessions/data', sessionsController.getSessionData)
  .get('/squares', squaresController.get)
  .get('/territories/current', territoriesController.current)
  .get('/territories/others', territoriesController.others)
  .post('/users', usersController.post)
  .put('/user/:id', usersController.put)

export default router
