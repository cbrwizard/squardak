import faker from 'faker'
import test from 'ava'

import generateColor from 'server/generators/generateColor'

test('function returns a random color', async (t) => {
  const expected = 'string'
  const actual = typeof generateColor()

  t.is(expected, actual)
})
