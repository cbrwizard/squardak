import test from 'ava'

import factory from 'server/test/factories/Square'

import Square from 'server/models/Square'
import hasUserSquares from 'server/policies/hasUserSquares'

test('returns false when a user has no squares', async (t) => {
  const user = await factory.create('User')

  await Square.remove({})

  const expected = false
  const actual = await hasUserSquares(user)

  t.is(actual, expected)
})

test('returns true when a user has some squares', async (t) => {
  const user = await factory.create('User')

  await Square.remove({})
  await factory.createMany('Square', 2, { _user: user._id })
  t.is(await Square.find({}).count(), 2)

  const expected = true
  const actual = await hasUserSquares(user)

  t.is(actual, expected)
})
