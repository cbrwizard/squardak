import test from 'ava'

import factory from 'server/test/factories/Square'

import Square from 'server/models/Square'
import shouldUserReceiveRandomSquare from 'server/policies/shouldUserReceiveRandomSquare'



test('returns true when a user has no squares', async (t) => {
  const user = await factory.create('User')

  await Square.remove({})
  t.is(await Square.find({}).count(), 0)

  const expected = true
  const actual = await shouldUserReceiveRandomSquare(user)

  t.is(actual, expected)
})

test('returns false when a user has some squares', async (t) => {
  const user = await factory.create('User')

  await Square.remove({})
  await factory.createMany('Square', 1, { _user: user._id })
  t.is(await Square.find({}).count(), 1)

  const expected = false
  const actual = await shouldUserReceiveRandomSquare(user)

  t.is(actual, expected)
})
