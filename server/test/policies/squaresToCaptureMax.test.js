import test from 'ava'

import factory from 'server/test/factories/Operation'

import Square from 'server/models/Operation'
import squaresToCaptureMax from 'server/policies/squaresToCaptureMax'
import { MAX_SQUARES_TO_CAPTURE } from 'shared/constants/game'



test('returns a correct number of squares to capture when user has less than 10 squares', async (t) => {
  const user = await factory.create('User')
  await Square.remove({})
  const numberOfSquares = 4

  await factory.createMany('Square', numberOfSquares, { _user: user._id })

  const expected = numberOfSquares
  const actual = await squaresToCaptureMax(user)

  t.is(actual, expected)
})

test('returns a correct number of squares to capture when user has 10 squares', async (t) => {
  const user = await factory.create('User')
  await Square.remove({})
  const numberOfSquares = 10

  await factory.createMany('Square', numberOfSquares, { _user: user._id })

  const expected = numberOfSquares
  const actual = await squaresToCaptureMax(user)

  t.is(actual, expected)
})

test('returns a correct number of squares to capture when user has more than 10 squares', async (t) => {
  const user = await factory.create('User')
  await Square.remove({})
  const numberOfSquares = 100

  await factory.createMany('Square', numberOfSquares, { _user: user._id })

  const expected = MAX_SQUARES_TO_CAPTURE
  const actual = await squaresToCaptureMax(user)

  t.is(actual, expected)
})
