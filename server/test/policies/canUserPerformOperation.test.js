import test from 'ava'

import factory from 'server/test/factories/Operation'

import Operation from 'server/models/Operation'
import canUserPerformOperation from 'server/policies/canUserPerformOperation'

test('returns false when a user already made a maximum amount of turns today', async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  t.is(await Operation.find({}).count(), 0)

  await factory.createMany('Operation', 10, { _user: user._id })

  const expected = false
  const actual = await canUserPerformOperation(user)

  t.is(actual, expected)
})

test(
  "returns true when a user hasn't performed the maximum amount of turns today yet",
  async (t) => {
    const user = await factory.create('User')
    await Operation.remove({})
    t.is(await Operation.find({}).count(), 0)

    const now = new Date()
    const yesterdayStart = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate() - 1)
    await factory.createMany('Operation', 9, {
      _user: user._id,
      createdAt: yesterdayStart.valueOf(),
    })

    await factory.createMany('Operation', 9, { _user: user._id })

    const expected = true
    const actual = await canUserPerformOperation(user)

    t.is(actual, expected)
  }
)
