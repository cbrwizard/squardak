import test from 'ava'

import factory from 'server/test/factories/Operation'

import Operation from 'server/models/Operation'
import currentTurnsLeft from 'server/policies/currentTurnsLeft'



test('returns a correct number of turns left', async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  t.is(await Operation.find({}).count(), 0)

  await factory.createMany('Operation', 4, { _user: user._id })

  const expected = 6
  const actual = await currentTurnsLeft(user)

  t.is(actual, expected)
})
