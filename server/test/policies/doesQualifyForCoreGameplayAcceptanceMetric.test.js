import test from 'ava'

import factory from 'server/test/factories/Operation'

import Operation from 'server/models/Operation'
import Square from 'server/models/Square'
import doesQualifyForCoreGameplayAcceptanceMetric
  from 'server/policies/doesQualifyForCoreGameplayAcceptanceMetric'

test('returns false when a user made less than 10 turns today', async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  t.is(await Operation.find({}).count(), 0)

  await factory.createMany('Operation', 9, { _user: user._id })

  const expected = false
  const actual = await doesQualifyForCoreGameplayAcceptanceMetric(user)

  t.is(actual, expected)
})

test('returns false when a user captured less than 30 squares', async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  t.is(await Operation.find({}).count(), 0)

  await factory.createMany('Operation', 9, { _user: user._id })

  const capturedSquares = await factory.createMany('Square', 29, {
    _user: user.id,
  })

  await factory.create('Operation', {
    _user: user._id,
    squares: capturedSquares.map(square => square._id),
  })

  t.is(await Square.where({ _user: user._id }).count(), 29)

  const expected = false
  const actual = await doesQualifyForCoreGameplayAcceptanceMetric(user)

  t.is(actual, expected)
})

// TODO: update the test
test.skip('returns true when a user made 10 turns and captured >= 30 squares today', async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  t.is(await Operation.find({}).count(), 0)

  await factory.createMany('Operation', 9, { _user: user._id })
  const capturedSquares = await factory.createMany('Square', 30, {
    _user: user.id,
  })

  await factory.create('Operation', {
    _user: user._id,
    squares: capturedSquares.map(square => square._id),
  })

  t.is(await Square.where({ _user: user._id }).count(), 30)

  const expected = true
  const actual = await doesQualifyForCoreGameplayAcceptanceMetric(user)

  t.is(actual, expected)
})
