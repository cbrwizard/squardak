import test from 'ava'

import factory from 'server/test/factories/Metric'

import Metric from 'server/models/Metric'
import doesMetricExistForUser from 'server/policies/doesMetricExistForUser'
import { CORE_GAMEPLAY_ACCEPTANCE } from 'server/constants/metricNames'

test('returns false when a metric record with passed name does not exist for a passed user', async (t) => {
  await Metric.remove({})
  t.is(await Metric.find({}).count(), 0)

  const user = await factory.create('User')
  const metricName = CORE_GAMEPLAY_ACCEPTANCE

  const expected = false
  const actual = await doesMetricExistForUser(metricName, user)

  t.is(actual, expected)
})

test('returns true when a metric record with passed name exists for a passed user', async (t) => {
  await Metric.remove({})
  t.is(await Metric.find({}).count(), 0)

  const user = await factory.create('User')
  const metricName = CORE_GAMEPLAY_ACCEPTANCE

  await factory.create('Metric', { _user: user._id, name: metricName })

  const expected = true
  const actual = await doesMetricExistForUser(metricName, user)

  t.is(actual, expected)
})
