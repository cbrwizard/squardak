import { factory, MongooseAdapter } from 'factory-girl'
import Metric from 'server/models/Metric'
import 'server/test/factories/User'
import { CORE_GAMEPLAY_ACCEPTANCE } from 'server/constants/metricNames'
import { SUCCEEDED } from 'server/constants/metricStatuses'

// use the mongoose adapter as the default adapter
const adapter = new MongooseAdapter()
factory.setAdapter(adapter)

factory.define('Metric', Metric, {
  _user: factory.assoc('User', '_id'),
  name: CORE_GAMEPLAY_ACCEPTANCE,
  status: SUCCEEDED,
})

export default factory
