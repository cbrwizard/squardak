import { factory, MongooseAdapter } from 'factory-girl'

import Operation from 'server/models/Operation'
import 'server/test/factories/User'
import 'server/test/factories/Square'

// use the mongoose adapter as the default adapter
const adapter = new MongooseAdapter()
factory.setAdapter(adapter)

factory.define('Operation', Operation, {
  _user: factory.assoc('User', '_id'),
  squares: factory.assocMany('Square', 2, '_id'),
})

export default factory
