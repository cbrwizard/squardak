import { factory, MongooseAdapter } from 'factory-girl'
import faker from 'faker'

import User from 'server/models/User'

// use the mongoose adapter as the default adapter
const adapter = new MongooseAdapter()
factory.setAdapter(adapter)

factory.define('User', User, {
  email: factory.seq('User.email', n => `${faker.internet.email()}_${n}`),
  nick: factory.seq('User.nick', n => `${faker.internet.userName()}_${n}`),
  password: () => faker.internet.password(),
})

factory.define('Guest', User, {
  email: null,
  guest: true,
  password: null,
})

export default factory
