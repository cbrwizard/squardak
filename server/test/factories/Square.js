import { factory, MongooseAdapter } from 'factory-girl'
import faker from 'faker'

import Square from 'server/models/Square'
import 'server/test/factories/User'

// use the mongoose adapter as the default adapter
const adapter = new MongooseAdapter()
factory.setAdapter(adapter)

// TODO: make position dynamically get assigned from column and row.
// Probably make it work in a model.
factory.define('Square', Square, {
  _user: factory.assoc('User', '_id'),
  column: factory.seq('Square.column', n => faker.random.number() + n),
  position: () =>
    `${factory.seq('Square.column', n => faker.random.number() + n)}-${factory.seq('Square.row', n => faker.random.number() + n)}`,
  row: factory.seq('Square.row', n => faker.random.number() + n),
})

export default factory
