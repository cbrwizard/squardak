import { factory, MongooseAdapter } from 'factory-girl'
import faker from 'faker'
import Message from 'server/models/Message'
import 'server/test/factories/User'

// use the mongoose adapter as the default adapter
const adapter = new MongooseAdapter()
factory.setAdapter(adapter)

factory.define('Message', Message, {
  _user: factory.assoc('User', '_id'),
  text: () => faker.lorem.paragraph(),
})

export default factory
