import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/Message'

import { createMessage } from 'server/services/messages'
import Message from 'server/models/Message'

test.serial("createMessage doesn't create a message when the attributes are invalid", async (t) => {
  await Message.remove({})

  const attributes = {
    text: faker.hacker.noun(),
  }

  try {
    await createMessage(attributes, false)
    t.fail()
  } catch (e) {
    t.is(await Message.count({}), 0)
    t.is(e.message, 'Message validation failed')
  }
})

test.serial('createMessage creates a message when the attributes are valid', async (t) => {
  await Message.remove({})

  const user = await factory.create('User')
  const attributes = {
    _user: user._id,
    text: faker.hacker.noun(),
  }

  await createMessage(attributes, false)
  t.is(await Message.count({}), 1)
})
