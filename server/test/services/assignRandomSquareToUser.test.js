import test from 'ava'

import factory from 'server/test/factories/Square'
import reload from 'server/test/helpers/reload'
import assignRandomSquareToUser from 'server/services/squares/assignRandomSquareToUser'
import Square from 'server/models/Square'

test('correctly assigns a random free square to a user', async (t) => {
  const user = await factory.create('User')
  const anotherUser = await factory.create('User')
  await factory.create('Square', { _user: user._id })
  await factory.create('Square', { _user: anotherUser._id.toString() })

  let freeSquare = await factory.create('Square', { _user: null })
  t.is(freeSquare._user, null)

  await assignRandomSquareToUser(user)
  freeSquare = await reload(freeSquare, Square)

  const expected = user._id
  const actual = freeSquare._user

  t.deepEqual(actual, expected)
})
