import test from 'ava'

import factory from 'server/test/factories/Operation'

import Square from 'server/models/Square'
import Operation from 'server/models/Operation'
import reload from 'server/test/helpers/reload'

import { performPlan } from 'server/services/plans'

// TODO: fix these tests.
test.skip("performPlan throws an error when the user can't capture that many squares this turn", async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  await Square.remove({})

  await factory.create('Square', {
    _user: user._id,
    column: 2,
    row: 1,
  })
  await factory.create('Square', {
    _user: user._id,
    column: 2,
    row: 2,
  })
  const col1Row1Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 1,
  })
  const col1Row2Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 2,
  })
  const col1Row3Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 3,
  })

  const userId = user._id.toString()
  const selectedSquaresPositions = [
    col1Row1Square,
    col1Row2Square,
    col1Row3Square,
  ].map(doc => doc.position)

  const error = await t.throws(performPlan(userId, selectedSquaresPositions))
  t.is(error.message, "Can't capture that many squares this turn")
})

test.skip("performPlan throws an error when the user can't make any more operations today", async (t) => {
  const user = await factory.create('User')
  await Operation.remove({})
  await Square.remove({})

  await factory.create('Square', {
    _user: user._id,
    column: 2,
    row: 1,
  })
  const col1Row1Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 1,
  })
  const col1Row2Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 2,
  })

  await factory.createMany('Operation', 10, { _user: user._id })

  const userId = user._id.toString()
  const selectedSquaresPositions = [col1Row1Square, col1Row2Square].map(
    doc => doc.position
  )

  const error = await t.throws(performPlan(userId, selectedSquaresPositions))
  t.is(error.message, 'Cannot make more turns today.')
})

test.skip('performPlan throws an error when not all squares are capturable', async (t) => {
  await Square.remove({})

  const user = await factory.create('User')
  const usersSquare = await factory.create('Square', {
    _user: user._id,
    column: 3,
    row: 1,
  })
  const col1Row1Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 1,
  })
  const col1Row2Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 2,
  })

  const userId = user._id.toString()
  const selectedSquaresPositions = [usersSquare, col1Row1Square, col1Row2Square].map(
    doc => doc.position
  )

  const error = await t.throws(performPlan(userId, selectedSquaresPositions))
  t.is(error.message, 'Not all squares are capturable')
})

test.skip("performPlan doesn't throw an error when a user can make more turns today and all squares are capturable", async (t) => {
  await Square.remove({})

  const user = await factory.create('User')
  await factory.create('Square', {
    _user: user._id,
    column: 2,
    row: 1,
  })
  await factory.create('Square', {
    _user: user._id,
    column: 2,
    row: 2,
  })

  const col1Row1Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 1,
  })
  const col1Row2Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 2,
  })

  const userId = user._id.toString()
  const selectedSquaresPositions = [col1Row1Square, col1Row2Square].map(
    doc => doc.position
  )

  await t.notThrows(performPlan(userId, selectedSquaresPositions))
})

test.skip('performPlan assignes the selected squares to a user when the params are correct', async (t) => {
  await Square.remove({})

  const hunter = await factory.create('User')
  await factory.create('Square', {
    _user: hunter._id.toString(),
    column: 2,
    row: 1,
  })
  await factory.create('Square', {
    _user: hunter._id.toString(),
    column: 2,
    row: 2,
  })

  const victim = await factory.create('User')
  let col1Row2Square = await factory.create('Square', {
    _user: victim._id.toString(),
    column: 1,
    row: 2,
  })

  let col1Row1Square = await factory.create('Square', {
    _user: null,
    column: 1,
    row: 1,
  })

  t.is(col1Row1Square._user, null)
  t.is(col1Row2Square._user.toString(), victim._id.toString())

  const userId = hunter._id.toString()
  const selectedSquaresPositions = [col1Row1Square, col1Row2Square].map(
    doc => doc.position
  )

  await performPlan(userId, selectedSquaresPositions)

  col1Row1Square = await reload(col1Row1Square, Square)
  col1Row2Square = await reload(col1Row2Square, Square)

  t.is(col1Row1Square._user.toString(), hunter._id.toString())
  t.is(col1Row2Square._user.toString(), hunter._id.toString())
})
