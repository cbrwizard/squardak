import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/User'
import reload from 'server/test/helpers/reload'
import { createUser, updateUser, updateUserById } from 'server/services/users'
import User from 'server/models/User'

// Flaky
test.skip(
  "createUser doesn't create a user when the attributes are invalid",
  async (t) => {
    const attributes = {
      email: faker.internet.email(),
      nick: faker.internet.userName(),
      password: null,
    }

    try {
      await createUser(attributes)
      t.fail()
    } catch (e) {
      t.is(await User.count({}), 0)
      t.is(e.message, 'User validation failed')
    }
  }
)

// Flaky
test.skip(
  'createUser creates a user when the attributes are valid',
  async (t) => {
    const attributes = {
      email: faker.internet.email(),
      nick: faker.internet.userName(),
      password: faker.internet.password(),
    }

    await createUser(attributes)
    t.is(await User.count({}), 1)
  }
)

test('createUser adds a random nick to a new user when the option handleEmptyNick is true', async (t) => {
  const attributes = {
    email: faker.internet.email(),
    nick: null,
    password: faker.internet.password(),
  }

  const user = await createUser(attributes, { handleEmptyNick: true })
  t.not(user.nick, null)
})

test("createUser doesn't add a random nick to a new user when no options are passed", async (t) => {
  const attributes = {
    email: faker.internet.email(),
    nick: null,
    password: faker.internet.password(),
  }

  const user = await createUser(attributes)
  t.is(user.nick, null)
})

test('createUser adds a random color to a new user when the option handleEmptyColor is true', async (t) => {
  const attributes = {
    color: null,
    email: faker.internet.email(),
    password: faker.internet.password(),
  }

  const user = await createUser(attributes, { handleEmptyColor: true })
  t.not(user.color, null)
})

test("createUser doesn't add a random color to a new user when no options are passed", async (t) => {
  const attributes = {
    color: null,
    email: faker.internet.email(),
    password: faker.internet.password(),
  }

  const user = await createUser(attributes)
  t.is(user.color, null)
})

test("updateUser doesn't update a user when the attributes are invalid", async (t) => {
  let user = await factory.create('User')
  const invalidNick = 'q'
  const attributes = {
    nick: invalidNick,
  }

  try {
    user = await updateUser(user, attributes)
    t.fail()
  } catch (e) {
    user = await reload(user, User)
    t.not(user.nick, invalidNick)
    t.is(e.message, 'User validation failed')
  }
})

test('updateUser updates a user when the attributes are valid', async (t) => {
  const previousEmail = faker.internet.email()
  const user = await factory.create('User', { email: previousEmail })
  const newEmail = faker.internet.email()
  const attributes = {
    email: newEmail,
  }

  t.not(previousEmail, newEmail)

  await updateUser(user, attributes)
  t.pass()
})

test('updateUserById updates a user when the attributes are valid', async (t) => {
  const previousEmail = faker.internet.email()
  const user = await factory.create('User', { email: previousEmail })
  const newEmail = faker.internet.email()
  const attributes = {
    email: newEmail,
  }

  t.not(previousEmail, newEmail)

  await updateUserById(user._id, attributes)
  t.pass()
})

test('updateUser switches a guest to false when a guest now has an email and password', async (t) => {
  const user = await factory.create('Guest')
  const email = faker.internet.email()
  const password = faker.internet.password()
  const attributes = {
    email,
    password,
  }

  t.is(user.guest, true)
  await updateUser(user, attributes)

  t.is(user.guest, false)
})

test("updateUser doesn't switch a guest to false when a guest now has an email without password", async (t) => {
  const user = await factory.create('Guest')
  const email = faker.internet.email()
  const attributes = { email }

  t.is(user.guest, true)
  await updateUser(user, attributes)

  t.is(user.guest, true)
})

test('updateUser allows a guest without password to remove an email', async (t) => {
  const email = faker.internet.email()
  const user = await factory.create('Guest', { email })
  const attributes = { email: null }

  t.is(user.email, email)
  t.is(user.guest, true)
  await updateUser(user, attributes)

  t.is(user.guest, true)
  t.is(user.email, null)
})

test("updateUser doesn't allow a non-guest to remove an email", async (t) => {
  const user = await factory.create('User')
  const attributes = { email: null }

  t.truthy(user.email)
  t.is(user.guest, false)

  try {
    await updateUser(user, attributes)
    t.fail()
  } catch (e) {
    t.is(user.guest, false)
    t.is(user.email, null)
    t.is(e.message, 'User validation failed')
  }
})

test('updateUser adds a random nick to a user when a user has no nick', async (t) => {
  const previousNick = null
  const user = await factory.create('User', { nick: previousNick })
  t.is(user.nick, previousNick)
  const attributes = {}

  await updateUser(user, attributes)
  t.not(user.nick, previousNick)
})

test('updateUser adds a random nick to a user when attributes have an empty nick key', async (t) => {
  const previousNick = faker.internet.userName()
  const user = await factory.create('User', { nick: previousNick })
  t.is(user.nick, previousNick)
  const attributes = { nick: null }

  await updateUser(user, attributes)
  t.not(user.nick, null)
  t.not(user.nick, previousNick)
})

test("updateUser doesn't change a nick when a user already has one", async (t) => {
  const previousNick = faker.internet.userName()
  const user = await factory.create('User', { nick: previousNick })
  t.is(user.nick, previousNick)
  const attributes = {}

  await updateUser(user, attributes)
  t.is(user.nick, previousNick)
})

test('updateUser sets a nick passed in attributes if there is one', async (t) => {
  const previousNick = faker.internet.userName()
  const newNick = faker.internet.userName()
  const user = await factory.create('User', { nick: previousNick })
  t.is(user.nick, previousNick)
  const attributes = { nick: newNick }

  await updateUser(user, attributes)
  t.is(user.nick, newNick)
})
