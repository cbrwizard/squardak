import test from 'ava'

import factory from 'server/test/factories/Square'

import assignSquareToUser from 'server/services/squares/assignSquareToUser'
import reload from 'server/test/helpers/reload'
import Square from 'server/models/Square'



test('correctly assigns a square to a user when a square is passed and a user is passed as an object', async (t) => {
  const user = await factory.create('User')
  const square = await factory.create('Square', { _user: null })
  t.is(square._user, null)

  await assignSquareToUser(square, user)

  const expected = user._id
  const actual = square._user

  t.deepEqual(actual, expected)
})

test('correctly assigns a square to a user when a square is passed and a user is passed as an id', async (t) => {
  const user = await factory.create('User')
  const square = await factory.create('Square', { _user: null })
  t.is(square._user, null)

  await assignSquareToUser(square, user._id)

  const expected = user._id
  const actual = square._user

  t.deepEqual(actual, expected)
})

test('correctly assigns a square to a user when a square id is passed and a user is passed as an object', async (t) => {
  const user = await factory.create('User')
  let square = await factory.create('Square', { _user: null })
  t.is(square._user, null)

  await assignSquareToUser(square._id, user)
  square = await reload(square, Square)

  const expected = user._id
  const actual = square._user

  t.deepEqual(actual, expected)
})

test('correctly assigns a square to a user when a square id is passed and a user is passed as an id', async (t) => {
  const user = await factory.create('User')
  let square = await factory.create('Square', { _user: null })
  t.is(square._user, null)

  await assignSquareToUser(square._id, user._id)
  square = await reload(square, Square)

  const expected = user._id
  const actual = square._user

  t.deepEqual(actual, expected)
})
