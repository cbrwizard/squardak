import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/Operation'
import { createOperation } from 'server/services/operations'
import Operation from 'server/models/Operation'
import Square from 'server/models/Square'

test.serial("createOperation doesn't create a operation when the attributes are invalid", async (t) => {
  await Square.remove({})
  await Operation.remove({})

  const user = await factory.create('User')
  const userId = user._id.toString()

  const col1Row1Square = await factory.create('Square', { _user: null, column: 1, row: 1 })
  const col1Row2Square = await factory.create('Square', { _user: null, column: 1, row: 2 })
  const selectedSquaresPositions = [col1Row1Square, col1Row2Square].map(doc => doc._id)

  const attributes = {
    squares: selectedSquaresPositions,
  }

  try {
    await createOperation(attributes, false)
    t.fail()
  } catch (e) {
    t.is(await Operation.count({}), 0)
    t.is(e.message, 'Operation validation failed')
  }
})

test.serial('createOperation creates a operation when the attributes are valid', async (t) => {
  await Square.remove({})
  await Operation.remove({})

  const user = await factory.create('User')
  const userId = user._id.toString()

  const col1Row1Square = await factory.create('Square', { _user: null, column: 1, row: 1 })
  const col1Row2Square = await factory.create('Square', { _user: null, column: 1, row: 2 })
  const selectedSquaresPositions = [col1Row1Square, col1Row2Square].map(doc => doc._id)

  const attributes = {
    _user: userId,
    squares: selectedSquaresPositions,
  }

  await createOperation(attributes)
  t.is(await Operation.count({}), 1)
})
