import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/Metric'
import reload from 'server/test/helpers/reload'

import {
  CORE_GAMEPLAY_ACCEPTANCE,
  GAME_SESSION_STARTED,
} from 'server/constants/metricNames'
import { SUCCEEDED } from 'server/constants/metricStatuses'
import {
  createMetric,
  updateMetric,
  updateOrCreateMetric,
} from 'server/services/metrics'
import Metric from 'server/models/Metric'

test.serial(
  "createMetric doesn't create a metric when the attributes are invalid",
  async (t) => {
    await Metric.remove({})

    const attributes = {
      name: faker.hacker.noun(),
    }

    try {
      await createMetric(attributes)
      t.fail()
    } catch (e) {
      t.is(await Metric.count({}), 0)
      t.is(e.message, 'Metric validation failed')
    }
  }
)

test.serial(
  'createMetric creates a metric when the attributes are valid',
  async (t) => {
    await Metric.remove({})

    const user = await factory.create('User')
    const attributes = {
      _user: user._id,
      name: CORE_GAMEPLAY_ACCEPTANCE,
      status: SUCCEEDED,
    }

    await createMetric(attributes)
    t.is(await Metric.count({}), 1)
  }
)

test.serial(
  "updateOrCreateMetric doesn't create a metric when the attributes are invalid and a metric with attributesToFind doesn't exist yet",
  async (t) => {
    await Metric.remove({})

    const attributesToFind = {
      name: CORE_GAMEPLAY_ACCEPTANCE,
    }
    const newAttributes = {
      status: 'what',
    }

    try {
      await updateOrCreateMetric(attributesToFind, newAttributes)
      t.fail()
    } catch (e) {
      t.is(await Metric.count({}), 0)
      t.is(e.message, 'Validation failed')
    }
  }
)

test.serial(
  "updateOrCreateMetric doesn't update a metric when the attributes are invalid and a metric with attributesToFind exists already",
  async (t) => {
    await Metric.remove({})

    const attributesToFind = {
      name: CORE_GAMEPLAY_ACCEPTANCE,
    }

    const newStatus = 'what'

    let metricToUpdate = await factory.create('Metric', { ...attributesToFind })
    t.not(metricToUpdate.status, newStatus)

    const newAttributes = {
      status: newStatus,
    }

    try {
      await updateOrCreateMetric(attributesToFind, newAttributes)
      t.fail()
    } catch (e) {
      metricToUpdate = await reload(metricToUpdate, Metric)
      t.not(metricToUpdate.status, newStatus)
      t.is(e.message, 'Validation failed')
    }
  }
)

test.serial(
  'createMetric updates a metric when the attributes are valid and a metric with attributesToFind already exists',
  async (t) => {
    await Metric.remove({})

    const user = await factory.create('User')
    const attributesToFind = {
      _user: user._id,
      name: CORE_GAMEPLAY_ACCEPTANCE,
    }

    const newName = GAME_SESSION_STARTED

    let metricToUpdate = await factory.create('Metric', { ...attributesToFind })
    t.not(metricToUpdate.name, newName)

    const newAttributes = {
      name: newName,
    }

    await updateOrCreateMetric(attributesToFind, newAttributes)

    metricToUpdate = await reload(metricToUpdate, Metric)

    const expected = newName
    const actual = metricToUpdate.name

    t.is(actual, expected)
  }
)

test.serial(
  "createMetric creates a metric when the attributes are valid and a metric with attributesToFind doesn't exist yet",
  async (t) => {
    await Metric.remove({})

    const user = await factory.create('User')
    const attributesToFind = {
      _user: user._id,
      name: CORE_GAMEPLAY_ACCEPTANCE,
    }
    const newAttributes = {
      status: SUCCEEDED,
    }

    await updateOrCreateMetric(attributesToFind, newAttributes)
    t.is(await Metric.count({}), 1)
  }
)

test.serial(
  "updateMetric doesn't update a metric when the attributes are invalid",
  async (t) => {
    await Metric.remove({})

    const newName = 'what'

    let metricToUpdate = await factory.create('Metric')
    t.not(metricToUpdate.name, newName)

    const newAttributes = {
      name: newName,
    }

    try {
      await updateMetric(metricToUpdate, newAttributes)
      t.fail()
    } catch (e) {
      metricToUpdate = await reload(metricToUpdate, Metric)
      t.not(metricToUpdate.name, newName)
      t.is(e.message, 'Validation failed')
    }
  }
)

test.serial(
  'updateMetric updates a metric when the attributes are valid',
  async (t) => {
    await Metric.remove({})

    const newName = GAME_SESSION_STARTED

    let metricToUpdate = await factory.create('Metric')
    t.not(metricToUpdate.name, newName)

    const newAttributes = {
      name: newName,
    }

    await updateMetric(metricToUpdate, newAttributes)

    metricToUpdate = await reload(metricToUpdate, Metric)

    const expected = newName
    const actual = metricToUpdate.name

    t.is(actual, expected)
  }
)
