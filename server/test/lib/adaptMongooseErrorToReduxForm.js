import expect from 'expect';
import adaptMongooseErrorToReduxForm from 'server/lib/adaptMongooseErrorToReduxForm';

// TODO: rewrite the spec!
describe('adaptMongooseErrorToReduxForm', () => {
  describe('when looks like a proper Mongoose error', () => {
    it('returns a ReduxForm ready error object', () => {
      const errors = {
        email: {
          kind: 'required',
          message: 'Path `email` is required.',
          name: 'ValidatorError',
          path: 'email',
          properties: {
            message: 'Path `{PATH}` is required.',
            path: 'email',
            type: 'required',
          },
        },
        password: {
          kind: 'minlength',
          message: 'Path `password` (`asdf`) is shorter than the minimum allowed length (6).',
          name: 'ValidatorError',
          path: 'password',
          properties: {
            message: 'Path `{PATH}` (`{VALUE}`) is shorter than the minimum allowed length (6).',
            minlength: 6,
            path: 'password',
            type: 'minlength',
            value: 'asdf',
          },
          value: 'asdf',
        },
      }
      const expected = {
        email: 'is required',
        password: 'is shorter than the minimum allowed length (6)',
      }
      const actual = adaptMongooseErrorToReduxForm(errors)

      expect(actual).toEqual(expected)
    })
  })

  describe('when is not a proper Mongoose error', () => {
    it('returns false', () => {
      const errors = { email: 'what', password: 'broke' }
      const expected = false
      const actual = adaptMongooseErrorToReduxForm(errors)

      expect(actual).toEqual(expected)
    })
  })
})
