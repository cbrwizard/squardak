import faker from 'faker'
import { pluck } from 'ramda'
import test from 'ava'

import factory from 'server/test/factories/Square'
import Square from 'server/models/Square'
import increaseNumberOfSquaresTo from 'server/lib/admin/increaseNumberOfSquaresTo'

test('function throws an Error when the number of squares is not a perfect square', async (t) => {
  const numberOfSquares = 1000

  const error = await t.throws(increaseNumberOfSquaresTo(numberOfSquares))
  t.is(error.message, 'Number of squares must be a perfect square')
})

test('function creates the squares the current number of squares is 0', async (t) => {
  const newNumberOfSquares = 4

  await Square.remove({})

  const currentNumberOfSquares = await Square.count()
  t.is(currentNumberOfSquares, 0)

  await increaseNumberOfSquaresTo(newNumberOfSquares)

  const newSquares = await Square.find({})
  const rows = pluck('row', newSquares)
  const columns = pluck('column', newSquares)

  t.deepEqual(rows, [1, 1, 2, 2])
  t.deepEqual(columns, [1, 2, 1, 2])
})

test('function creates the squares the current number of squares is not 0', async (t) => {
  const newNumberOfSquares = 9

  await Square.remove({})

  await factory.create('Square', { column: 1, row: 1 })
  await factory.create('Square', { column: 1, row: 2 })
  await factory.create('Square', { column: 2, row: 1 })
  await factory.create('Square', { column: 2, row: 2 })

  const currentNumberOfSquares = await Square.count()
  t.is(currentNumberOfSquares, 4)

  await increaseNumberOfSquaresTo(newNumberOfSquares)

  const numberOfSquaresAfterRun = await Square.count()
  t.is(numberOfSquaresAfterRun, newNumberOfSquares)
})

test('function throws an Error when the new number of squares is less than the current', async (t) => {
  const newNumberOfSquares = 4

  await factory.createMany('Square', 9)

  const error = await t.throws(increaseNumberOfSquaresTo(newNumberOfSquares))
  t.is(error.message, 'New number of squares must be bigger than the existing')
})
