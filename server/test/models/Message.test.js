import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/Message'

test('default factory is valid', async (t) => {
  const message = await factory.build('Message')

  await message.validate()
  t.pass()
})

test('validation succeeds when message has a user', async (t) => {
  const user = await factory.create('User')
  try {
    await factory.create('Message', { _user: user._id })
    t.pass()
  } catch (e) {
    console.log(e)
  }
})

test('validation fails when message has no user', async (t) => {
  const message = await factory.build('Message', { _user: null })
  try {
    await message.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors._user.message, 'Path `_user` is required.')
  }
})

test('validation fails when message has no text', async (t) => {
  const message = await factory.build('Message', { text: null })

  try {
    await message.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.text.message, 'Path `text` is required.')
  }
})

test('validation fails when message has a text less than 2 chars', async (t) => {
  const message = await factory.build('Message', { text: 'w' })

  try {
    await message.validate()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.text.message)
  }
})

test('validation succeeds when message has a text longer than 2 chars', async (t) => {
  const message = await factory.build('Message', { text: faker.hacker.noun() })

  await message.validate()
  t.pass()
})
