import faker from 'faker'
import test from 'ava'

import User from 'server/models/User'
import factory from 'server/test/factories/User'

test('default factory is valid', async (t) => {
  const user = await factory.build('User')

  await user.validate()
  t.pass()
})

test('guest factory is valid', async (t) => {
  const user = await factory.build('Guest')

  await user.validate()
  t.pass()
})


test('validation succeeds when user is a guest and has no email', async (t) => {
  const user = await factory.build('User', { email: null, guest: true })

  await user.validate()
  t.pass()
})

test('validation fails when user is not a guest and has no email', async (t) => {
  const user = await factory.build('User', { email: null, guest: false })

  try {
    await user.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.email.message, 'Path `email` is required.')
  }
})

// TODO: fix with https://github.com/konsumer/mongoose-type-email/issues/6
test.skip("validation fails when user has an email but it's not correct", async (t) => {
  const fakeEmail = faker.hacker.noun()
  const user = await factory.build('User', { email: fakeEmail })

  try {
    await user.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.email.message, 'invalid email address')
  }
})

test('validation succeeds when user has a valid unique email', async (t) => {
  await User.remove({})

  const firstEmail = faker.internet.email()
  const secondEmail = faker.internet.email()
  const firstUser = await factory.create('User', { email: firstEmail })
  const secondUser = await factory.build('User', { email: secondEmail })

  t.not(firstUser.email, secondUser.email)

  await secondUser.validate()
  t.pass()
})

test('validation fails when user has a valid not unique email', async (t) => {
  await User.remove({})

  const sharedEmail = faker.internet.email()
  const firstUser = await factory.create('User', { email: sharedEmail })
  const secondUser = await factory.build('User', { email: sharedEmail })

  t.is(firstUser.email, secondUser.email)

  try {
    await secondUser.save()
    t.fail()
  } catch (e) {
    t.is(e.errors.email.message, 'email already exists')
  }
})

test('validation fails when user has a nick less than 2 chars long', async (t) => {
  const wrongNick = 'c'
  const user = await factory.build('User', { nick: wrongNick })

  try {
    await user.save()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.nick.message)
  }
})

test('validation fails when user has a valid not unique email', async (t) => {
  await User.remove({})

  const sharedNick = faker.hacker.noun()
  const firstUser = await factory.create('User', { nick: sharedNick })
  const secondUser = await factory.build('User', { nick: sharedNick })

  t.is(firstUser.nick, secondUser.nick)

  try {
    await secondUser.save()
    t.fail()
  } catch (e) {
    t.is(e.errors.nick.message, 'nick already exists')
  }
})

test('validation succeeds when user has a valid unique nick', async (t) => {
  await User.remove({})

  const firstNick = faker.hacker.noun()
  const secondNick = faker.hacker.verb()
  const firstUser = await factory.create('User', { nick: firstNick })
  const secondUser = await factory.build('User', { nick: secondNick })

  t.not(firstUser.nick, secondUser.nick)

  await secondUser.validate()
  t.pass()
})

test('validation succeeds when user is a guest and has no password', async (t) => {
  const user = await factory.build('User', { guest: true, password: null })

  await user.validate()
  t.pass()
})

test('validation fails when user is not a guest and has no password', async (t) => {
  const user = await factory.build('User', { guest: false, password: null })

  try {
    await user.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.password.message, 'Path `password` is required.')
  }
})

test('validation succeeds when user has a password longer than 6 chars', async (t) => {
  const user = await factory.build('User', { password: faker.internet.password() })

  await user.validate()
  t.pass()
})

test('validation fails when user has a color less than 2 chars long', async (t) => {
  const wrongColor = 'c'
  const user = await factory.build('User', { color: wrongColor })

  try {
    await user.save()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.color.message)
  }
})

test('validation succeeds when user has a color longer than 2 chars', async (t) => {
  const user = await factory.build('User', { color: faker.internet.color() })

  await user.validate()
  t.pass()
})
