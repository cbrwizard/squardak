import test from 'ava'

import factory from 'server/test/factories/Operation'

test('default factory is valid', async (t) => {
  const operation = await factory.build('Operation')

  await operation.validate()
  t.pass()
})

test('validation fails when operation has no user', async (t) => {
  const operation = await factory.build('Operation', { _user: null })

  try {
    await operation.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors._user.message, 'Path `_user` is required.')
  }
})

// TODO: solve this; possibly a Mongoose bug.
test.skip('validation fails when operation has no squares', async (t) => {
  const operation = await factory.build('Operation', { squares: null })

  try {
    await operation.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.squares.message, 'Path `squares` is required.')
  }
})
