import faker from 'faker'
import test from 'ava'

import factory from 'server/test/factories/Metric'
import { CORE_GAMEPLAY_ACCEPTANCE } from 'server/constants/metricNames'
import { SUCCEEDED } from 'server/constants/metricStatuses'

test('default factory is valid', async (t) => {
  const metric = await factory.build('Metric')

  await metric.validate()
  t.pass()
})

test('validation succeeds when metric has a user', async (t) => {
  const user = await factory.create('User')
  try {
    await factory.create('Metric', { _user: user._id })
    t.pass()
  } catch (e) {
    console.log(e)
  }
})

test('validation fails when metric has no user', async (t) => {
  const metric = await factory.build('Metric', { _user: null })
  try {
    await metric.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors._user.message, 'Path `_user` is required.')
  }
})

test('validation succeeds when metric has a name', async (t) => {
  try {
    await factory.create('Metric', { name: CORE_GAMEPLAY_ACCEPTANCE })
    t.pass()
  } catch (e) {
    console.log(e)
  }
})

test('validation fails when metric has a name not in enum list', async (t) => {
  const metric = await factory.build('Metric', { name: 'vagueThing' })

  try {
    await metric.validate()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.name.message)
  }
})

test('validation succeeds when metric has a name from enum list', async (t) => {
  const metric = await factory.build('Metric', { name: CORE_GAMEPLAY_ACCEPTANCE })

  await metric.validate()
  t.pass()
})

test('validation fails when metric has no status', async (t) => {
  const metric = await factory.build('Metric', { status: null })

  try {
    await metric.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.status.message, 'Path `status` is required.')
  }
})

test('validation fails when metric has a status not in enum list', async (t) => {
  const metric = await factory.build('Metric', { status: 'notStarted' })

  try {
    await metric.validate()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.status.message)
  }
})

test('validation succeeds when metric has a status from enum list', async (t) => {
  const metric = await factory.build('Metric', { status: SUCCEEDED })

  await metric.validate()
  t.pass()
})
