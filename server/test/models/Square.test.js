import faker from 'faker'
import test from 'ava'

import Square from 'server/models/Square'
import factory from 'server/test/factories/Square'

test('default factory is valid', async (t) => {
  const square = await factory.build('Square')

  await square.validate()
  t.pass()
})

test('validation fails when a square with the same column and row already exists', async (t) => {
  await Square.remove({})

  const existingColumn = 1
  const existingRow = 1
  await factory.create('Square', { column: existingColumn, row: existingRow })
  const secondSquare = await factory.build('Square', { column: existingColumn, row: existingRow })

  try {
    await secondSquare.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.column.message, 'Combination of column and row already exists')
  }
})

test('validation fails when square has no column', async (t) => {
  const square = await factory.build('Square', { column: null })

  try {
    await square.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.column.message, 'Path `column` is required.')
  }
})

test('validation fails when square has no row', async (t) => {
  const square = await factory.build('Square', { row: null })

  try {
    await square.validate()
    t.fail()
  } catch (e) {
    t.is(e.errors.row.message, 'Path `row` is required.')
  }
})

test('validation fails when square has a column smaller than 1', async (t) => {
  const square = await factory.build('Square', { column: 0 })

  try {
    await square.validate()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.column.message)
  }
})

test('validation fails when square has a row smaller than 1', async (t) => {
  const square = await factory.build('Square', { row: -2 })

  try {
    await square.validate()
    t.fail()
  } catch (e) {
    t.truthy(e.errors.row.message)
  }
})

test('validation succeeds when square has a column and a row bigger than 0', async (t) => {
  const square = await factory.build('Square', { column: 1, row: 3 })

  await square.validate()
  t.pass()
})

test('validation succeeds when square has no user', async (t) => {
  const square = await factory.build('Square', { _user: null })

  await square.validate()
  t.pass()
})
