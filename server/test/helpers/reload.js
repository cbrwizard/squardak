// Is responsible for returning a fresh copy of a document.
const reload = (doc, model) =>
  model.findOne({ _id: doc._id })

export default reload
