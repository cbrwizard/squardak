import { createAndPopulate } from 'server/queries/operations'
import setUpRavenClient from 'server/lib/setUpRavenClient'
import getLogger from 'server/lib/getLogger'

const logger = getLogger()
const Raven = setUpRavenClient()

/**
 * Is responsible for creating an Operation record.
 */
export const createOperation = async (attributes) => {
  try {
    const saveResult = await createAndPopulate(attributes)
    logger.info({ attributes }, 'Created an operation')

    return saveResult
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}
