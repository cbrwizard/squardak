import setUpRavenClient from 'server/lib/setUpRavenClient'
import getLogger from 'server/lib/getLogger'
import assignRandomSquareToUser
  from 'server/services/squares/assignRandomSquareToUser'
import shouldUserReceiveRandomSquare
  from 'server/policies/shouldUserReceiveRandomSquare'
import serializeGameSession from 'server/serializers/game'
import { io } from 'server/app'
import modelIdFromModelOrId from 'server/lib/modelIdFromModelOrId'
import { GAME_SESSION_STARTED } from 'server/constants/metricNames'
import { SUCCEEDED } from 'server/constants/metricStatuses'
import { updateOrCreateMetric } from 'server/services/metrics'
import { createEvent } from 'server/services/gaEvents'
import shouldCreateGameSessionStartedMetric
  from 'server/policies/shouldCreateGameSessionStartedMetric'

const logger = getLogger()
const Raven = setUpRavenClient()

const handleRandomSquareForUser = async (user) => {
  if (await shouldUserReceiveRandomSquare(user)) {
    return assignRandomSquareToUser(user, io)
  }
  return false
}

const handleGameSessionStartedMetric = async (userId) => {
  if (await shouldCreateGameSessionStartedMetric(userId)) {
    const attributesToFind = {
      _user: userId,
      name: GAME_SESSION_STARTED,
    }
    const newAttributes = { status: SUCCEEDED }

    await createEvent(GAME_SESSION_STARTED, userId)
    await updateOrCreateMetric(attributesToFind, newAttributes)
  }
}

export const initializeGame = async (user) => {
  try {
    await handleRandomSquareForUser(user)
    const game = await serializeGameSession(user)
    const userId = modelIdFromModelOrId(user)

    logger.info({ game }, 'Initialized a game')

    // No need to wait for it.
    handleGameSessionStartedMetric(userId)

    return game
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}
