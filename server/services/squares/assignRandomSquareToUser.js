import { populate, randomSquare } from 'server/queries/squares'
import assignSquareToUser from 'server/services/squares/assignSquareToUser'
import setUpRavenClient from 'server/lib/setUpRavenClient'
import broadcastAppendTerritory
  from 'server/broadcasts/broadcastAppendTerritory'
import { SQUARE_ASSIGN_REASON_FREE } from 'shared/constants/game'
import getLogger from 'server/lib/getLogger'
import serializeTerritory from '../../serializers/territory'

const logger = getLogger()
const Raven = setUpRavenClient()

const assignRandomSquareToUser = async (user, io = false) => {
  try {
    const squareToAssign = await randomSquare()

    const saveResult = await assignSquareToUser(squareToAssign, user, {
      reason: SQUARE_ASSIGN_REASON_FREE,
    })

    if (io) {
      const populatedSquare = await populate(saveResult)
      const territory = await serializeTerritory([populatedSquare], user)
      broadcastAppendTerritory(io, territory)
    }

    return saveResult
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}

export default assignRandomSquareToUser
