import { ObjectID } from 'mongodb'


import { update, updateById } from 'server/queries/squares'
import setUpRavenClient from 'server/lib/setUpRavenClient'
import getLogger from 'server/lib/getLogger'
import modelIdFromModelOrId from 'server/lib/modelIdFromModelOrId'

const logger = getLogger()
const Raven = setUpRavenClient()

const assignSquareToUser = async (
  squareOrSquareId,
  userOrUserId,
  options = { reason: 'no reason' }
) => {
  try {
    const updateFunction = ObjectID.isValid(squareOrSquareId) ? updateById : update
    const userId = modelIdFromModelOrId(userOrUserId)

    const saveResult = await updateFunction(squareOrSquareId, { _user: userId.toString() })

    logger.info(
      { saveResult },
      `Assigned a square ${saveResult._id} to user ${userId} at location (${saveResult.column}:${saveResult.row}) because ${options.reason}`
    )

    return saveResult
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}

export default assignSquareToUser
