import { uniq } from 'ramda'

import setUpRavenClient from 'server/lib/setUpRavenClient'
import getCapturableSquaresPositions
  from 'shared/services/getCapturableSquaresPositions'
import { findByPositions, userSquares } from 'server/queries/squares'
import { findById } from 'server/queries/users'
import canUserPerformOperation from 'server/policies/canUserPerformOperation'
import assignSquareToUser from 'server/services/squares/assignSquareToUser'
import { createOperation } from 'server/services/operations'
import squaresToCaptureMax from 'server/policies/squaresToCaptureMax'
import getLogger from 'server/lib/getLogger'
import { SQUARE_ASSIGN_REASON_CAPTURE } from 'shared/constants/game'
import {
  handleSquareCapturedMetric,
  handleTurnsMadeMetrics,
  handleCoreGameplayAcceptanceMetric,
} from 'server/services/metrics'

const logger = getLogger()
const Raven = setUpRavenClient()

const validateCapturable = async (selectedSquares, currentUserSquares) => {
  const capturableSquaresPositions = await getCapturableSquaresPositions(
    currentUserSquares.map(square => square.position)
  )

  const allSelectedSquaresAreCapturable = selectedSquares.every(
    selectedSquare =>
      !!capturableSquaresPositions.find(
        capturableSquarePosition =>
          selectedSquare.position === capturableSquarePosition
      )
  )

  if (!allSelectedSquaresAreCapturable) {
    throw new Error('Not all squares are capturable')
  }
}

const validateUserOperations = async (userId) => {
  const user = await findById(userId)
  const userPerformedMaxOperationsAlready = !await canUserPerformOperation(
    user
  )

  if (userPerformedMaxOperationsAlready) {
    throw new Error('Cannot make more turns today.')
  }
}

const validateNumberOfSquaresToCapture = async (selectedSquares, userId) => {
  const numberOfSquaresToCaptureMax = await squaresToCaptureMax(userId)

  if (selectedSquares.length > numberOfSquaresToCaptureMax) {
    throw new Error("Can't capture that many squares this turn")
  }
}

const validatePlan = async ({
  currentUserSquares,
  userId,
  selectedSquares,
}) => {
  await validateCapturable(selectedSquares, currentUserSquares)
  await validateUserOperations(userId)
  await validateNumberOfSquaresToCapture(selectedSquares, userId)
}

const handleSuccess = async (userId, selectedSquares) => {
  // TODO: need to change this so it runs in a single query.
  const assignmentPromises = selectedSquares.map(async (square) => {
    await assignSquareToUser(square._id, userId, {
      reason: SQUARE_ASSIGN_REASON_CAPTURE,
    })
  })

  return Promise.all(assignmentPromises)
}

const handleMetrics = async (userId) => {
  await handleCoreGameplayAcceptanceMetric(userId)
  await handleSquareCapturedMetric(userId)
  await handleTurnsMadeMetrics(userId)
}

/**
 * Performs a plan by transforming it into an Operation.
 * Right now the only plan possible is Capture, so it's like performCapture.
 * Has dependency injection just for testing.
 */
export const performPlan = async (userId, selectedSquaresPositions) => {
  try {
    const currentUserSquares = await userSquares(userId)
    const selectedSquares = await findByPositions(selectedSquaresPositions)

    await validatePlan({
      currentUserSquares,
      selectedSquares,
      userId,
    })

    await handleSuccess(userId, selectedSquares)

    // No need to wait for this.
    handleMetrics(userId)

    const affectedUsers = uniq(
      selectedSquares.filter(square => square._user).map(square => square._user)
    )

    const operation = await createOperation({
      _user: userId,
      affectedUsers,
      squares: selectedSquares,
    })

    logger.info({ selectedSquaresPositions, userId }, 'Performed a plan')

    return operation
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}
