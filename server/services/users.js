import { isNilOrEmpty } from 'ramdasauce'
import { equals, omit } from 'ramda'

import { create, update, findById } from 'server/queries/users'
import setUpRavenClient from 'server/lib/setUpRavenClient'
import { serializeUserForSquare } from 'server/serializers/user'
import broadcastUpdateUserInSquares from 'server/broadcasts/broadcastUpdateUserInSquares'
import generateNick from 'server/generators/generateNick'
import generateColor from 'server/generators/generateColor'
import getLogger from 'server/lib/getLogger'
import ensurePlainObject from 'server/lib/mongoose/ensurePlainObject'

const logger = getLogger()
const Raven = setUpRavenClient()

// Is responsible for handling the use case when a guest converts to a
// full user.
const attributesWithHandledGuestConversion = (user, attributes) => {
  const wasGuestButNowConvertingToUser =
    user.guest &&
    !isNilOrEmpty(attributes.email) &&
    !isNilOrEmpty(attributes.password)

  if (wasGuestButNowConvertingToUser) {
    return Object.assign({}, attributes, { guest: false })
  }
  return attributes
}

const shouldGenerateField = (user, attributes, field) => {
  const fieldPresentInRecord = !isNilOrEmpty(user[field])
  const newFieldInAttributes = !isNilOrEmpty(attributes[field])
  const nullFieldInAttributes = attributes[field] === null
  const noFieldInAttributes = attributes[field] === undefined
  return (
    !newFieldInAttributes &&
    ((!fieldPresentInRecord && !nullFieldInAttributes && noFieldInAttributes) ||
      (!fieldPresentInRecord && nullFieldInAttributes) ||
      (fieldPresentInRecord && nullFieldInAttributes))
  )
}

// Is responsible for making sure that a generated nick is present in
// user attributes when needed.
// @note: user is empty when creating a new record
const attributesWithHandledNickGeneration = (user, attributes) => {
  if (shouldGenerateField(user, attributes, 'nick')) {
    return Object.assign({}, attributes, { nick: generateNick() })
  }
  return attributes
}

// Is responsible for making sure that a color is present in
// user attributes when needed.
// @note: user is empty when creating a new record
const attributesWithHandledColorGeneration = (user, attributes) => {
  if (shouldGenerateField(user, attributes, 'color')) {
    return Object.assign({}, attributes, { color: generateColor() })
  }
  return attributes
}

export const createUser = async (
  attributes,
  options = { handleEmptyColor: false, handleEmptyNick: false }
) => {
  try {
    let nickHandledAttributes
    let colorHandledAttributes
    if (options.handleEmptyNick) {
      nickHandledAttributes = attributesWithHandledNickGeneration(
        {},
        attributes
      )
    } else {
      nickHandledAttributes = attributes
    }

    if (options.handleEmptyColor) {
      colorHandledAttributes = attributesWithHandledColorGeneration(
        {},
        nickHandledAttributes
      )
    } else {
      colorHandledAttributes = nickHandledAttributes
    }
    const saveResult = await create(colorHandledAttributes)
    logger.info(
      { colorHandledAttributes: omit(['password'], colorHandledAttributes) },
      'Created a user'
    )

    return saveResult
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}

export const updateUser = async (user, attributes, io) => {
  try {
    const guestHandledAttributes = attributesWithHandledGuestConversion(
      user,
      attributes
    )
    const nickHandledAttributes = attributesWithHandledNickGeneration(
      user,
      guestHandledAttributes
    )
    const colorHandledAttributes = attributesWithHandledColorGeneration(
      user,
      nickHandledAttributes
    )

    const previousUserAttributes = ensurePlainObject(user)
    const updatedUser = await update(user, colorHandledAttributes)
    logger.info(
      { colorHandledAttributes: omit(['password'], colorHandledAttributes) },
      'Updated a user'
    )

    if (io && !equals(previousUserAttributes, ensurePlainObject(updatedUser))) {
      const serializedUser = await serializeUserForSquare(updatedUser)
      broadcastUpdateUserInSquares(io, serializedUser)
    }

    return updatedUser
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}

export const updateUserById = async (id, attributes, io) => {
  try {
    const user = await findById(id)
    return updateUser(user, attributes, io)
  } catch (err) {
    Raven.captureException(err)
    logger.error({ err, stack: err.stack })
    throw err
  }
}
