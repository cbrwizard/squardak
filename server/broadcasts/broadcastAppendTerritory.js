import { APPEND_TERRITORY } from '../../client/constants/redux/territories'
import broadcast from 'server/broadcasts/broadcast'

/**
 * Is responsible for broadcasting the appending a territory event.
 */
const broadcastAppendTerritory = async (io, payload) =>
  broadcast(io, payload, APPEND_TERRITORY)

export default broadcastAppendTerritory
