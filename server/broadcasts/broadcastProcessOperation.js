import { PROCESS_OPERATION } from 'client/constants/redux/operations'
import broadcast from 'server/broadcasts/broadcast'

/**
 * Is responsible for broadcasting the operation to process by the clients.
 */
const broadcastProcessOperation = async (io, operation) =>
  broadcast(io, operation, PROCESS_OPERATION)

export default broadcastProcessOperation
