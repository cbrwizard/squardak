import { UPDATE_USER_IN_SQUARES } from 'client/constants/redux/users'
import broadcast from 'server/broadcasts/broadcast'

/**
 * Is responsible for broadcasting the update user in squares event.
 */
const broadcastUpdateUserInSquares = async (io, user) =>
  broadcast(io, user, UPDATE_USER_IN_SQUARES)

export default broadcastUpdateUserInSquares
