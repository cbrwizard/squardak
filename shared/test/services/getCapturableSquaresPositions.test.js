/* eslint no-unused-vars: 0 */

import test from 'ava'
import { difference } from 'ramda'

import getCapturableSquaresPositions
  from 'shared/services/getCapturableSquaresPositions'

test('returns an empty array when a user has no squares', async (t) => {
  const currentUserSquarePositions = []

  const expected = []
  const actual = await getCapturableSquaresPositions(currentUserSquarePositions)

  t.deepEqual(difference(actual, expected), [])
})

test('returns a correct array of capturable squares coordinates when a user has squares: 4 squares scenario', async (t) => {
  const currentUserSquarePositions = ['1-1']

  const expected = ['1-2', '2-2', '2-1']

  const actual = await getCapturableSquaresPositions(currentUserSquarePositions)

  t.deepEqual(difference(actual, expected), [])
})

test('returns a correct array of capturable squares coordinates when a user has squares: 9 squares scenario', async (t) => {
  const currentUserSquarePositions = ['3-1']

  const expected = ['2-1', '2-2', '3-2', '4-1', '4-2']
  const actual = await getCapturableSquaresPositions(currentUserSquarePositions)

  t.deepEqual(difference(actual, expected), [])
})

test('returns a correct array of capturable squares coordinates when a user has squares: 16 squares scenario', async (t) => {
  const currentUserSquarePositions = ['2-2', '3-3']

  const expected = [
    '1-1',
    '1-2',
    '1-3',
    '2-1',
    '2-3',
    '2-4',
    '3-1',
    '3-2',
    '3-4',
    '4-2',
    '4-3',
    '4-4',
  ]

  const actual = await getCapturableSquaresPositions(currentUserSquarePositions)

  t.deepEqual(difference(actual, expected), [])
})
