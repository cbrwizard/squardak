import { WORLD_OFFSET } from 'shared/constants/game'
import repositionTutorial from '../../lib/repositionTutorial'

/**
 * Is responsible for manipulating the Phaser camera.
 * All interaction with the Phaser camera should go through this class.
 */
class CameraManipulator {
  constructor(camera) {
    this.camera = camera
  }

  move(direction, amount) {
    this.camera[direction] += amount
    repositionTutorial()
  }

  /*
    We want to make a camera bigger than the world so that the ui
    doesn't make some squares not visible.
   */
  makeCameraBiggerThanWorld(worldSize) {
    const cameraSize = worldSize + WORLD_OFFSET * 2

    const topCameraPoint = WORLD_OFFSET * -1
    const leftCameraPoint = topCameraPoint
    this.changeBoundsTo(topCameraPoint, leftCameraPoint, cameraSize, cameraSize)
  }

  /*
    We want to center the camera to the user's square.
    TODO: calculate the position once actually.
   */
  setInitialCameraPosition() {
    const squareToFocusOn = this.camera.game.findSquareByPosition(
      this.camera.game.picsWarsState.positionCameraAtSquarePosition
    )
    this.focusOn(squareToFocusOn)
  }

  focusOn(sprite) {
    this.camera.focusOn(sprite)
  }

  changeBoundsTo(top, left, width, height) {
    this.camera.bounds.setTo(top, left, width, height)
  }
}

export default CameraManipulator
