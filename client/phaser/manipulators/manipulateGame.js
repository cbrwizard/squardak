import getRunningGame from 'client/phaser/lib/getRunningGame'
import { SQUARE_SIZE } from 'shared/constants/game'
import { convertUUIDToURL } from '../../lib/uploadcare'

/**
 * Is responsible for setting the territory's user's data in all squares which
 * are included in that territory.
 * Works both for current and others.
 * TODO: refactor this.
 */
export const setTerritoryDataInSquares = ({
  color,
  copiedImageUUID,
  imageUUID,
  imagePositions,
  linkText,
  linkUrl,
  nick,
  squarePositions,
  userId,
}) => {
  const game = getRunningGame()
  if (!game) {
    return false
  }

  const { squaresGroup } = game.groups
  let rowLength
  let imageKey
  let imageUUIDToUse
  if (copiedImageUUID) {
    imageUUIDToUse = copiedImageUUID
  } else if (imageUUID) {
    imageUUIDToUse = imageUUID
  }

  if (squarePositions.length && imageUUIDToUse) {
    // TODO: move this to a game.
    // debugger
    const resize = game.getResizeFromPositions(imagePositions)
    const frameMax = resize.width / SQUARE_SIZE * (resize.height / SQUARE_SIZE)
    imageKey = `${imageUUIDToUse}-${resize.width}x${resize.height}`

    game.load.spritesheet(
      imageKey,
      convertUUIDToURL(imageUUIDToUse, {
        resize,
      }),
      SQUARE_SIZE,
      SQUARE_SIZE,
      frameMax
    )
    game.load.start()

    rowLength =
      imagePositions.bottomRight.column - imagePositions.topLeft.column + 1
  }

  squaresGroup.forEach((square) => {
    let frame = 0
    if (squarePositions.length && imageUUIDToUse) {
      // TODO: move this to a lib.
      const columnDif = square.record.column - imagePositions.topLeft.column
      const rowDif = square.record.row - imagePositions.topLeft.row

      frame = rowDif * rowLength + columnDif
    }

    if (squarePositions.includes(square.picsWarsPosition)) {
      square.updateAttributes({
        mergeToData: { dataLoaded: true, frame },
        mergeToRecord: {
          color,
          imageKey,
          imageUUID: imageUUIDToUse,
          linkText,
          linkUrl,
          nick,
          userId,
        },
      })
    }
  })

  return true
}

const resetPicsWarsStatePropInAllSquares = (prop) => {
  const game = getRunningGame()
  if (!game) {
    return false
  }

  const { squaresGroup } = game.groups

  squaresGroup.forEach((square) => {
    if (square.data[prop]) {
      square.updateAttributes({ mergeToData: { [prop]: false } })
    }
  })

  return true
}

export const unselectAllSquares = () =>
  resetPicsWarsStatePropInAllSquares('selected')

/**
 * Is responsible for resetting (removing and enabling it back where needed)
 * capturable for all squares.
 */
export const resetCapturableSquares = () => {
  const game = getRunningGame()
  if (!game) {
    return false
  }

  resetPicsWarsStatePropInAllSquares('capturable')
  const { capturableSquaresPositions } = game.picsWarsState

  capturableSquaresPositions.forEach((squarePosition) => {
    const square = game.findSquareByPosition(squarePosition)

    // It can be out of game board bounds === not found === undefined.
    if (square) {
      square.updateAttributes({ mergeToData: { capturable: true } })
    }
  })

  return true
}

/**
 * Is responsible for updating all squares in a game with the same attributes.
 */
export const updateSquaresRecords = ({
  squarePositions,
  mergeToRecord,
  mergeToData,
}) => {
  const game = getRunningGame()
  if (!game) {
    return false
  }

  squarePositions.forEach((squarePosition) => {
    const square = game.findSquareByPosition(squarePosition)
    square.updateAttributes({
      mergeToData,
      mergeToRecord,
    })
  })

  return true
}

export const firstUsersSquare = () => {
  const game = getRunningGame()
  if (!game) {
    return false
  }
  const { squaresGroup } = game.groups

  return squaresGroup.children.find(square => square.data.belongsToCurrentUser)
}

export const firstCapturableSquare = () => {
  const game = getRunningGame()
  if (!game) {
    return false
  }
  const { squaresGroup } = game.groups

  return squaresGroup.children.find(square => square.data.capturable)
}
