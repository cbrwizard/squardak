import { contains } from 'ramda'
import 'pixi'
import 'p2'
import Phaser from 'phaser'

import MainState from 'client/phaser/states/MainState'
import directionToMoveCamera from 'client/phaser/lib/directionToMoveCamera'
import { SQUARE_SIZE } from 'shared/constants/game'
import {
  setTerritoryDataInSquares,
} from 'client/phaser/manipulators/manipulateGame'

/**
 * Is responsible for managing the PicsWars Game.
 * TODO: resize game on window resize?
 * I think that a Game class should have a onResize(newWidth, newHeight)
 * function which gets called when something above detects a resize.
 * This one shouldn't detect the resize by itself.
 * TODO: figure out the best way to differentiate responsibility between the
 * Game and the State.
 */
class MainGame extends Phaser.Game {
  constructor(
    options = {
      parentDOMId: '',
    }
  ) {
    const docElement = document.documentElement
    const width = docElement.clientWidth
    const height = docElement.clientHeight

    const gameConfig = {
      antialias: false,
      height,
      multiTexture: false,
      parent: options.parentDOMId,
      renderer: Phaser.AUTO,
      width,
    }
    super(gameConfig)

    this.forceSingleUpdate = true

    // TODO: probably move this to a separate key.
    this.onSquareMouseOver = options.onSquareMouseOver
    this.onSquareDeselect = options.onSquareDeselect
    this.onSquareMouseOver = options.onSquareMouseOver
    this.onSquareMouseOut = options.onSquareMouseOut
    this.onSquareSelect = options.onSquareSelect
    this.onReady = options.onReady

    // To be used later, not accessible right now
    this.cursors = null
    this.cameraManipulator = null

    this.picsWarsState = {
      canMakeTurnsToday: options.canMakeTurnsToday,
      canSelectSquares: options.canSelectSquares,
      capturableSquaresPositions: options.capturableSquaresPositions,
      currentUser: options.currentUser,
      currentUserSquarePositions: options.currentUserSquarePositions,
      isWaitingForResults: options.isWaitingForResults,
      mouseScrollDirection: false,
      positionCameraAtSquarePosition: options.positionCameraAtSquarePosition,
      squaresCount: options.squaresCount,
      territories: options.territories,
    }

    this.groups = {}

    this.state.add('Main', MainState)
    this.state.start('Main')

    // At this point, the game data might be proper, but the visual state
    // is still lagging and not rendered, thus the game is not yet ready.
  }

  changePicsWarsState(key, value) {
    this.picsWarsState[key] = value
  }

  handleMouseMove() {
    this.picsWarsState.mouseScrollDirection = directionToMoveCamera(
      this.input.mousePointer
    )
  }

  // TODO: unite with handleMouseScrolling
  handleCursorsPush() {
    const CURSOR_SCROLL_SPEED = 8

    if (this.cursors.up.isDown) {
      this.cameraManipulator.move('y', -1 * CURSOR_SCROLL_SPEED)
    } else if (this.cursors.down.isDown) {
      this.cameraManipulator.move('y', CURSOR_SCROLL_SPEED)
    }

    if (this.cursors.left.isDown) {
      this.cameraManipulator.move('x', -1 * CURSOR_SCROLL_SPEED)
    } else if (this.cursors.right.isDown) {
      this.cameraManipulator.move('x', CURSOR_SCROLL_SPEED)
    }
  }

  handleMouseScrolling() {
    const MOUSE_SCROLL_SPEED = 5
    const direction = this.picsWarsState.mouseScrollDirection

    if (!direction || !this.isCanvasHovered()) return

    if (direction.x > 0) {
      this.cameraManipulator.move('x', MOUSE_SCROLL_SPEED)
    } else if (direction.x < 0) {
      this.cameraManipulator.move('x', -1 * MOUSE_SCROLL_SPEED)
    }

    if (direction.y > 0) {
      this.cameraManipulator.move('y', MOUSE_SCROLL_SPEED)
    } else if (direction.y < 0) {
      this.cameraManipulator.move('y', -1 * MOUSE_SCROLL_SPEED)
    }
  }

  findSquareByPosition(position) {
    return this.groups.squaresGroup.children.find(
      child => child.picsWarsPosition === position
    )
  }

  isCanvasHovered() {
    return (
      this.canvas &&
      this.canvas.parentElement.querySelector(':hover') === this.canvas
    )
  }

  // TODO: consider changing the logic here entirely so it actually goes through
  // currentUserSquares and applies an effect 'belongsToCurrentUser' to those.
  // Need to make sure though that it optimizes things.
  doesSquareBelongToCurrentUser(squarePosition) {
    return contains(
      squarePosition,
      this.picsWarsState.currentUserSquarePositions
    )
  }

  // TODO: consider changing the logic as above.
  isSquareOneOfCapturable(squarePosition) {
    // console.log('squarePosition')
    // console.log(squarePosition)
    return contains(
      squarePosition,
      this.picsWarsState.capturableSquaresPositions
    )
  }

  // TODO: move to a lib
  getResizeFromPositions({ bottomRight, topLeft }) {
    const columnDif = bottomRight.column - topLeft.column + 1
    const rowDif = bottomRight.row - topLeft.row + 1
    const resize = {
      height: rowDif * SQUARE_SIZE,
      width: columnDif * SQUARE_SIZE,
    }

    return resize
  }
}

export default MainGame
