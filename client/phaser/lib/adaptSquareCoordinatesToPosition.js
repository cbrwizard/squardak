/**
 * Is responsible for converting a Square's position string
 * into a position object.
 * @example
 * adaptSquareCoordinatesToPosition({ column: 15, row: 37 })
 * // '15-37'
 */
const adaptSquareCoordinatesToPosition = ({ column, row }) => `${column}-${row}`

export default adaptSquareCoordinatesToPosition
