const CLOSINESS_GAP = 0.03

const shouldCameraBePositivelyMoved = (coordinate, edge) =>
  coordinate / edge > (1 - CLOSINESS_GAP)

const shouldCameraBeNegativelyMoved = (coordinate, edge) =>
  coordinate / edge < CLOSINESS_GAP

/**
 * Is responsible for telling which coordinate and in which direction should
 * the camera be moved.
 * 0 = no move needed.
 * 1 = a positive change is needed.
 * -1 = a negative change is needed.
 * Doesn't specify the magnitude by which a camera should be moved.
 */
const directionToMoveCamera = ({ x, y, game }) => {
  const result = { x: 0, y: 0 }

  if (shouldCameraBePositivelyMoved(x, game.width)) {
    result.x = 1
  } else if (shouldCameraBeNegativelyMoved(x, game.width)) {
    result.x = -1
  }

  if (shouldCameraBePositivelyMoved(y, game.height)) {
    result.y = 1
  } else if (shouldCameraBeNegativelyMoved(y, game.height)) {
    result.y = -1
  }

  return result
}

export default directionToMoveCamera
