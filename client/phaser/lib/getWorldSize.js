import {
  SQUARE_BORDER_WIDTH,
  SQUARE_SIZE,
} from 'shared/constants/game'

/**
 * Is responsible for returning the size of the world, defined by the number
 * and size of squares.
 * We return the sqrt since the number of squares is a square.
 */
const getWorldSize = numberOfSquares =>
  (Math.sqrt(numberOfSquares) * SQUARE_SIZE) +
    (SQUARE_BORDER_WIDTH * Math.sqrt(numberOfSquares))

export default getWorldSize
