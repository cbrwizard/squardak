import { SQUARE_SIZE } from 'shared/constants/game'

/**
 * Is responsible for converting a square's position (column, row) into Phaser
 * coordinates on the canvas.
 */
const adaptSquareCoordinatesToPhaserCoordinates = ({ column, row }) => (
  {
    x: SQUARE_SIZE * (column - 1),
    y: SQUARE_SIZE * (row - 1),
  }
)

export default adaptSquareCoordinatesToPhaserCoordinates
