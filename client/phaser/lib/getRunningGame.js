/**
 * Is responsible for returning the initiated Phaser Game object.
 */
const getRunningGame = () => Phaser.GAMES[Phaser.GAMES.length - 1]

export default getRunningGame
