import Phaser from 'phaser'
import throttle from 'lodash.throttle'

import isProduction from 'shared/lib/isProduction'
import getWorldSize from 'client/phaser/lib/getWorldSize'
import {
  SQUARE_BORDER_WIDTH,
  DEFAULT_BORDER_COLOR,
  STAGE_BACKGROUND_COLOR,
  SQUARE_SIZE,
} from 'shared/constants/game'
import CameraManipulator from 'client/phaser/manipulators/CameraManipulator'
import adaptSquareCoordinatesToPhaserCoordinates
  from 'client/phaser/lib/adaptSquareCoordinatesToPhaserCoordinates'
import adaptSquareCoordinatesToPosition
  from 'client/phaser/lib/adaptSquareCoordinatesToPosition'
import Square from 'client/phaser/graphics/Square'
import colors from 'shared/constants/colors'
import crownIcon from 'client/images/icons/crown.png'
import capturableIcon from 'client/images/icons/capturable.png'
import selectedIcon from 'client/images/icons/selected.png'

const WHITE_COLOR = '#ffffff'

/**
* Is responsible for setting the default state of the game.
* // TODO: refactor this superclass.
*/
class MainState extends Phaser.State {
  init() {
    this.stage.backgroundColor = STAGE_BACKGROUND_COLOR

    // Assigning it here because in Game constructor camera is null.
    this.game.cameraManipulator = new CameraManipulator(this.game.camera)
    this.throttledHandleControls = throttle(() => {
      this.handleControls()
    }, 10)

    // Info needed just for iteratively creating squares.
    this.squaresCreatedInfo = {
      currentColumn: 1,
      currentRow: 1,
      numberOfSquaresCreated: 0,
      numberOfSquaresToCreate: this.game.picsWarsState.squaresCount,
    }

    // Assigning it here because in game's constructor it's still undefined
    // TODO: redraw only affected squares.
    this.game.load.onLoadComplete.add(() => {
      if (this.game.groups.squaresGroup) {
        this.game.groups.squaresGroup.children.forEach(square =>
          square.redraw()
        )
      }
    })
  }

  preload() {
    // So that we can load images from uploadcare
    this.game.load.crossOrigin = 'anonymous'
    // TODO: use a spritesheet instead
    this.game.load.image('crownIcon', crownIcon)
    this.game.load.image('capturableIcon', capturableIcon)
    this.game.load.image('selectedIcon', selectedIcon)
  }

  // TODO: move this to a creator function or something.
  createSquaresOnEachRow() {
    if (
      this.squaresCreatedInfo.numberOfSquaresCreated ===
      this.squaresCreatedInfo.numberOfSquaresToCreate
    ) {
      return
    }

    this.createSquaresOnARow()
    this.squaresCreatedInfo.currentRow += 1

    // If we have reached the totally new row, we need to start from the first column again.
    if (this.squaresCreatedInfo.currentRow >= 1) {
      this.squaresCreatedInfo.currentColumn = 1
    } else {
      this.squaresCreatedInfo.currentColumn = 1
    }

    return this.createSquaresOnEachRow()
  }

  createSquaresOnARow() {
    // if reached the most right column, stop and reset the column or if done creating.
    if (
      this.squaresCreatedInfo.numberOfSquaresCreated ===
        this.squaresCreatedInfo.numberOfSquaresToCreate ||
      this.squaresCreatedInfo.currentColumn >
        Math.sqrt(this.squaresCreatedInfo.numberOfSquaresToCreate)
    ) {
      return
    }

    const record = {
      column: this.squaresCreatedInfo.currentColumn,
      position: adaptSquareCoordinatesToPosition({
        column: this.squaresCreatedInfo.currentColumn,
        row: this.squaresCreatedInfo.currentRow,
      }),
      row: this.squaresCreatedInfo.currentRow,
    }

    const { x, y } = adaptSquareCoordinatesToPhaserCoordinates(record)

    const recordColor = record.color ? record.color : WHITE_COLOR
    const key = this.game.picsWarsState.graphicsTextures.find(
      texture => texture.color === recordColor
    )
    const square = new Square({
      game: this.game,
      key,
      onSquareDeselect: this.game.onSquareDeselect,
      onSquareMouseOut: this.game.onSquareMouseOut,
      onSquareMouseOver: this.game.onSquareMouseOver,
      onSquareSelect: this.game.onSquareSelect,
      record,
      x,
      y,
    })
    square.bringToTop()

    this.game.groups.squaresGroup.add(square)

    this.squaresCreatedInfo.numberOfSquaresCreated += 1
    this.squaresCreatedInfo.currentColumn += 1

    return this.createSquaresOnARow()
  }

  create() {
    this.game.picsWarsState.graphicsTextures = []
    this.setUpGraphics()
    this.game.groups.squaresGroup = this.game.add.group()

    this.createSquaresOnEachRow()

    this.setUpWorldView()
    this.setUpControls()

    // At this point, the game is fully ready and rendered.
    this.game.onReady()
  }

  update() {
    this.throttledHandleControls()
  }

  handleControls() {
    this.game.handleCursorsPush()
    this.game.handleMouseScrolling()
  }

  render() {
    if (!isProduction) {
      this.game.debug.cameraInfo(this.game.camera, 332, 32)
    }
  }

  setUpWorldView() {
    const worldSize = getWorldSize(this.game.picsWarsState.squaresCount)

    // Width and height are the same because we want a square.
    this.game.world.setBounds(0, 0, worldSize, worldSize)
    this.game.cameraManipulator.makeCameraBiggerThanWorld(worldSize)
    this.game.cameraManipulator.setInitialCameraPosition()
  }

  setUpControls() {
    this.game.input.addMoveCallback(this.game.handleMouseMove, this.game)

    // Assigning this here because from Game it doesn't work.
    this.game.cursors = this.game.input.keyboard.createCursorKeys()
  }

  // Is responsible for pre-generating graphics for each color for sprites to use.
  setUpGraphics() {
    // Adding a white color to be used as default.
    colors.concat(WHITE_COLOR).forEach((color) => {
      const graphics = this.game.add.graphics(0, 0)
      const rgbColor = Phaser.Color.hexToRGB(color)
      graphics.beginFill(rgbColor, 1)
      graphics.lineStyle(SQUARE_BORDER_WIDTH, DEFAULT_BORDER_COLOR, 1)
      graphics.drawRect(0, 0, SQUARE_SIZE, SQUARE_SIZE)
      graphics.endFill()

      const texture = graphics.generateTexture()
      texture.rgbColor = rgbColor
      texture.color = color
      this.game.picsWarsState.graphicsTextures.push(texture)

      graphics.destroy()
    })
  }
}

export default MainState
