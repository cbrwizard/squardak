import Phaser from 'phaser'

import {
  DEFAULT_BACKGROUND_HEX,
  DEFAULT_TINT,
  HOVER_TINT,
  CAPTURABLE_TINT,
  SELECTED_TINT,
} from 'shared/constants/game'

/**
 * Is responsible for representing a single Square in Phaser.
 * Handles the state and the drawing.
 * @note: it might make more sense to generate icons sprites on hover, instead of
 * pre-generating them. It depends on performance.
 */
class Square extends Phaser.Image {
  constructor({
    game,
    key,
    onSquareDeselect,
    onSquareMouseOut,
    onSquareMouseOver,
    onSquareSelect,
    record,
    x,
    y,
  }) {
    super(game, x, y, key)
    this.inputEnabled = true

    this.setUpChildren()
    this.setDefaults(record)

    this.onDeselect = onSquareDeselect
    this.onMouseOver = onSquareMouseOver
    this.onMouseOut = onSquareMouseOut
    this.onSelect = onSquareSelect

    this.updateState()
    this.draw()

    this.events.onInputDown.add(this.handleInputDown.bind(this))
    this.events.onInputOver.add(this.handleInputOver.bind(this))
    this.events.onInputOut.add(this.handleInputOut.bind(this))
  }

  handleInputDown() {
    // We want it to indicate that it's selected, not hovered.
    this.data.hovered = false

    /*
      If selected:
        we want to be able to deselect it unless the player is waiting for their
        turn results.
     */
    if (this.data.selected && !this.game.picsWarsState.isWaitingForResults) {
      this.data.selected = false
      this.onDeselect(this)

      /*
      If we can select squares right now and the square is capturable:
        select it.
     */
    } else if (
      this.game.picsWarsState.canSelectSquares &&
      this.data.capturable
    ) {
      this.data.selected = true
      this.onSelect(this)

      this.redraw()
      /*
        We want players to follow the link only if it's not capturable.
       */
    } else if (this.record.linkUrl) {
      window.open(this.record.linkUrl, '_blank')
    }
  }

  handleInputOver() {
    this.onMouseOver(this)
    this.data.hovered = true

    this.redraw()
  }

  handleInputOut() {
    this.onMouseOut(this)
    this.data.hovered = false
    this.redraw()
  }

  setDefaults(record) {
    // Contains a db record.
    this.record = record

    // For quick reference when searching. This is static anyway.
    this.id = record._id
    // 'position' is used by Phaser, hence the name.
    this.picsWarsPosition = record.position

    // Contains the logic state, like if a square is selectable.
    this.data = {
      belongsToCurrentUser: false,
      capturable: false,
      dataLoaded: undefined, // it's either true or undefined since we don't know.
      frame: 0,
      hovered: false,
      selected: false,
    }

    // Contains the graphical state, like current colors.
    this.graphicalState = {
      color: DEFAULT_BACKGROUND_HEX,
      tint: DEFAULT_TINT,
    }
  }

  setUpChildren() {
    const selected = this.game.make.sprite(0, 0, 'selectedIcon')
    selected.scale.set(0.5, 0.5)
    selected.anchor.set(-0.1, -0.1)
    selected.visible = false
    this.addChild(selected)

    const crown = this.game.make.sprite(0, 0, 'crownIcon')
    crown.scale.set(0.6, 0.6)
    crown.anchor.set(-0.05, 0)
    crown.visible = false
    crown.alpha = 0.8
    this.addChild(crown)

    const capturable = this.game.make.sprite(0, 0, 'capturableIcon')
    capturable.scale.set(0.5, 0.5)
    capturable.anchor.set(-0.15, -0.15)
    capturable.visible = false
    this.addChild(capturable)
  }

  updateAttributes({ mergeToRecord, mergeToData }) {
    this.record = { ...this.record, ...mergeToRecord }
    this.data = { ...this.data, ...mergeToData }
    this.redraw()
  }

  // Updates both graphicalState and data.
  // Doesn't draw anything.
  updateState() {
    const { color } = this.record
    const { hovered, selected } = this.data

    if (color) {
      this.graphicalState.color = color
    } else {
      this.graphicalState.color = DEFAULT_BACKGROUND_HEX
    }

    if (hovered) {
      this.setSquareToVisuallyHovered()
    } else if (selected) {
      this.setSquareToVisuallySelected()
    } else if (this.game.doesSquareBelongToCurrentUser(this.picsWarsPosition)) {
      this.setSquareToCurrentUser()
      this.input.useHandCursor = false
      // TODO: optimize it so it doesn't query all squares.
    } else if (
      this.game.picsWarsState.canMakeTurnsToday &&
      this.game.isSquareOneOfCapturable(this.picsWarsPosition)
    ) {
      if (this.game.picsWarsState.canSelectSquares) {
        this.input.useHandCursor = true
      }
      this.setSquareToCapturable()
    } else {
      this.input.useHandCursor = false
      this.resetSquareToNormal()
    }
  }

  setSquareToVisuallyHovered() {
    this.graphicalState.tint = HOVER_TINT
  }

  setSquareToVisuallySelected() {
    this.graphicalState.tint = SELECTED_TINT
    this.showChildWithKey('selectedIcon')
  }

  setSquareToCurrentUser() {
    this.data.belongsToCurrentUser = true
    this.data.capturable = false

    this.graphicalState.tint = DEFAULT_TINT
    this.showChildWithKey('crownIcon')
  }

  setSquareToCapturable() {
    this.data.belongsToCurrentUser = false
    this.data.capturable = true

    this.graphicalState.tint = CAPTURABLE_TINT
    this.showChildWithKey('capturableIcon')
  }

  // Resets to non-capturable and not belonging to current user, either someone's or not.
  resetSquareToNormal() {
    this.data.belongsToCurrentUser = false
    this.data.capturable = false
    this.graphicalState.tint = DEFAULT_TINT
    this.hideAllChildren()
  }

  redraw() {
    this.updateState()
    this.draw()
  }

  hideAllChildren() {
    this.children.forEach((child) => {
      child.visible = false
    })
  }

  showChildWithKey(key) {
    this.hideAllChildren()
    this.children.find(child => child.key === key).visible = true
  }

  // Assigns either an image or a colored graphic as texture and
  // adds visual effects based on graphicalState.
  draw() {
    const { imageKey } = this.record
    const { color, tint } = this.graphicalState
    const { cache, picsWarsState } = this.game
    const { frame } = this.data

    let newTexture
    const isImageLoaded = imageKey && cache.checkImageKey(imageKey)

    if (isImageLoaded) {
      newTexture = imageKey
    } else {
      newTexture = picsWarsState.graphicsTextures.find(
        texture => texture.color === color
      )
    }
    if (this.texture !== newTexture) {
      this.loadTexture(newTexture, frame)
    }

    this.tint = tint
  }
}

export default Square
