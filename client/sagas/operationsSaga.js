import { all, call, put, select, takeLatest } from 'redux-saga/effects'
import { delay } from 'redux-saga'
import { actions as toastrActions } from 'react-redux-toastr'

import { PROCESS_OPERATION } from 'client/constants/redux/operations'
import { RETURN_TOMORROW_MODAL } from 'client/constants/modals'
import { openModal } from 'client/actions/lib/modals'
import {
  addSquarePositionsToCurrent,
  addSquarePositionsToOther,
  removeSquarePositionsFromCurrent,
  removeSquarePositionsFromOther,
} from 'client/actions/territories'
import {
  changePlanningState,
  setCurrentTurnsLeft,
  setSquaresToCaptureMax,
  removeAllSelectedSquares,
} from 'client/actions/game'
import { index } from 'client/api/games'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import translate from 'client/lib/translate'
import {
  resetCapturableSquares,
  updateSquaresRecords,
  setTerritoryDataInSquares,
} from 'client/phaser/manipulators/manipulateGame'
import { increaseStep } from '../actions/tutorial'
import getTutotialPositionForStep from '../lib/getTutorialPositionForStep'

const Raven = setUpRavenClient()

const currentUserIdSelector = state =>
  state.app.session.user && state.app.session.user._id
const currentTerritorySelector = state => state.app.territories.current
const othersTerritoriesSelector = state => state.app.territories.others
const tutorialStepSelector = state => state.app.tutorial.step

  // TODO: move to a selector or something.
function* territoryOfOtherUser(userId) {
  const othersTerritories = yield select(othersTerritoriesSelector)
  return othersTerritories.find(
    territory => territory.userId === userId
  ) || {}
}

function* handleProcessOtherUserOperation({ squarePositions, _user }) {
  yield put(addSquarePositionsToOther(squarePositions, _user._id))

  setTerritoryDataInSquares(yield call(territoryOfOtherUser, _user._id))
}

function* handleAffectCurrentTerritory(squarePositionsToRemove) {
  yield put(removeSquarePositionsFromCurrent(squarePositionsToRemove))
  const currentTerritory = yield select(currentTerritorySelector) || {}
  setTerritoryDataInSquares(currentTerritory)

  // Update the current user's info so that they don't try to capture
  // squares they can't anymore.
  const gameData = yield call(index)
  yield put(setSquaresToCaptureMax(gameData.squaresToCaptureMax))
  yield put(removeAllSelectedSquares())
  yield put(
    toastrActions.add({
      position: 'top-right',
      title: translate('app.notifications.operationCapture.victim'),
      type: 'error',
    })
  )
}

function* handleProcessCurrentUserOperation({ squarePositions }) {
  yield put(addSquarePositionsToCurrent(squarePositions))
  const currentTerritory = yield select(currentTerritorySelector)
  setTerritoryDataInSquares(currentTerritory)

  const gameData = yield call(index)
  yield put(setCurrentTurnsLeft(gameData.currentTurnsLeft))
  yield put(setSquaresToCaptureMax(gameData.squaresToCaptureMax))
  yield put(removeAllSelectedSquares())
  yield put(changePlanningState('ready'))

  // We wait before repositioning to allow squares to redraw.
  yield call(delay, 300)

  const tutorialStep = yield select(tutorialStepSelector)
  if (tutorialStep === 6) {
    yield put(increaseStep(getTutotialPositionForStep(7)))
  } else if (tutorialStep === 9) {
    yield put(increaseStep(getTutotialPositionForStep(10)))
  }

  yield put(
    toastrActions.add({
      position: 'top-right',
      title: translate('app.notifications.operationCapture.success'),
      type: 'success',
    })
  )

  if (gameData.currentTurnsLeft <= 3) {
    yield put(
      toastrActions.add({
        position: 'top-right',
        // TODO: handle the situation with 1 turn left and 's' in 'turns'.
        title: `${gameData.currentTurnsLeft} ${translate('app.notifications.turnsLeftToday')}`,
        type: 'warning',
      })
    )
  }

  // We display the return tomorrow modal only when the player made all turns today.
  if (gameData.currentTurnsLeft === 0) {
    yield put(openModal(RETURN_TOMORROW_MODAL))
  }
}

function* handleAffectOtherTerritory(squarePositionsToRemove, affectedUserId) {
  yield put(
    removeSquarePositionsFromOther(squarePositionsToRemove, affectedUserId)
  )
  setTerritoryDataInSquares(yield call(territoryOfOtherUser, affectedUserId))
}

function* handleProcessOperation({ payload }) {
  try {
    const currentUserId = yield select(currentUserIdSelector)

    // First we add the captured territories to the state and update the squares.
    if (currentUserId === payload._user._id) {
      yield call(handleProcessCurrentUserOperation, payload)
    } else {
      yield call(handleProcessOtherUserOperation, payload)
    }

    // Now we remove the captured squares from the previous owners and notify
    // users.
    // @note we don't care about passing all squarePositions to every territory
    // to check, because that's 10 squares max anyway.
    yield all(
      payload.affectedUsers.map(function* (affectedUserId) {
        if (currentUserId === affectedUserId) {
          yield call(handleAffectCurrentTerritory, payload.squarePositions)
        } else {
          yield call(
            handleAffectOtherTerritory,
            payload.squarePositions,
            affectedUserId
          )
        }
      })
    )

    resetCapturableSquares()
    updateSquaresRecords({
      // We want to make it not selected so that players don't try to capture
      // it by mistake when the owner changed.
      mergeToData: { selected: false },
      squarePositions: payload.squarePositions,
    })
  } catch (error) {
    yield put(
      toastrActions.add({
        position: 'top-right',
        title: translate('app.notifications.planSending.error'),
        type: 'error',
      })
    )
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* gameSaga() {
  yield takeLatest(PROCESS_OPERATION, handleProcessOperation)
}

export default gameSaga
