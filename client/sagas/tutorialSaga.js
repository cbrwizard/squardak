import { call, put, select, takeLatest } from 'redux-saga/effects'

import { FINISH_TUTORIAL } from 'client/constants/redux/tutorial'
import { putUpdate } from 'client/api/users'
import { fetchSessionData } from 'client/actions/sessions'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import { resetTutorial } from '../actions/tutorial'

const Raven = setUpRavenClient()

const currentUserIdSelector = state =>
  state.app.session.user && state.app.session.user._id

function* handleFinish() {
  try {
    yield put(resetTutorial())
    const currentUserId = yield select(currentUserIdSelector)

    yield call(putUpdate, currentUserId, { passedTutorial: true })
    yield put(fetchSessionData())
  } catch (error) {
    Raven.captureException(error)
  }
}

function* usersSaga() {
  yield takeLatest(FINISH_TUTORIAL, handleFinish)
}

export default usersSaga
