import gameSaga from 'client/sagas/gameSaga'
import imagesSaga from 'client/sagas/imagesSaga'
import messagesSaga from 'client/sagas/messagesSaga'
import operationsSaga from 'client/sagas/operationsSaga'
import plansSaga from 'client/sagas/plansSaga'
import sessionsSaga from 'client/sagas/sessionsSaga'
import territoriesSaga from 'client/sagas/territoriesSaga'
import tutorialSaga from 'client/sagas/tutorialSaga'
import usersSaga from 'client/sagas/usersSaga'

export default function* () {
  yield [
    gameSaga(),
    imagesSaga(),
    messagesSaga(),
    operationsSaga(),
    plansSaga(),
    sessionsSaga(),
    territoriesSaga(),
    tutorialSaga(),
    usersSaga(),
  ]
}
