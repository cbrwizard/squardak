import { call, put, select, takeLatest } from 'redux-saga/effects'
import { pick } from 'ramda'
import { startSubmit, stopSubmit } from 'redux-form'
import { actions as toastrActions } from 'react-redux-toastr'

import translate from 'client/lib/translate'
import { fetchSessionData } from 'client/actions/sessions'
import { updateCurrent, updateOtherByUserId } from 'client/actions/territories'
import {
  BECOME_PATREON,
  SIGN_UP,
  UPDATE,
  UPDATE_USER_IN_SQUARES,
} from 'client/constants/redux/users'
import { SIGN_UP_FORM, SETTINGS_FORM } from 'client/constants/forms'
import { SIGN_UP_MODAL } from 'client/constants/modals'
import { closeModal } from 'client/actions/lib/modals'
import { postSignUp, putUpdate } from 'client/api/users'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import {
  setTerritoryDataInSquares,
} from 'client/phaser/manipulators/manipulateGame'

const Raven = setUpRavenClient()

const PERMITTED_SIGN_UP_KEYS = ['email', 'password']
// Images are handled by a different saga.
const PERMITTED_UPDATE_KEYS = [
  'color',
  'email',
  'nick',
  'linkText',
  'linkUrl',
  'password',
]
const currentUserIdSelector = state =>
  state.app.session.user && state.app.session.user._id
const currentTerritorySelector = state => state.app.territories.current
const otherTerritoriesSelector = state => state.app.territories.others

function* handleUpdateSquaresForOther({
  color,
  copiedImageUUID,
  nick,
  _id,
  imageUUID,
}) {
  yield put(
    updateOtherByUserId({ color, copiedImageUUID, imageUUID, nick }, _id)
  )
  const otherTerritories = yield select(otherTerritoriesSelector)
  const otherTerritory = otherTerritories.find(
    territory => territory.userId === _id
  )

  setTerritoryDataInSquares(otherTerritory)
}

function* handleUpdateSquaresForCurrent(attributes) {
  yield put(updateCurrent(attributes))
  const currentTerritory = yield select(currentTerritorySelector)

  setTerritoryDataInSquares(currentTerritory)
}

function* handleSignUp(action) {
  try {
    const filteredValues = pick(PERMITTED_SIGN_UP_KEYS, action.payload)
    yield call(postSignUp, filteredValues)
    yield put(closeModal(SIGN_UP_MODAL))
    yield put(fetchSessionData())
    yield put(
      toastrActions.add({
        position: 'top-right',
        title: translate('app.notifications.signUp.success'),
        type: 'success',
      })
    )
  } catch (error) {
    Raven.captureException(error)
    yield put(stopSubmit(SIGN_UP_FORM, error.details))
    if (!error.details) {
      console.error(error)
    }
  }
}

// TODO: also re-render messages list since nick can change.
function* handleUpdate(action) {
  try {
    yield put(startSubmit(SETTINGS_FORM))
    const filteredValues = pick(PERMITTED_UPDATE_KEYS, action.payload)
    yield call(putUpdate, action.payload._id, filteredValues)
    yield put(fetchSessionData())
    yield put(
      toastrActions.add({
        position: 'top-right',
        title: translate('app.notifications.settings.success'),
        type: 'success',
      })
    )
    yield put(stopSubmit(SETTINGS_FORM))
  } catch (error) {
    Raven.captureException(error)
    yield put(stopSubmit(SETTINGS_FORM, error.details))
  }
}

/**
 * Updates a user hash in all squares with this user.
 * Is handy when the owner of the square doesn't change, but user's info does.
 */
function* handleUpdateUserInSquares({ payload }) {
  try {
    const currentUserId = yield select(currentUserIdSelector)

    if (currentUserId === payload._id) {
      // TODO: pick only needed attrs from a user here
      yield call(handleUpdateSquaresForCurrent, payload)
    } else {
      yield call(handleUpdateSquaresForOther, payload)
    }
  } catch (error) {
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* handleBecomePatreon() {
  try {
    const currentUserId = yield select(currentUserIdSelector)
    yield call(putUpdate, currentUserId, { isPatreon: true })
    yield put(
      toastrActions.add({
        position: 'top-right',
        title: translate('app.text.patreonValidation'),
        type: 'success',
      })
    )
  } catch (error) {
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* usersSaga() {
  yield takeLatest(SIGN_UP, handleSignUp)
  yield takeLatest(UPDATE, handleUpdate)
  yield takeLatest(UPDATE_USER_IN_SQUARES, handleUpdateUserInSquares)
  yield takeLatest(BECOME_PATREON, handleBecomePatreon)
}

export default usersSaga
