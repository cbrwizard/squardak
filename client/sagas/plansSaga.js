import { call, put, select, takeLatest } from 'redux-saga/effects'
import { actions as toastrActions } from 'react-redux-toastr'

import { SUBMIT_PLAN } from 'client/constants/redux/plans'
import { changePlanningState } from 'client/actions/game'
import { post } from 'client/api/plans'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import translate from 'client/lib/translate'
import { increaseStep } from '../actions/tutorial'
import getTutotialPositionForStep from '../lib/getTutorialPositionForStep'

const Raven = setUpRavenClient()

const selectedSquaresPositionsSelector = state => state.app.game.selectedSquaresPositions
const tutorialStepSelector = state => state.app.tutorial.step

function* handleSubmitPlan() {
  try {
    yield put(changePlanningState('processing'))
    const selectedSquaresPositions = yield select(selectedSquaresPositionsSelector)
    yield call(post, { selectedSquaresPositions })

    const tutorialStep = yield select(tutorialStepSelector)
    if (tutorialStep === 5) {
      yield put(increaseStep(getTutotialPositionForStep(6)))
    } else if (tutorialStep === 8) {
      yield put(increaseStep(getTutotialPositionForStep(9)))
    }
  } catch (error) {
    yield put(toastrActions.add({
      position: 'top-right',
      title: translate('app.notifications.planSending.error'),
      type: 'error',
    }))
    Raven.captureException(error)
    if (!error.details) { console.error(error) }
    yield put(changePlanningState('ready'))
  }
}

function* gameSaga() {
  yield takeLatest(SUBMIT_PLAN, handleSubmitPlan)
}

export default gameSaga
