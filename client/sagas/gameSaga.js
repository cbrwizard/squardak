import { call, put, select, takeLatest } from 'redux-saga/effects'
import { pick } from 'ramda'
import { stopSubmit } from 'redux-form'

import { fetchSessionData } from 'client/actions/sessions'
import {
  GAME_RENDERED,
  DESELECT_SQUARE,
  SELECT_SQUARE_POSITION,
  FETCH_GAME_SESSION,
} from 'client/constants/redux/game'
import {
  addSelectedSquarePosition,
  removeSelectedSquarePosition,
  setCurrentTurnsLeft,
  setSquaresToCaptureMax,
  setSquaresCount,
  setPositionCameraAtSquarePosition,
  setStarted,
  setIsFetchingGameSession,
} from 'client/actions/game'
import { setIsLoading } from 'client/actions/greeting'
import { handleFetchCurrent } from 'client/sagas/territoriesSaga'
import { GREETING_FORM } from 'client/constants/forms'
import { postStart } from 'client/api/games'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import { increaseStep } from '../actions/tutorial'
import getTutotialPositionForStep from '../lib/getTutorialPositionForStep'

const Raven = setUpRavenClient()
const PERMITTED_START_KEYS = ['guest', 'nick']

const tutorialStepSelector = state => state.app.tutorial.step
const selectedSquaresPositionsSelector = state => state.app.game.selectedSquaresPositions

function* handleFetchGameSession(action) {
  try {
    yield put(setIsFetchingGameSession(true))
    yield put(setIsLoading(true))

    const filteredValues = pick(PERMITTED_START_KEYS, action.payload)
    const gameData = yield call(postStart, filteredValues)
    yield put(fetchSessionData())

    yield put(setCurrentTurnsLeft(gameData.currentTurnsLeft))
    yield put(setSquaresToCaptureMax(gameData.squaresToCaptureMax))
    yield put(setSquaresCount(gameData.squaresCount))
    yield put(
      setPositionCameraAtSquarePosition(gameData.positionCameraAtSquarePosition)
    )

    yield put(setStarted(true))
  } catch (error) {
    Raven.captureException(error)
    yield put(stopSubmit(GREETING_FORM, error.details))
    if (!error.details) {
      console.error(error)
    }
    yield put(setStarted(false))
  } finally {
    yield put(setIsLoading(false))
  }
}

/**
 * Is called when the game is playable and navigatable.
 */
function* handleGameRendered() {
  try {
    yield put(setIsFetchingGameSession(false))

    yield call(handleFetchCurrent)
  } catch (error) {
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* handleDeselectSquarePosition(action) {
  try {
    yield put(removeSelectedSquarePosition(action.payload))
  } catch (error) {
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* handleSelectSquarePosition(action) {
  try {
    yield put(addSelectedSquarePosition(action.payload))

    const tutorialStep = yield select(tutorialStepSelector)
    const selectedSquaresPositions = yield select(selectedSquaresPositionsSelector)
    if (tutorialStep === 4) {
      yield put(increaseStep(getTutotialPositionForStep(5)))
    } else if (tutorialStep === 7 && selectedSquaresPositions.length >= 2) {
      yield put(increaseStep(getTutotialPositionForStep(8)))
    }
  } catch (error) {
    Raven.captureException(error)
    if (!error.details) {
      console.error(error)
    }
  }
}

function* gameSaga() {
  yield takeLatest(DESELECT_SQUARE, handleDeselectSquarePosition)
  yield takeLatest(GAME_RENDERED, handleGameRendered)
  yield takeLatest(SELECT_SQUARE_POSITION, handleSelectSquarePosition)
  yield takeLatest(FETCH_GAME_SESSION, handleFetchGameSession)
}

export default gameSaga
