import { call, select, put, takeLatest } from 'redux-saga/effects'

import {
  APPEND_TERRITORY,
  FETCH_CURRENT,
  FETCH_OTHERS,
} from 'client/constants/redux/territories'
import { current, others } from 'client/api/territories'
import { fetchOthers, setCurrent, setOthers } from 'client/actions/territories'
import { setIsLatestDataLoaded } from 'client/actions/game'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import {
  resetCapturableSquares,
  setTerritoryDataInSquares,
} from 'client/phaser/manipulators/manipulateGame'
import { increaseStep } from '../actions/tutorial'
import getTutotialPositionForStep from '../lib/getTutorialPositionForStep'

const Raven = setUpRavenClient()

const userPassedTutorialSelector = state =>
  state.app.session.user && state.app.session.user.passedTutorial

export function* handleAppendTerritory({ payload }) {
  try {
    setTerritoryDataInSquares(payload)

    resetCapturableSquares()
  } catch (error) {
    Raven.captureException(error)
    console.error(error)
  }
}

export function* handleFetchCurrent() {
  try {
    const response = yield call(current)

    yield put(setCurrent(response.territory))
    setTerritoryDataInSquares(response.territory)
    resetCapturableSquares()

    const passedTutorial = yield select(userPassedTutorialSelector)

    if (!passedTutorial) {
      yield put(increaseStep(getTutotialPositionForStep(1)))
    }
    yield put(fetchOthers())
  } catch (error) {
    Raven.captureException(error)
    console.error(error)
  }
}

export function* handleFetchOthers() {
  try {
    const response = yield call(others)

    yield put(setOthers(response.territories))

    response.territories.forEach(territory =>
      setTerritoryDataInSquares(territory)
    )

    yield put(setIsLatestDataLoaded(true))
  } catch (error) {
    Raven.captureException(error)
    console.error(error)
  }
}

function* territoriesSaga() {
  yield takeLatest(APPEND_TERRITORY, handleAppendTerritory)
  yield takeLatest(FETCH_CURRENT, handleFetchCurrent)
  yield takeLatest(FETCH_OTHERS, handleFetchOthers)
}

export default territoriesSaga
