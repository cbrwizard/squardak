import { pick } from 'ramda'
import { call, put, select, takeLatest } from 'redux-saga/effects'
import { startSubmit, stopSubmit } from 'redux-form'
import { actions as toastrActions } from 'react-redux-toastr'

import translate from 'client/lib/translate'
import { fetchSessionData } from 'client/actions/sessions'
import { IMAGE_MODAL } from 'client/constants/modals'
import { closeModal } from 'client/actions/lib/modals'
import { UPDATE } from 'client/constants/redux/images'
import { IMAGE_FORM } from 'client/constants/forms'
import { putUpdate } from 'client/api/users'
import setUpRavenClient from 'client/lib/setUpRavenClient'
import { increaseStep } from '../actions/tutorial'
import getTutotialPositionForStep from '../lib/getTutorialPositionForStep'

const Raven = setUpRavenClient()
const PERMITTED_UPDATE_KEYS = ['copyImageFromUserNick', 'imageUUID']
const tutorialStepSelector = state => state.app.tutorial.step

function* handleUpdate(action) {
  try {
    yield put(startSubmit(IMAGE_FORM))
    const filteredValues = pick(PERMITTED_UPDATE_KEYS, action.payload)
    yield call(putUpdate, action.payload._id, filteredValues)
    yield put(fetchSessionData())
    yield put(
      toastrActions.add({
        position: 'top-right',
        title: translate('app.notifications.image.success'),
        type: 'success',
      })
    )
    yield put(stopSubmit(IMAGE_FORM))
    yield put(closeModal(IMAGE_MODAL))

    const tutorialStep = yield select(tutorialStepSelector)
    if (tutorialStep === 12) {
      yield put(increaseStep(getTutotialPositionForStep(13)))
    }
  } catch (error) {
    Raven.captureException(error)
    yield put(stopSubmit(IMAGE_FORM, error.details))
  }
}

function* usersSaga() {
  yield takeLatest(UPDATE, handleUpdate)
}

export default usersSaga
