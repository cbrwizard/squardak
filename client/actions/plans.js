import { SUBMIT_PLAN } from 'client/constants/redux/plans'

/*
 * Is responsible for handling the interaction with plans.
 */
export const submitPlan = () => (
  {
    type: SUBMIT_PLAN,
  }
)
