import React from 'react'
import { bool, func } from 'prop-types'

import AlreadyPatreon from './AlreadyPatreon'
import BecomePatreon from './BecomePatreon'

const propTypes = {
  handleClick: func.isRequired,
  isPatreon: bool,
}
const defaultProps = {
  isPatreon: false,
}

/*
 * Is responsible for rendering content related to Patreon.
 */
const Patreon = ({ handleClick, isPatreon }) => {
  const Content = isPatreon ? AlreadyPatreon : BecomePatreon
  return <Content onClick={handleClick} />
}

Patreon.propTypes = propTypes
Patreon.defaultProps = defaultProps

export default Patreon
