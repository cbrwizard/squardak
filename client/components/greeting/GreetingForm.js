import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import { LinearProgress } from 'material-ui/Progress'
import React from 'react'
import { bool, func, object, oneOf } from 'prop-types'
import { Field } from 'redux-form'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'
import { green } from 'material-ui/colors'

import ReduxFormTextField from 'client/components/form/ReduxFormTextField'
import GreetingTitle from 'client/components/greeting/GreetingTitle'
import GreetingButtons from 'client/components/greeting/GreetingButtons'

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: green[700],
    },
    backgroundColor: green[500],
    color: 'white',
    margin: '2px',
  },
  progress: {
    margin: '10px auto 0',
    width: '80%',
  },
}

const propTypes = {
  classes: object.isRequired,
  guest: bool.isRequired,
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired,
  isLoading: bool.isRequired,
  onLoginClick: func,
  onLogoutClick: func,
  onSignUpClick: func,
  type: oneOf(['user', 'visitor']).isRequired,
}
const defaultProps = {
  onLoginClick: () => {},
  onLogoutClick: () => {},
  onSignUpClick: () => {},
}

/*
 * Is responsible for rendering a Greeting form for both Users and visitors.
 */
const GreetingForm = ({
  classes,
  handleSubmit,
  guest,
  intl,
  isLoading,
  onLoginClick,
  onLogoutClick,
  onSignUpClick,
  type,
}) => (
  <form onSubmit={handleSubmit} className="h-center-text">
    <GreetingTitle {...{ type }} />
    <Grid item>
      <Field
        name="nick"
        component={ReduxFormTextField}
        label={intl.formatMessage({ id: 'app.forms.nick.label' })}
      />
    </Grid>
    <GreetingButtons
      {...{ guest }}
      {...{ onLoginClick }}
      {...{ onLogoutClick }}
      {...{ onSignUpClick }}
      {...{ type }}
    />
    <Grid item>
      <Button
        className={classes.button}
        disabled={isLoading}
        type="submit"
        raised
      >
        {intl.formatMessage({ id: 'app.forms.play' })}
      </Button>
    </Grid>
    <Grid item>
      {isLoading &&
        <LinearProgress className={classes.progress} mode="query" size={50} />}
    </Grid>
  </form>
)

GreetingForm.propTypes = propTypes
GreetingForm.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(GreetingForm))
