import Grid from 'material-ui/Grid'
import React from 'react'
import { object } from 'prop-types'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
}

const styleSheet = {
  container: {
    margin: '15px 0 0',
    maxWidth: '300px',
    textAlign: 'center',
  },
  text: {
    margin: '0 0 5px',
  },
}

/*
 * Is responsible for rendering text when you're already a patreon.
 */
const AlreadyPatreon = ({ classes, intl }) => (
  <Grid item className={classes.container}>
    <Typography className={classes.text} type="body1">
      {intl.formatMessage({ id: 'app.text.thanksForSupportingOnPatreon' })}
    </Typography>
  </Grid>
)

AlreadyPatreon.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(AlreadyPatreon))
