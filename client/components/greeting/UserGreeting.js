import Grid from 'material-ui/Grid'
import React from 'react'
import { bool, func, object } from 'prop-types'
import { withStyles } from 'material-ui/styles'

import ConnectedGreetingForm from 'client/containers/greeting/ConnectedGreetingForm'

const propTypes = {
  classes: object.isRequired,
  guest: bool,
  isLoading: bool.isRequired,
  onLogoutClick: func.isRequired,
  onSubmit: func.isRequired,
}
const defaultProps = {
  guest: false,
}

const styleSheet = {
  container: {
    margin: '0 auto',
    width: '190px',
  },
}

const UserGreeting = ({
  classes,
  isLoading,
  guest,
  onSubmit,
  onLogoutClick,
}) => (
  <Grid className={classes.container} container direction="column">
    <ConnectedGreetingForm
      {...{ isLoading }}
      {...{ guest }}
      {...{ onSubmit }}
      {...{ onLogoutClick }}
      type="user"
    />
  </Grid>
)

UserGreeting.defaultProps = defaultProps
UserGreeting.propTypes = propTypes

export default withStyles(styleSheet)(UserGreeting)
