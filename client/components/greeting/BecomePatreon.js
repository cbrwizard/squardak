import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import React from 'react'
import { func, object } from 'prop-types'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
  onClick: func.isRequired,
}

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: '#d65948',
    },
    backgroundColor: '#F96854',
    margin: '2px',
  },
  container: {
    margin: '15px 0 0',
    maxWidth: '300px',
    textAlign: 'center',
  },
  link: {
    color: '#fff',
    textDecoration: 'none',
  },
  text: {
    margin: '0 0 5px',
  },
}

/*
 * Is responsible for rendering a become a Patreon button.
 */
const BecomePatreon = ({ classes, intl, onClick }) => (
  <Grid item className={classes.container}>
    <Typography className={classes.text} type="body1">
      {intl.formatMessage({ id: 'app.text.supportOnPatreon' })}
    </Typography>
    <Button className={classes.button} {...{ onClick }}>
      <a
        className={classes.link}
        href="https://www.patreon.com/bePatron?u=4852765"
        rel="noopener noreferrer"
        target="_blank"
      >
        Become a Patron!
      </a>
    </Button>
  </Grid>
)

BecomePatreon.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(BecomePatreon))
