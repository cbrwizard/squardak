import Grid from 'material-ui/Grid'
import React from 'react'
import { bool, func, object, shape } from 'prop-types'
import Paper from 'material-ui/Paper'
import { withStyles } from 'material-ui/styles'

import ConfirmLogoutContainerModal
  from 'client/containers/sessions/ConfirmLogoutContainerModal'
import MobilesNotSupportedContainerModal
  from 'client/containers/greeting/MobilesNotSupportedContainerModal'
import LoginContainerModal from 'client/containers/sessions/LoginContainerModal'
import SignUpContainerModal from 'client/containers/users/SignUpContainerModal'
import UserGreeting from 'client/components/greeting/UserGreeting'
import VisitorGreeting from 'client/components/greeting/VisitorGreeting'
import PatreonContainer from '../../containers/greeting/PatreonContainer'

const styleSheet = {
  paper: {
    margin: '0 auto',
    padding: '20px 0',
    width: '200px',
  },
}

const propTypes = {
  classes: object.isRequired,
  isLoading: bool.isRequired,
  onLoginClick: func.isRequired,
  onLogoutClick: func.isRequired,
  onSignUpClick: func.isRequired,
  onUserSubmit: func.isRequired,
  onVisitorSubmit: func.isRequired,
  session: shape({
    isLoggedIn: bool.isRequired,
    user: object,
  }).isRequired,
}

/*
 * Is responsible for rendering the Greeting for both users and visitors.
 * @note a visitor is a logged out user. A user can be a guest or not, but cannot
 * be a visitor.
 */
class Greeting extends React.Component {
  renderVisitorGreeting() {
    const {
      isLoading,
      onSignUpClick,
      onLoginClick,
      onVisitorSubmit,
    } = this.props

    return (
      <VisitorGreeting
        {...{ isLoading }}
        {...{ onSignUpClick }}
        onSubmit={onVisitorSubmit}
        {...{ onLoginClick }}
      />
    )
  }

  renderUserGreeting() {
    const { isLoading, onLoginClick, onLogoutClick, onUserSubmit } = this.props
    const { guest } = this.props.session.user

    return (
      <UserGreeting
        {...{ isLoading }}
        {...{ guest }}
        {...{ onLoginClick }}
        {...{ onLogoutClick }}
        onSubmit={onUserSubmit}
      />
    )
  }

  render() {
    const { classes } = this.props
    const { isLoggedIn } = this.props.session
    let content

    if (isLoggedIn) {
      content = this.renderUserGreeting()
    } else {
      content = this.renderVisitorGreeting()
    }

    return (
      <Grid container spacing={0} justify="center">
        <Grid item>
          <Paper className={classes.paper} elevation={4}>
            {content}
            <LoginContainerModal />
            <SignUpContainerModal />
          </Paper>
          {isLoggedIn && <PatreonContainer />}

          <ConfirmLogoutContainerModal />
          <MobilesNotSupportedContainerModal />
        </Grid>
      </Grid>
    )
  }
}

Greeting.propTypes = propTypes

export default withStyles(styleSheet)(Greeting)
