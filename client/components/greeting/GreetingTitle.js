import React from 'react'
import PropTypes from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'

const { oneOf } = PropTypes
const propTypes = {
  intl: intlShape.isRequired,
  type: oneOf(['user', 'visitor']).isRequired,
}

/*
 * Is responsible for rendering a title in a Greeting component.
 */
const GreetingTitle = ({ intl, type }) => {
  if (type === 'user') {
    return (
      <h2>
        {intl.formatMessage({ id: 'app.titles.welcomeBack' })}
      </h2>
    )
  }
  return null
}

GreetingTitle.propTypes = propTypes

export default injectIntl(GreetingTitle)
