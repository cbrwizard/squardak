import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import React from 'react'
import { bool, func, object, oneOf } from 'prop-types'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'

import NotYouButton from 'client/components/buttons/NotYouButton'

const propTypes = {
  classes: object.isRequired,
  guest: bool.isRequired,
  intl: intlShape.isRequired,
  onLoginClick: func,
  onLogoutClick: func,
  onSignUpClick: func,
  type: oneOf(['user', 'visitor']).isRequired,
}
const defaultProps = {
  onLoginClick: () => {},
  onLogoutClick: () => {},
  onSignUpClick: () => {},
}

const styleSheet = {
  alternative: {
    margin: '5px 0 0',
  },
  button: {
    margin: '2px',
  },
  notice: {
    'line-height': '1.5',
  },
}

/*
 * Is responsible for rendering a title in a Greeting component.
 * TODO: 'or' should be a locale too.
 */
const GreetingButtons = ({
  classes,
  guest,
  intl,
  onLoginClick,
  onLogoutClick,
  onSignUpClick,
  type,
}) => {
  if (type === 'user') {
    return (
      <NotYouButton
        className={classes.button}
        onClick={onLogoutClick}
        {...{ guest }}
      />
    )
  }

  return (
    <Grid item className={classes.alternative}>
      <span>
        or
      </span>
      <Button className={classes.button} dense onClick={onLoginClick}>
        {intl.formatMessage({ id: 'app.links.login' })}
      </Button>
      <span>
        /
      </span>
      <Button className={classes.button} dense onClick={onSignUpClick}>
        {intl.formatMessage({ id: 'app.links.signUp' })}
      </Button>
      <Typography className={classes.notice} paragraph type="caption">
        {intl.formatMessage({ id: 'app.text.registerLater' })}
      </Typography>
    </Grid>
  )
}

GreetingButtons.propTypes = propTypes
GreetingButtons.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(GreetingButtons))
