import React from 'react'
import { bool, func, object } from 'prop-types'
import Grid from 'material-ui/Grid'
import { withStyles } from 'material-ui/styles'

import ConnectedGreetingForm from 'client/containers/greeting/ConnectedGreetingForm'

const propTypes = {
  classes: object.isRequired,
  isLoading: bool.isRequired,
  onLoginClick: func.isRequired,
  onSignUpClick: func.isRequired,
  onSubmit: func.isRequired,
}

const styleSheet = {
  container: {
    margin: '0 auto',
    width: '190px',
  },
}

const VisitorGreeting = ({
  classes,
  isLoading,
  onSubmit,
  onSignUpClick,
  onLoginClick,
}) => (
  <Grid className={classes.container} container direction="column">
    <ConnectedGreetingForm
      {...{ isLoading }}
      guest={false}
      {...{ onSubmit }}
      {...{ onSignUpClick }}
      {...{ onLoginClick }}
      type="visitor"
    />
  </Grid>
)

VisitorGreeting.propTypes = propTypes

export default withStyles(styleSheet)(VisitorGreeting)
