import {
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import React from 'react'
import { injectIntl, intlShape } from 'react-intl'

const propTypes = {
  intl: intlShape.isRequired,
}

/*
 * Is responsible for rendering a mobiles not supported warning
 */
const MobilesNotSupported = ({ intl }) => (
  <div>
    <DialogTitle>
      {intl.formatMessage({ id: 'app.titles.mobilesNotSupported' })}
    </DialogTitle>
    <DialogContent>
      <DialogContentText>
        {intl.formatMessage({ id: 'app.text.mobilesNotSupported' })}
      </DialogContentText>
    </DialogContent>
  </div>
)

MobilesNotSupported.propTypes = propTypes

export default injectIntl(MobilesNotSupported)
