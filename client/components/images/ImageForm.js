import React from 'react'
import { bool, func, number, object, string } from 'prop-types'
import { DialogTitle } from 'material-ui/Dialog'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'
import Tabs, { Tab } from 'material-ui/Tabs'

import UploadImageForm from 'client/components/images/UploadImageForm'
import CopyImageForm from 'client/components/images/CopyImageForm'

const propTypes = {
  change: func.isRequired,
  classes: object.isRequired,
  copiedImageUUID: string,
  handleSubmit: func.isRequired,
  imageUUID: string,
  intl: intlShape.isRequired,
  onImageChange: func.isRequired,
  step: number,
  submitting: bool.isRequired,
}
const defaultProps = {
  copiedImageUUID: null,
  imageUUID: null,
  step: 0,
}

const styleSheet = {
  form: {
    'max-height': '90vh',
    overflow: 'auto',
    width: '600px',
  },
}

/*
 * Is responsible for rendering a Image form.
 */
class ImageForm extends React.Component {
  constructor(props) {
    super(props)

    this.state = { selectedTabIndex: 0 }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event, selectedTabIndex) {
    this.setState({ selectedTabIndex })
  }

  render() {
    const {
      change,
      classes,
      copiedImageUUID,
      handleSubmit,
      intl,
      imageUUID,
      onImageChange,
      step,
      submitting,
    } = this.props
    const { selectedTabIndex } = this.state

    return (
      <form className={classes.form} onSubmit={handleSubmit}>
        <DialogTitle className="h-center-text">
          {intl.formatMessage({ id: 'app.titles.images' })}
        </DialogTitle>
        <Tabs
          value={selectedTabIndex}
          onChange={this.handleChange}
          indicatorColor="primary"
          textColor="primary"
          centered
        >
          <Tab label={intl.formatMessage({ id: 'app.titles.uploadImage' })} />
          <Tab label={intl.formatMessage({ id: 'app.titles.copyImage' })} />
        </Tabs>
        {selectedTabIndex === 0 &&
          <UploadImageForm
            {...{ change, imageUUID, onImageChange, step, submitting }}
          />}
        {selectedTabIndex === 1 &&
          <CopyImageForm {...{ copiedImageUUID, submitting }} />}
      </form>
    )
  }
}

ImageForm.propTypes = propTypes
ImageForm.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(ImageForm))
