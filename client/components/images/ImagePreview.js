import Typography from 'material-ui/Typography'
import React from 'react'
import { object, string } from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import { withStyles } from 'material-ui/styles'
import { convertUUIDToURL } from 'client/lib/uploadcare'

const styleSheet = {
  image: {
    'max-height': '400px',
    'max-width': '100%',
  },
}

const propTypes = {
  classes: object.isRequired,
  imageUUID: string.isRequired, // instanceOf(File) actually.
  intl: intlShape.isRequired,
}

/*
 * Is responsible for rendering an image preview from a file.
 */
const ImagePreview = ({ classes, intl, imageUUID }) => (
  <div>
    <Typography type="subheading">
      {intl.formatMessage({ id: 'app.text.imagePreview' })}
    </Typography>
    <img className={classes.image} src={convertUUIDToURL(imageUUID)} />
  </div>
)

ImagePreview.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(ImagePreview))
