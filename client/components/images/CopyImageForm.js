import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import React from 'react'
import { bool, object, string } from 'prop-types'
import { DialogActions, DialogContent } from 'material-ui/Dialog'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import { Field } from 'redux-form'

import ReduxFormTextField from 'client/components/form/ReduxFormTextField'
import ImagePreview from 'client/components/images/ImagePreview'

const propTypes = {
  classes: object.isRequired,
  copiedImageUUID: string,
  copyImageFromUserNick: string,
  intl: intlShape.isRequired,
  submitting: bool.isRequired,
}
const defaultProps = {
  copiedImageUUID: null,
}

const styleSheet = {
  convertNotice: {
    margin: '0 0 15px',
  },
  previewContainer: {
    margin: '10px 0 0',
  },
  remove: {
    margin: '0 0 0 10px',
  },
  submit: {
    margin: '2px',
  },
}

/*
 * Is responsible for rendering a form to copy an image from another player.
 */
const CopyImageForm = ({ classes, intl, copiedImageUUID, submitting }) => (
  <div>
    <DialogContent>
      <Grid item>
        <Field
          fullWidth
          name="copyImageFromUserNick"
          component={ReduxFormTextField}
          label={intl.formatMessage({ id: 'app.forms.copyImageFromUserNick.label' })}
          type="text"
        />
      </Grid>
      <Grid item>
        <Typography type="body2" paragraph>
          {intl.formatMessage({ id: 'app.text.copyAdvice' })}
        </Typography>
      </Grid>
      <Grid item className={classes.previewContainer}>
        {copiedImageUUID && <ImagePreview imageUUID={copiedImageUUID} />}
      </Grid>
    </DialogContent>
    <DialogActions>
      <Grid item>
        <Button
          color="accent"
          className={classes.submit}
          disabled={submitting}
          type="submit"
          raised
        >
          {intl.formatMessage({ id: 'app.forms.save' })}
        </Button>
      </Grid>
    </DialogActions>
  </div>
)

CopyImageForm.propTypes = propTypes
CopyImageForm.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(CopyImageForm))
