import Button from 'material-ui/Button'
import React from 'react'
import { func, number, object } from 'prop-types'
import AddAPhotoIcon from 'material-ui-icons/AddAPhoto'
import { withStyles } from 'material-ui/styles'

import { green } from 'material-ui/colors'

import ConnectedImageFormModal
  from '../../containers/images/ConnectedImageFormModal'

const propTypes = {
  classes: object.isRequired,
  onClick: func.isRequired,
  step: number,
}
const defaultProps = {
  step: 0,
}

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: green[700],
    },
    backgroundColor: green[500],
    color: 'white',
    margin: '0 0 0 10px',
  },
}

/*
* Is responsible for rendering a Image Button.
*/
const ImageButton = ({ classes, onClick, step }) => (
  <Button
    className={`${classes.button} js-image-button`}
    dense
    raised
    onClick={() => onClick(step)}
  >
    <AddAPhotoIcon />

    <ConnectedImageFormModal />
  </Button>
)

ImageButton.propTypes = propTypes
ImageButton.defaultProps = defaultProps

export default withStyles(styleSheet)(ImageButton)
