import Button from 'material-ui/Button'
import Grid from 'material-ui/Grid'
import React from 'react'
import { bool, func, number, object, string } from 'prop-types'
import { DialogActions, DialogContent } from 'material-ui/Dialog'
import { withStyles } from 'material-ui/styles'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'

import ImagePreview from 'client/components/images/ImagePreview'
import { convertURLToUUID } from 'client/lib/uploadcare'
import {
  validateDimensions,
  validateMaxFileSize,
} from 'client/lib/validateImage'
import Uploader from '../lib/Uploader'

const propTypes = {
  change: func.isRequired,
  classes: object.isRequired,
  imageUUID: string,
  intl: intlShape.isRequired,
  onImageChange: func.isRequired,
  step: number.isRequired,
  submitting: bool.isRequired,
}
const defaultProps = {
  imageUUID: null,
}

const styleSheet = {
  convertNotice: {
    margin: '0 0 15px',
  },
  previewContainer: {
    height: '400px',
    margin: '10px 0 0',
  },
  remove: {
    margin: '0 0 0 10px',
  },
  submit: {
    margin: '2px',
  },
}

const handleUploaderChange = (change, value) =>
  change('imageUUID', convertURLToUUID(value))

const handleRemoveImage = change => change('imageUUID', null)

/*
 * Is responsible for rendering a form to upload an image.
 */
const UploadImageForm = ({
  change,
  classes,
  intl,
  imageUUID,
  onImageChange,
  step,
  submitting,
}) => (
  <div>
    <DialogContent>
      <Grid item>
        <Uploader
          id="file"
          name="file"
          onChange={(value) => {
            handleUploaderChange(change, value)
            onImageChange(step)
          }}
          validators={[
            validateMaxFileSize(1024 * 1024 * 5), // 5 mb
            validateDimensions(5000, 5000), // 5000x5000 max
          ]}
        />
        {imageUUID &&
          <Button
            className={classes.remove}
            onClick={() => handleRemoveImage(change)}
          >
            {intl.formatMessage({ id: 'app.links.removeImage' })}
          </Button>}
      </Grid>
      <Grid item>
        <Typography type="body2" paragraph>
          {intl.formatMessage({ id: 'app.text.censorshipAdvice' })}
        </Typography>
      </Grid>
      <Grid item className={classes.previewContainer}>
        {imageUUID && <ImagePreview {...{ imageUUID }} />}
      </Grid>
    </DialogContent>
    <DialogActions>
      <Grid item>
        <Button
          color="accent"
          className={`${classes.submit} js-image-submit-button`}
          disabled={submitting}
          type="submit"
          raised
        >
          {intl.formatMessage({ id: 'app.forms.save' })}
        </Button>
      </Grid>
    </DialogActions>
  </div>
)

UploadImageForm.propTypes = propTypes
UploadImageForm.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(UploadImageForm))
