// False positive.
/* eslint no-lonely-if: 0 */

import React from 'react'
import { bool, string } from 'prop-types'
import Typography from 'material-ui/Typography'

const propTypes = {
  belongsToCurrentUser: bool,
  belongsToText: string.isRequired,
  hoverSquareLoadingText: string.isRequired,
  hoverSquareNoOwnerText: string.isRequired,
  isGameLatestDataLoaded: bool.isRequired,
  isSquareDataLoaded: bool,
  nick: string,
  youText: string.isRequired,
}

const defaultProps = {
  belongsToCurrentUser: false,
  isSquareDataLoaded: false,
  nick: null,
}

/**
 * Is responsible for rendering the square owner in a tooltip.
 */
const HoveredOwner = ({
  hoverSquareNoOwnerText,
  youText,
  belongsToText,
  nick,
  hoverSquareLoadingText,
  belongsToCurrentUser,
  isGameLatestDataLoaded,
  isSquareDataLoaded,
}) => {
  if (isSquareDataLoaded || isGameLatestDataLoaded) {
    if (!nick) {
      return (
        <Typography type="body2">
          {hoverSquareNoOwnerText}
        </Typography>
      )
    }

    let name
    if (belongsToCurrentUser) {
      name = youText
    } else {
      name = nick
    }

    return (
      <Typography type="body2">
        {`${belongsToText} `}
        <strong>
          {name}
        </strong>
      </Typography>
    )
  }
  return (
    <Typography type="body2">
      {hoverSquareLoadingText}
    </Typography>
  )
}

HoveredOwner.propTypes = propTypes
HoveredOwner.defaultProps = defaultProps

export default HoveredOwner
