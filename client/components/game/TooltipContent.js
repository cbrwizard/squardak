import React from 'react'
import { bool, object, shape, string } from 'prop-types'
import Typography from 'material-ui/Typography'
import { isEmpty } from 'ramda'
import HoveredOwner from 'client/components/game/HoveredOwner'

const propTypes = {
  belongsToText: string.isRequired,
  capturableText: string.isRequired,
  isClickableText: string.isRequired,
  deselectableText: string.isRequired,
  hoveredSquareInfo: shape({
    _user: object,
    belongsToCurrentUser: bool,
    capturable: bool,
    dataLoaded: bool,
    linkText: string,
    linkUrl: string,
  }).isRequired,
  hoverSquareLoadingText: string.isRequired,
  hoverSquareNoOwnerText: string.isRequired,
  isGameLatestDataLoaded: bool.isRequired,
  youText: string.isRequired,
}

/*
* Is responsible for rendering a tooltip content for canvas.
*/
const TooltipContent = ({
  capturableText,
  deselectableText,
  hoveredSquareInfo,
  isGameLatestDataLoaded,
  isClickableText,
  hoverSquareNoOwnerText,
  youText,
  belongsToText,
  hoverSquareLoadingText,
}) => {
  if (isEmpty(hoveredSquareInfo)) {
    return null
  }
  return (
    <div>
      <Typography type="subheading">
        {hoveredSquareInfo.selected &&
          <Typography type="body2">
            {deselectableText}
          </Typography>}
        {!hoveredSquareInfo.selected &&
          hoveredSquareInfo.capturable &&
          <Typography type="body2">
            <strong>
              {capturableText}
            </strong>
          </Typography>}
        <HoveredOwner
          belongsToCurrentUser={hoveredSquareInfo.belongsToCurrentUser}
          nick={hoveredSquareInfo.nick}
          {...{ isGameLatestDataLoaded }}
          {...{ hoverSquareLoadingText }}
          {...{ hoverSquareNoOwnerText }}
          {...{ youText }}
          {...{ belongsToText }}
          isSquareDataLoaded={hoveredSquareInfo.dataLoaded}
        />
        {hoveredSquareInfo.linkUrl &&
          <Typography type="body2">
            {`${isClickableText} `}
            <a href={hoveredSquareInfo.linkUrl}>
              <strong>
                {hoveredSquareInfo.linkText || 'Link'}
              </strong>
            </a>
          </Typography>}
      </Typography>
    </div>
  )
}

TooltipContent.propTypes = propTypes

export default TooltipContent
