import React from 'react'
import Grid from 'material-ui/Grid'

import CreatePlanButtonContainer
  from 'client/containers/game/CreatePlanButtonContainer'
import AdvicePanelContainer from 'client/containers/game/AdvicePanelContainer'

/*
 * Is responsible for rendering the UI on the bottom of the screen with main
 * game controls and information.
 */
const CommandPanel = () => (
  <div className="h-position-bottom-center">
    <Grid container spacing={0}>
      <Grid item>
        <AdvicePanelContainer />
      </Grid>
      <Grid item>
        <CreatePlanButtonContainer />
      </Grid>
    </Grid>
  </div>
)

export default CommandPanel
