import React from 'react'
import { green } from 'material-ui/colors'
import Button from 'material-ui/Button'
import { injectIntl, intlShape } from 'react-intl'
import { bool, func, object, string } from 'prop-types'
import { withStyles } from 'material-ui/styles'

const propTypes = {
  buttonColor: string,
  classes: object.isRequired,
  intl: intlShape.isRequired,
  isEnabled: bool.isRequired,
  onClick: func.isRequired,
}

const defaultProps = {
  buttonColor: '',
}

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: green[700],
    },
    'border-radius': '0 2px 2px 0',
    color: 'white',
    height: '50px',
    'min-width': '120px',
  },
}

/*
 * Is responsible for rendering the create a plan button.
 */
const CreatePlanButton = ({
  buttonColor,
  classes,
  intl,
  isEnabled,
  onClick,
}) => (
  <Button
    className={`${classes.button} js-capture-button`}
    disabled={!isEnabled}
    {...{ onClick }}
    raised
    style={{
      backgroundColor: buttonColor,
    }}
  >
    <span>
      {intl.formatMessage({ id: 'app.forms.capture' })}
    </span>
  </Button>
)

CreatePlanButton.propTypes = propTypes
CreatePlanButton.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(CreatePlanButton))
