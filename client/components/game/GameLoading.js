import React from 'react'
import Grid from 'material-ui/Grid'
import { object } from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import { CircularProgress } from 'material-ui/Progress'
import { withStyles } from 'material-ui/styles'

import Logo from 'client/components/generic/Logo'
import RandomFact from 'client/components/game/RandomFact'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
}

const styleSheet = {
  container: {
    'max-width': '500px',
  },
  title: {
    margin: '15px 0',
  },
}

/**
 * Is responsible for rendering a loading game spinner with text.
 */
const GameLoading = ({ classes, intl }) => (
  <div className="h-height-full h-center-flex h-center-text">
    <Grid
      align="center"
      container
      direction="column"
      justify="center"
      className={classes.container}
    >
      <Logo />
      <Typography className={classes.title} type="title">
        {intl.formatMessage({ id: 'app.titles.didYouKnow' })}
      </Typography>
      <RandomFact />
      <CircularProgress size={60} />
    </Grid>
  </div>
)

GameLoading.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(GameLoading))
