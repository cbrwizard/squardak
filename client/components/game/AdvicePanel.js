import React from 'react'
import { blue } from 'material-ui/colors'
import Typography from 'material-ui/Typography'
import { injectIntl, intlShape } from 'react-intl'
import { number, object, string } from 'prop-types'
import { withStyles } from 'material-ui/styles'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
  squaresToCaptureLeft: number.isRequired,
  squaresToCaptureMax: number.isRequired,
  textMessagePath: string.isRequired,
}

const styleSheet = {
  container: {
    'align-items': 'center',
    backgroundColor: blue[300],
    'border-radius': '2px 0 0 2px',
    display: 'flex',
    height: '50px',
    'max-width': '175px',
    padding: '5px 7px 5px 5px',
  },
  text: {
    color: 'white',
  },
}

/*
 * Is responsible for rendering the advice on what to do next.
 */
const AdvicePanel = ({
  classes,
  intl,
  squaresToCaptureLeft,
  squaresToCaptureMax,
  textMessagePath,
}) => (
  <div className={`${classes.container} js-advice-panel`}>
    <Typography className={classes.text}>
      {intl.formatMessage(
        {
          id: textMessagePath,
        },
        {
          squaresToCaptureLeft,
          squaresToCaptureMax,
        }
      )}
    </Typography>
  </div>
)

AdvicePanel.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(AdvicePanel))
