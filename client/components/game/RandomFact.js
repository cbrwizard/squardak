import React from 'react'
import Typography from 'material-ui/Typography'
import { injectIntl, intlShape } from 'react-intl'
import Autolinker from 'autolinker'
import faker from 'faker'

const propTypes = {
  intl: intlShape.isRequired,
}

// Taken from a locale file.
const NUMBER_OF_FACTS = 6
const factNumber = faker.random.number(NUMBER_OF_FACTS)

/*
* Is responsible for rendering a random fact.
*/
export const RandomFact = ({ intl }) => {
  const factText = intl.formatMessage({ id: `app.text.randomFact${factNumber}` })

  return (
    <Typography paragraph type="subheading">
      <span
        dangerouslySetInnerHTML={{
          __html: Autolinker.link(factText, { truncate: 75 }),
        }}
      />
    </Typography>
  )
}


RandomFact.propTypes = propTypes

export default injectIntl(RandomFact)
