import Button from 'material-ui/Button'
import { DialogContent, DialogTitle } from 'material-ui/Dialog'
import React from 'react'
import { bool, func, object } from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import Grid from 'material-ui/Grid'
import { ShareButtons, generateShareIcon } from 'react-share'
import { withStyles } from 'material-ui/styles'

import {
  WEBSITE_ADDRESS,
  WEBSITE_IMAGE_ADDRESS,
  WEBSITE_HASHTAGS,
  WEBSITE_DESCRIPTION,
  WEBSITE_TITLE,
  WEBSITE_JOIN,
} from '../../../shared/constants/game'

const {
  FacebookShareButton,
  TwitterShareButton,
  VKShareButton,
  RedditShareButton,
} = ShareButtons

const propTypes = {
  classes: object.isRequired,
  handlePatreonClick: func.isRequired,
  intl: intlShape.isRequired,
  isPatreon: bool,
}
const defaultProps = {
  isPatreon: false,
}

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: '#d65948',
    },
    backgroundColor: '#F96854',
    margin: '2px 0 10px',
  },
  icon: {
    cursor: 'pointer',
  },
  link: {
    color: '#fff',
    textDecoration: 'none',
  },
}

const FacebookIcon = generateShareIcon('facebook')
const TwitterIcon = generateShareIcon('twitter')
const VKIcon = generateShareIcon('vk')
const RedditIcon = generateShareIcon('reddit')

/*
 * Is responsible for rendering a return tomorrow text.
 */
const ReturnTomorrow = ({ classes, intl, isPatreon, handlePatreonClick }) => {
  let patreonContent
  if (!isPatreon) {
    patreonContent = (
      <div>
        <Typography>
          {intl.formatMessage({ id: 'app.text.returnTomorrow.becomePatreon' })}
        </Typography>
        <Button className={classes.button} onClick={handlePatreonClick}>
          <a
            className={classes.link}
            href="https://www.patreon.com/bePatron?u=4852765"
            rel="noopener noreferrer"
            target="_blank"
          >
            Become a Patron!
          </a>
        </Button>
      </div>
    )
  }
  return (
    <div>
      <DialogTitle>
        {intl.formatMessage({ id: 'app.text.returnTomorrow.turnsUp' })}
      </DialogTitle>
      <DialogContent>
        <Typography paragraph>
          {intl.formatMessage({ id: 'app.text.returnTomorrow.return' })}
        </Typography>
        {patreonContent}
        <Typography paragraph>
          {intl.formatMessage({ id: 'app.text.returnTomorrow.invite' })}
        </Typography>
        <Grid container>
          <Grid item>
            <FacebookShareButton url={WEBSITE_ADDRESS} quote={WEBSITE_JOIN}>
              <FacebookIcon className={classes.icon} round />
            </FacebookShareButton>
          </Grid>
          <Grid item>
            <TwitterShareButton
              url={WEBSITE_ADDRESS}
              title={WEBSITE_JOIN}
              hashtags={WEBSITE_HASHTAGS}
            >
              <TwitterIcon className={classes.icon} round />
            </TwitterShareButton>
          </Grid>
          <Grid item>
            <VKShareButton
              url={WEBSITE_ADDRESS}
              title={WEBSITE_TITLE}
              description={WEBSITE_DESCRIPTION}
              image={WEBSITE_IMAGE_ADDRESS}
            >
              <VKIcon className={classes.icon} round />
            </VKShareButton>
          </Grid>
          <Grid item>
            <RedditShareButton
              url={WEBSITE_ADDRESS}
              title={`${WEBSITE_TITLE}: ${WEBSITE_DESCRIPTION}`}
            >
              <RedditIcon className={classes.icon} round />
            </RedditShareButton>
          </Grid>
        </Grid>
      </DialogContent>
    </div>
  )
}

ReturnTomorrow.propTypes = propTypes
ReturnTomorrow.defaultProps = defaultProps

export default withStyles(styleSheet)(injectIntl(ReturnTomorrow))
