import React from 'react'
import { bool, object } from 'prop-types'
import { grey } from 'material-ui/colors'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import { CircularProgress } from 'material-ui/Progress'
import { withStyles } from 'material-ui/styles'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
  isLatestDataLoaded: bool.isRequired,
}

const styleSheet = {
  title: {
    color: grey[900],
    margin: '15px 0',
  },
}

/**
 * Is responsible for rendering a data loading spinner with text.
 */
const DataLoading = ({ classes, intl, isLatestDataLoaded }) => {
  if (!isLatestDataLoaded) {
    return (
      <div className="h-center-text">
        <Typography className={classes.title} type="title">
          {intl.formatMessage({ id: 'app.titles.dataLoading' })}
        </Typography>
        <CircularProgress size={30} />
      </div>
    )
  }

  return null
}

DataLoading.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(DataLoading))
