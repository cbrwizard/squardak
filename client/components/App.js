import React from 'react'
import { bool, func, object } from 'prop-types'
import { withStyles } from 'material-ui/styles'

import MainMenu from 'client/components/startScreen/MainMenu'
import GameContainer from 'client/containers/GameContainer'
import GameLoading from 'client/components/game/GameLoading'

const propTypes = {
  classes: object.isRequired,
  isFetchingGameSession: bool.isRequired,
  isMaintenance: bool,
  onLoad: func.isRequired,
  started: bool.isRequired,
}

const styleSheet = {
  container: {
    background: '#fff',
    width: '100%',
  },
}

const defaultProps = {
  isMaintenance: false,
}

/*
 * Is responsible for rendering the main components.
 * @note GameLoading is rendered with GameContainer when started so that
 * it shows up while the game is still loading.
 */
class App extends React.Component {
  componentDidMount() {
    this.props.onLoad()
  }

  render() {
    const { started, classes, isFetchingGameSession, isMaintenance } = this.props
    if (!started) {
      return (
        <main className="h-center-flex h-padding-around">
          <MainMenu {...{ isMaintenance }} />
        </main>
      )
    }

    return (
      <main>
        <GameContainer />
        {isFetchingGameSession &&
          <div
            className={`h-height-full h-position-top-center ${classes.container}`}
          >
            <GameLoading />
          </div>}
      </main>
    )
  }
}

App.propTypes = propTypes
App.defaultProps = defaultProps

export default withStyles(styleSheet)(App)
