import React from 'react'
import { bool, object } from 'prop-types'
import { withStyles } from 'material-ui/styles'

import TutorialContainer from 'client/containers/tutorial/TutorialContainer'
import ChatContainer from 'client/containers/ChatContainer'
import DataLoading from 'client/components/game/DataLoading'
import SettingsButtonContainer
  from 'client/containers/users/SettingsButtonContainer'
import TutorialButtonContainer from 'client/containers/tutorial/TutorialButtonContainer'
import ReturnTomorrowContainerModal from 'client/containers/game/ReturnTomorrowContainerModal'
import ImageButtonContainer from 'client/containers/images/ImageButtonContainer'
import PhaserReduxContainer from 'client/containers/phaser/PhaserReduxContainer'
import CommandPanel from 'client/components/game/CommandPanel'
import MainGame from 'client/phaser/games/MainGame'

const propTypes = {
  classes: object.isRequired,
  isLatestDataLoaded: bool.isRequired,
}

const styleSheet = {
  topLeftButtonsContainer: {
    // Because otherwise gets overwritten by MUI styles.
    position: 'fixed',
  },
}

/*
 * Is responsible for rendering the game screen.
 */
const Game = ({ classes, isLatestDataLoaded }) => (
  <div className="h-height-full">
    <div className={`h-position-top-left ${classes.topLeftButtonsContainer}`}>
      <SettingsButtonContainer />
      <TutorialButtonContainer />
      <ImageButtonContainer />
    </div>
    <div className="h-position-top-center">
      <DataLoading {...{ isLatestDataLoaded }} />
    </div>
    <ChatContainer />
    <CommandPanel />
    <PhaserReduxContainer Game={MainGame} />
    <ReturnTomorrowContainerModal />
    <TutorialContainer />
  </div>
)

Game.propTypes = propTypes

export default withStyles(styleSheet)(Game)
