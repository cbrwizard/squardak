import Button from 'material-ui/Button'
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from 'material-ui/Dialog'
import React from 'react'
import { func } from 'prop-types'
import { injectIntl, intlShape } from 'react-intl'

const propTypes = {
  intl: intlShape.isRequired,
  onConfirmClick: func.isRequired,
  onDismissClick: func.isRequired,
}

/*
 * Is responsible for rendering a logout confirmation.
 */
const ConfirmLogout = ({ intl, onConfirmClick, onDismissClick }) => (
  <div>
    <DialogTitle>
      {intl.formatMessage({ id: 'app.titles.confirmLogout' })}
    </DialogTitle>
    <DialogContent>
      <DialogContentText>
        {intl.formatMessage({ id: 'app.text.guestLoseDataNotice' })}
      </DialogContentText>
    </DialogContent>
    <DialogActions>
      <Button onClick={onDismissClick} color="primary">
        {intl.formatMessage({ id: 'app.links.logoutLater' })}
      </Button>
      <Button onClick={onConfirmClick} color="primary">
        {intl.formatMessage({ id: 'app.links.confirmLogout' })}
      </Button>
    </DialogActions>
  </div>
)

ConfirmLogout.propTypes = propTypes

export default injectIntl(ConfirmLogout)
