import React from 'react'
import PropTypes from 'prop-types'
import ReactTimeAgo from 'react-time-ago'
import Typography from 'material-ui/Typography'

const { string } = PropTypes
const propTypes = {
  createdAt: string.isRequired,
}

// eslint-disable-next-line react/prop-types
const CreatedAtWrapper = ({ children }) => (
  <Typography align="right" type="caption">
    {children}
  </Typography>
)

/*
 * Is responsible for rendering the message created at time, which is updated
 * dynamically.
 */
const MessageCreatedAt = ({ createdAt }) => (
  <ReactTimeAgo wrapper={CreatedAtWrapper}>
    {new Date(createdAt)}
  </ReactTimeAgo>
)

MessageCreatedAt.propTypes = propTypes

export default MessageCreatedAt
