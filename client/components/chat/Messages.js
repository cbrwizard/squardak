import React from 'react'
import { arrayOf, number, object, string, shape } from 'prop-types'
import { withStyles } from 'material-ui/styles'

import Message from 'client/components/chat/Message'

const propTypes = {
  classes: object.isRequired,
  messages: arrayOf(
    shape({
      date: string,
      id: number,
      text: string,
      userName: string,
    })
  ),
}
const defaultProps = {
  messages: [],
}

const styleSheet = {
  container: {
    height: '300px',
    overflow: 'auto',
  },
}

/*
 * Is responsible for rendering all chat messages.
 * TODO: create a shapes folder and store there the propTypes.
 * TODO: convert the container to a plugged fancy scroll container.
 */
const Messages = ({ classes, messages }) => (
  <section className={classes.container}>
    {messages.map(message => <Message key={message.createdAt} {...{ message }} />)}
  </section>
)

Messages.defaultProps = defaultProps
Messages.propTypes = propTypes

export default withStyles(styleSheet)(Messages)
