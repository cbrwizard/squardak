import React from 'react'
import { object, string } from 'prop-types'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'

import Autolinker from 'autolinker'

const propTypes = {
  classes: object.isRequired,
  text: string.isRequired,
}

const styleSheet = {
  text: {
    'word-wrap': 'break-word',
  },
}

/*
 * Is responsible for rendering the message text body.
 */
const MessageText = ({ classes, text }) => (
  <Typography className={classes.text}>
    <span
      dangerouslySetInnerHTML={{
        __html: Autolinker.link(text, { truncate: 35 }),
      }}
    />
  </Typography>
)

MessageText.propTypes = propTypes

export default withStyles(styleSheet)(MessageText)
