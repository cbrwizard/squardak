import React from 'react'
import PropTypes from 'prop-types'
import Typography from 'material-ui/Typography'

const { string } = PropTypes
const propTypes = {
  nick: string.isRequired,
}

/*
 * Is responsible for rendering the message's user nick.
 * TODO: display (you) next to a nick in your message.
 */
const MessageNick = ({ nick }) => (
  <Typography type="body2">
    {nick}
  </Typography>
)

MessageNick.propTypes = propTypes

export default MessageNick
