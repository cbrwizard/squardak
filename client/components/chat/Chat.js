import Paper from 'material-ui/Paper'
import React from 'react'
import { arrayOf, bool, number, object, string, func, shape } from 'prop-types'

import { blue, grey } from 'material-ui/colors'
import { withStyles } from 'material-ui/styles'

import ConnectedMessageForm from 'client/containers/chat/ConnectedMessageForm'
import Messages from 'client/components/chat/Messages'

const propTypes = {
  classes: object.isRequired,
  handleSubmit: func.isRequired,
  isLoading: bool,
  onLoad: func.isRequired,
  records: arrayOf(
    shape({
      date: string,
      id: number,
      text: string,
      userName: string,
    })
  ),
}

const defaultProps = {
  isLoading: false,
  records: [],
}

const styleSheet = {
  root: {
    width: '250px',
  },
  text: {
    'border-bottom': `1px solid ${grey[200]}`,
    color: blue[500],
    'line-height': '48px',
    padding: '0 5px',
  },
}

/*
 * Is responsible for rendering the Chat.
 */
class Chat extends React.Component {
  componentDidMount() {
    this.props.onLoad()
  }

  render() {
    const { classes, isLoading, records, handleSubmit } = this.props

    return (
      <div className="h-position-bottom-left">
        <Paper className={classes.root} elevation={4}>
          <Messages messages={records} />
          <ConnectedMessageForm {...{ isLoading }} onSubmit={handleSubmit} />
        </Paper>
      </div>
    )
  }
}

Chat.propTypes = propTypes
Chat.defaultProps = defaultProps

export default withStyles(styleSheet)(Chat)
