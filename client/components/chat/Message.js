import React from 'react'
import { number, object, string, shape } from 'prop-types'
import Divider from 'material-ui/Divider'
import { withStyles } from 'material-ui/styles'

import MessageCreatedAt from 'client/components/chat/MessageCreatedAt'
import MessageNick from 'client/components/chat/MessageNick'
import MessageText from 'client/components/chat/MessageText'

const propTypes = {
  classes: object.isRequired,
  message: shape({
    _user: shape({
      nick: string,
    }),
    createdAt: string,
    id: number,
    text: string,
  }).isRequired,
}

const styleSheet = {
  container: {
    padding: '10px 5px',
  },
}

/*
 * Is responsible for rendering a single chat message.
 * TODO: combine last messages from a single person together.
 */
const Message = ({ classes, message }) => (
  <article>
    <div className={classes.container}>
      <MessageNick nick={message._user.nick} />
      <MessageText text={message.text} />
      <MessageCreatedAt createdAt={message.createdAt} />
    </div>
    <Divider light />
  </article>
)

Message.propTypes = propTypes

export default withStyles(styleSheet)(Message)
