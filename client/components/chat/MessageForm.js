import React from 'react'
import { bool, func, object } from 'prop-types'
import { withStyles } from 'material-ui/styles'
import { CircularProgress } from 'material-ui/Progress'
import { Field } from 'redux-form'
import Button from 'material-ui/Button'
import SendIcon from 'material-ui-icons/Send'
import { green } from 'material-ui/colors'
import Grid from 'material-ui/Grid'
import { injectIntl, intlShape } from 'react-intl'

import ReduxFormTextField from 'client/components/form/ReduxFormTextField'

const propTypes = {
  classes: object.isRequired,
  handleSubmit: func.isRequired,
  intl: intlShape.isRequired,
  isLoading: bool.isRequired,
}

const styleSheet = {
  container: {
    'align-items': 'baseline',
    'justify-content': 'space-between',
    padding: '0 10px',
  },
  input: {
    width: '165px',
  },
  progress: {
    color: green[500],
    left: -2,
    position: 'absolute',
    top: -2,
  },
  wrapper: {
    position: 'relative',
  },
}

/*
 * Is responsible for rendering a message form.
 * TODO: move label to a locale.
 */
const MessageForm = ({ classes, handleSubmit, intl, isLoading }) => (
  <form onSubmit={handleSubmit}>
    <Grid container className={classes.container} spacing={0}>
      <Grid item>
        <Field
          name="text"
          className={classes.input}
          component={ReduxFormTextField}
          label={intl.formatMessage({ id: 'app.forms.message.label' })}
        />
      </Grid>
      <Grid item>
        <div className={classes.wrapper}>
          <Button fab disabled={isLoading} color="primary" type="submit">
            <SendIcon />
          </Button>
          {isLoading &&
            <CircularProgress className={classes.progress} size={60} />}
        </div>
      </Grid>
    </Grid>
  </form>
)

MessageForm.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(MessageForm))
