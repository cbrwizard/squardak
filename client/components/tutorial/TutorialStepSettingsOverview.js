import { object } from 'prop-types'
import React from 'react'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import SettingsIcon from 'material-ui-icons/Settings'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    margin: '0 2px 0 0',
    padding: '3px',
    verticalAlign: 'text-bottom',
  },
}

/*
 * Is responsible for rendering a content of a tutorial step about settings.
 */
const TutorialStepSettingsOverview = ({ classes }) => (
  <div>
    <Typography type="body2">
      You can also add a link to your territory to any website in the settings! There you can also change your nick and add an email.
      {' '}
      <SettingsIcon className={classes.icon} />
    </Typography>
  </div>
)

TutorialStepSettingsOverview.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepSettingsOverview)
