import { object } from 'prop-types'
import React from 'react'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import AddAPhotoIcon from 'material-ui-icons/AddAPhoto'
import { green } from 'material-ui/colors'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    backgroundColor: green[500],
    color: 'white',
    margin: '0 2px 0 0',
    padding: '3px',
    verticalAlign: 'text-bottom',
  },
}

/*
 * Is responsible for rendering a content of a tutorial step about opening an images form.
 */
const TutorialStepOpenImages = ({ classes }) => (
  <div>
    <Typography type="body2">
      Cool, now let's add more fun by adding a picture to your territories! Click on a photo icon
      {' '}
      <AddAPhotoIcon className={classes.icon} />
      to open image settings!
    </Typography>
  </div>
)

TutorialStepOpenImages.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepOpenImages)
