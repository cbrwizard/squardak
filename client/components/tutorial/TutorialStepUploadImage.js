import { object } from 'prop-types'
import React from 'react'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import { green } from 'material-ui/colors'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    backgroundColor: green[500],
    color: 'white',
    margin: '0 2px 0 0',
    padding: '3px',
    verticalAlign: 'text-bottom',
  },
}

/*
 * Is responsible for rendering a content of a tutorial 9th step.
 */
const TutorialStepUploadImage = ({ classes }) => (
  <div>
    <Typography type="body2">
      Click on Choose an image button to upload your image. You can also copy other player's image in the second tab by typing their nick there!
    </Typography>
  </div>
)

TutorialStepUploadImage.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepUploadImage)
