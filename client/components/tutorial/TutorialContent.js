import { bool, func, node, number, object } from 'prop-types'
import Button from 'material-ui/Button'
import React from 'react'
import { withStyles } from 'material-ui/styles'
import Typography from 'material-ui/Typography'

const propTypes = {
  children: node.isRequired,
  classes: object.isRequired,
  onNextClick: func.isRequired,
  onSkipClick: func.isRequired,
  shouldDisplayFinish: bool,
  shouldDisplayNext: bool,
  step: number.isRequired,
}

const defaultProps = {
  shouldDisplayFinish: false,
  shouldDisplayNext: true,
}

const styleSheet = {
  tooltip: {
    maxWidth: '250px',
    textAlign: 'left',
  },
}

/*
 * Is responsible for rendering a content of a tutorial popup.
 * @note we increase the step passed because we want to get position of the
 * step to come next.
 * TODO: convert texts to locales. Probably will need to pass the translation function.
 */
const TutorialContent = ({
  classes,
  children,
  onNextClick,
  onSkipClick,
  shouldDisplayNext,
  shouldDisplayFinish,
  step,
}) => {
  let finishText

  if (shouldDisplayFinish) {
    finishText = 'Finish tutorial'
  } else {
    finishText = 'Skip tutorial'
  }
  return (
    <div className={classes.tooltip}>
      <Typography className={classes.text} type="subheading">
        How to play
      </Typography>
      {children}
      <div>
        {shouldDisplayNext &&
          <Button onClick={() => onNextClick(step + 1)}>
            Next
          </Button>}
        <Button onClick={onSkipClick}>
          {finishText}
        </Button>
      </div>
    </div>
  )
}

TutorialContent.defaultProps = defaultProps
TutorialContent.propTypes = propTypes

export default withStyles(styleSheet)(TutorialContent)
