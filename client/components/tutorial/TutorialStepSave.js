import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial step about clicking on save button.
 */
const TutorialStepSave = () => (
  <div>
    <Typography type="body2">
      Click on Save button.
    </Typography>
  </div>
)

export default TutorialStepSave
