import { object } from 'prop-types'
import React from 'react'
import { withStyles } from 'material-ui/styles'
import capturableIcon from 'client/images/icons/capturable.png'
import Typography from 'material-ui/Typography'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    background: '#ccc',
    padding: '2px',
    verticalAlign: 'text-bottom',
    width: '20px',
  },
}

/*
 * Is responsible for rendering a content of a tutorial 2nd step.
 */
const TutorialStepCaptureOne = ({ classes }) => (
  <div>
    <Typography type="body2">
      Some squares are neutral, others belong to the players. You can capture squares marked with a white flag
      {' '}
      <img className={classes.icon} src={capturableIcon} />
      . Click on one to select it!
    </Typography>
  </div>
)

TutorialStepCaptureOne.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepCaptureOne)
