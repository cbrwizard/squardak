import { func, number, object } from 'prop-types'
import React from 'react'
import { withStyles } from 'material-ui/styles'
import { Tooltip } from 'react-tippy'

import TutorialContent from './TutorialContent'

import TutorialStepIntro from './TutorialStepIntro'
import TutorialStepNavigation from './TutorialStepNavigation'
import TutorialStepYourSquare from './TutorialStepYourSquare'
import TutorialStepCaptureOne from './TutorialStepCaptureOne'
import TutorialStepClickCapture from './TutorialStepClickCapture'
import TutorialStepCaptureWait from './TutorialStepCaptureWait'
import TutorialStepCaptureMore from './TutorialStepCaptureMore'
import TutorialStepOpenImages from './TutorialStepOpenImages'
import TutorialStepUploadImage from './TutorialStepUploadImage'
import TutorialStepSave from './TutorialStepSave'
import TutorialStepImageResult from './TutorialStepImageResult'
import TutorialStepSettingsOverview from './TutorialStepSettingsOverview'
import TutorialStepTurnsLimit from './TutorialStepTurnsLimit'
import TutorialStepGLHF from './TutorialStepGLHF'

const propTypes = {
  classes: object.isRequired,
  left: number.isRequired,
  onNextClick: func.isRequired,
  onSkipClick: func.isRequired,
  step: number.isRequired,
  top: number.isRequired,
}

const styleSheet = {
  container: {
    position: 'fixed',
  },
}

const TutorialStepComponentBasedOnStep = (step) => {
  switch (step) {
    case 1: {
      return TutorialStepIntro
    }
    case 2: {
      return TutorialStepNavigation
    }
    case 3: {
      return TutorialStepYourSquare
    }
    case 4: {
      return TutorialStepCaptureOne
    }
    case 5:
    case 8: {
      return TutorialStepClickCapture
    }
    case 6:
    case 9: {
      return TutorialStepCaptureWait
    }
    case 7: {
      return TutorialStepCaptureMore
    }
    case 10: {
      return TutorialStepOpenImages
    }
    case 11: {
      return TutorialStepUploadImage
    }
    case 12: {
      return TutorialStepSave
    }
    case 13: {
      return TutorialStepImageResult
    }
    case 14: {
      return TutorialStepSettingsOverview
    }
    case 15: {
      return TutorialStepTurnsLimit
    }
    case 16: {
      return TutorialStepGLHF
    }
    default:
      return null
  }
}

const tutorialStepPropsBasedOnStep = (step) => {
  switch (step) {
    case 1:
    case 2:
    case 3:
    case 13:
    case 14:
    case 15: {
      return { shouldDisplayNext: true }
    }
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
    case 10:
    case 11:
    case 12: {
      return { shouldDisplayNext: false }
    }
    case 16: {
      return { shouldDisplayFinish: true, shouldDisplayNext: false }
    }
    default:
      return {}
  }
}

/*
 * Is responsible for rendering a help tutorial template.
 */
class Tutorial extends React.Component {
  constructor(props) {
    super(props)
    this.state = { repositioningTooltip: false }
  }

  // We have to manually hide and reopen a Tooltip when needed
  // because otherwise it doesn't get repositioned if only the props update.
  componentWillUpdate(nextProps) {
    if (
      nextProps.step !== this.props.step ||
      nextProps.left !== this.props.left ||
      nextProps.top !== this.props.top
    ) {
      setTimeout(() => {
        this.setState({ repositioningTooltip: false })
      }, 100) // 1 should also work, but left 100 just in case.
      this.setState({ repositioningTooltip: true })
    }
  }

  render() {
    const { classes, onNextClick, onSkipClick, step, left, top } = this.props
    const { repositioningTooltip } = this.state
    if (step > 0 && !repositioningTooltip) {
      const TutorialStepContent = TutorialStepComponentBasedOnStep(step)

      return (
        <div className={classes.container} style={{ left, top }}>
          <Tooltip
            animation="shift"
            arrow
            useContext
            interactive
            hideOnClick={false}
            theme="light"
            position="right"
            open={!repositioningTooltip}
            unmountHTMLWhenHide
            style={{
              maxWidth: '250px',
              textAlign: 'left',
            }}
            html={
              <TutorialContent
                {...{ onNextClick, onSkipClick, step }}
                {...tutorialStepPropsBasedOnStep(step)}
              >
                {<TutorialStepContent {...{ step }} />}
              </TutorialContent>
            }
          />
        </div>
      )
    }
    return null
  }
}

Tutorial.propTypes = propTypes

export default withStyles(styleSheet)(Tutorial)
