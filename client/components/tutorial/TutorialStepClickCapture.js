import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial 3rd step.
 */
const TutorialStepClickCapture = () => (
  <div>
    <Typography type="body2">
      Great! Now click 💥 Capture button to finish the turn.
    </Typography>
  </div>
)

export default TutorialStepClickCapture
