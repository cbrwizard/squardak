import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial about intro.
 */
const TutorialStepNavigation = () => (
  <div>
    <Typography type="body2">
      Other players already own some territories. Use cursor keys or point mouse to the edge of the screen to navigate the game.
    </Typography>
  </div>
)

export default TutorialStepNavigation
