import Button from 'material-ui/Button'
import React from 'react'
import { func, object } from 'prop-types'
import HelpIcon from 'material-ui-icons/Help'
import { withStyles } from 'material-ui/styles'
import { blue } from 'material-ui/colors'

const propTypes = {
  classes: object.isRequired,
  onClick: func.isRequired,
}

const styleSheet = {
  button: {
    '&:hover': {
      backgroundColor: blue[700],
    },
    backgroundColor: blue[500],
    color: 'white',
    margin: '0 0 0 10px',
  },
}

/*
* Is responsible for rendering a Tutorial Button.
*/
const TutorialButton = ({ classes, onClick }) => (
  <Button className={classes.button} dense raised {...{ onClick }}>
    <HelpIcon />
  </Button>
)

TutorialButton.propTypes = propTypes

export default withStyles(styleSheet)(TutorialButton)
