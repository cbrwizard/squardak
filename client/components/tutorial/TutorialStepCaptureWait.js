import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial step about waiting for a capture processing.
 */
const TutorialStepCaptureWait = () => (
  <div>
    <Typography type="body2">
      Let's wait while it's processing...
    </Typography>
  </div>
)

export default TutorialStepCaptureWait
