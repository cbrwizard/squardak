import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial step about inviting friends.
 */
const TutorialStepGLHF = () => (
  <div>
    <Typography type="body2">
      Good luck and have fun! Invite your friends for better experience!
    </Typography>
  </div>
)

export default TutorialStepGLHF
