import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial intro step.
 */
const TutorialStepIntro = () => (
  <div>
    <Typography type="body2">
      Welcome to PicsWars.io! In this game you will capture territories for your picture.
    </Typography>
  </div>
)

export default TutorialStepIntro
