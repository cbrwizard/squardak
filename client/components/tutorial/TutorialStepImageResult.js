import React from 'react'
import Typography from 'material-ui/Typography'

/*
 * Is responsible for rendering a content of a tutorial step about showing an image on a territory.
 */
const TutorialStepImageResult = () => (
  <div>
    <Typography type="body2">
      Ahh, looking good! The more squares you capture, the more people will be able to see your image!
    </Typography>
  </div>
)

export default TutorialStepImageResult
