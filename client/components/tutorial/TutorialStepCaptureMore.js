import { object } from 'prop-types'
import React from 'react'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'
import capturableIcon from 'client/images/icons/capturable.png'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    background: '#ccc',
    padding: '2px',
    verticalAlign: 'text-bottom',
    width: '20px',
  },
}

/*
 * Is responsible for rendering a content of a tutorial step about capturing more squares.
 */
const TutorialStepCaptureMore = ({ classes }) => (
  <div>
    <Typography type="body2">
      Awesome, you now have a bigger territory! You can capture more squares now. Let's select white flags again!
      {' '}
      <img className={classes.icon} src={capturableIcon} />
    </Typography>
  </div>
)

TutorialStepCaptureMore.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepCaptureMore)
