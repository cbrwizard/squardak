import { object } from 'prop-types'
import React from 'react'
import { withStyles } from 'material-ui/styles'
import Typography from 'material-ui/Typography'
import crownIcon from 'client/images/icons/crown.png'

const propTypes = {
  classes: object.isRequired,
}

const styleSheet = {
  icon: {
    verticalAlign: 'text-bottom',
    width: '20px',
  },
}

/*
 * Is responsible for rendering a content of a tutorial 1st step.
 */
const TutorialStepYourSquare = ({ classes }) => (
  <div>
    <Typography type="body2">
      This is your first square, marked with a crown
      {' '}
      <img className={classes.icon} src={crownIcon} />
      . You want to capture more!
    </Typography>
  </div>
)

TutorialStepYourSquare.propTypes = propTypes

export default withStyles(styleSheet)(TutorialStepYourSquare)
