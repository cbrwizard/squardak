import Grid from 'material-ui/Grid'
import React from 'react'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import { object } from 'prop-types'
import { withStyles } from 'material-ui/styles'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
}

const styleSheet = {
  container: {
    margin: '35px 0',
    'text-align': 'center',
    width: '100%',
  },
  description: {
    'line-height': '1.5',
    padding: '0 5px',
  },
}

/*
 * Is responsible for rendering the maintenance screen.
 */
const Maintenance = ({ classes, intl }) => (
  <Grid
    className={classes.container}
    container
    direction="column"
    justify="center"
  >
    <Typography className={classes.description} paragraph type="title">
      {intl.formatMessage({ id: 'app.text.maintenance' })}
    </Typography>
    <Typography className={classes.description} paragraph type="title">
      {intl.formatMessage({ id: 'app.text.stayInTouch' })}
      <a href={process.env.TWITTER_LINK}>
        {intl.formatMessage({ id: 'app.text.twitter' })}
      </a>
    </Typography>
  </Grid>
)

Maintenance.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(Maintenance))
