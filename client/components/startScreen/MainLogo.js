import Grid from 'material-ui/Grid'
import { object } from 'prop-types'
import React from 'react'
import { injectIntl, intlShape } from 'react-intl'
import Typography from 'material-ui/Typography'
import { withStyles } from 'material-ui/styles'

import Logo from 'client/components/generic/Logo'

const propTypes = {
  classes: object.isRequired,
  intl: intlShape.isRequired,
}

const styleSheet = {
  container: {
    'max-width': '500px',
    'text-align': 'center',
  },
  description: {
    'line-height': '1.5',
    padding: '0 5px',
  },
}

/*
 * Is responsible for rendering the main big logo.
 * @note dangerouslySetInnerHTML is used to insert a <br>.
 */
const MainLogo = ({ classes, intl }) => {
  const gameDescription = intl.formatMessage({ id: 'app.text.gameDescription' })

  return (
    <Grid
      className={classes.container}
      container
      direction="column"
      justify="center"
    >
      <Logo />
      <Typography className={classes.description} paragraph type="title">
        <span dangerouslySetInnerHTML={{ __html: gameDescription }} />
      </Typography>
    </Grid>
  )
}

MainLogo.propTypes = propTypes

export default withStyles(styleSheet)(injectIntl(MainLogo))
