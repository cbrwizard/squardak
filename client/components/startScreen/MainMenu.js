import React from 'react'
import Grid from 'material-ui/Grid'
import { withStyles } from 'material-ui/styles'
import { bool, object } from 'prop-types'

import MainLogo from 'client/components/startScreen/MainLogo'
import GreetingContainer from 'client/containers/GreetingContainer'
import Maintenance from './Maintenance'

const styleSheet = {
  container: {
    transform: 'translate(0, -15%)',
  },
}
const propTypes = {
  classes: object.isRequired,
  isMaintenance: bool.isRequired,
}

/*
 * Is responsible for rendering the start screen menu.
 */
const MainMenu = ({ classes, isMaintenance }) => {
  let mainContent

  if (isMaintenance) {
    mainContent = (
      <Maintenance />
    )
  } else {
    mainContent = (
      <GreetingContainer />
    )
  }

  return (
    <Grid className={classes.container} container justify="center">
      <MainLogo />
      {mainContent}
    </Grid>
  )
}

MainMenu.propTypes = propTypes

export default withStyles(styleSheet)(MainMenu)
