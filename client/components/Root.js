/* eslint camelcase: 0 */
/*
 * Is responsible for rendering the whole app.
 */
import React from 'react'
import { object } from 'prop-types'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl-redux'
import ReduxToastr from 'react-redux-toastr'
import WindowSizeListener from 'react-window-size-listener'

import AppContainer from 'client/containers/AppContainer'
import repositionTutorial from '../lib/repositionTutorial'

WindowSizeListener.DEBOUNCE_TIME = 500

const propTypes = {
  store: object.isRequired,
}

const Root = ({ store }) => (
  <Provider store={store}>
    <div className="h-height-full" style={{ background: '#fff' }}>
      <IntlProvider>
        <AppContainer />
      </IntlProvider>
      <ReduxToastr />
      <WindowSizeListener onResize={repositionTutorial} />
    </div>
  </Provider>
)

Root.propTypes = propTypes

export default Root
