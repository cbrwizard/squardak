import { connect } from 'react-redux'

import { becomePatreon } from 'client/actions/users'
import Patreon from 'client/components/greeting/Patreon'

const mapStateToProps = state => ({
  isPatreon: state.app.session.user && state.app.session.user.isPatreon,
})

const mapDispatchToProps = dispatch => ({
  handleClick: () => {
    dispatch(becomePatreon())
  },
})

const PatreonContainer = connect(mapStateToProps, mapDispatchToProps)(
  Patreon
)

export default PatreonContainer
