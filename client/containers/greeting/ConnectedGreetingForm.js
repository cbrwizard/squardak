/* eslint import/no-mutable-exports: 0 */

import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'

import GreetingForm from 'client/components/greeting/GreetingForm'
import { GREETING_FORM } from 'client/constants/forms'

/*
 * Is responsible for connecting a GreetingForm to ReduxForm
 */
let ConnectedGreetingForm = reduxForm({
  enableReinitialize: true,
  form: GREETING_FORM,
})(GreetingForm)

ConnectedGreetingForm = connect(
  state => ({
    initialValues: state.app.session.user,
  })
)(ConnectedGreetingForm)

export default ConnectedGreetingForm
