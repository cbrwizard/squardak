import React from 'react'

import MobilesNotSupported from 'client/components/greeting/MobilesNotSupported'
import ReduxModal from 'client/containers/lib/ReduxModal'
import { MOBILES_NOT_SUPPORTED_MODAL } from 'client/constants/modals'

export const MobilesNotSupportedModal = () => <MobilesNotSupported />

const MobilesNotSupportedContainerModal = ReduxModal(MOBILES_NOT_SUPPORTED_MODAL)(MobilesNotSupportedModal)
export default MobilesNotSupportedContainerModal
