import React from 'react'

import LoginContainer from 'client/containers/sessions/LoginContainer'
import ReduxModal from 'client/containers/lib/ReduxModal'
import { LOGIN_MODAL } from 'client/constants/modals'

export const Login = () => <LoginContainer />

const LoginContainerModal = ReduxModal(LOGIN_MODAL)(Login)
export default LoginContainerModal
