import { connect } from 'react-redux'

import { closeModal } from 'client/actions/lib/modals'
import {
  CONFIRM_LOGOUT_MODAL,
} from 'client/constants/modals'
import { logout } from 'client/actions/sessions'
import ConfirmLogout from 'client/components/sessions/ConfirmLogout'

const mapStateToProps = null
const mapDispatchToProps = dispatch => ({
  onConfirmClick: () => {
    dispatch(closeModal(CONFIRM_LOGOUT_MODAL))
    dispatch(logout())
  },
  onDismissClick: () => {
    dispatch(closeModal(CONFIRM_LOGOUT_MODAL))
  },
})

const ConfirmLogoutContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConfirmLogout)

export default ConfirmLogoutContainer
