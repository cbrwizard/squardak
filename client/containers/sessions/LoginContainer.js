import { connect } from 'react-redux'

import { closeModal, openModal } from 'client/actions/lib/modals'
import {
  FORGOT_PASSWORD_MODAL,
  LOGIN_MODAL,
  SIGN_UP_MODAL,
} from 'client/constants/modals'
import { signIn } from 'client/actions/sessions'
import ConnectedLoginForm from 'client/containers/sessions/ConnectedLoginForm'

const mapStateToProps = null
const mapDispatchToProps = dispatch => ({
  onForgotPasswordClick: () => {
    dispatch(closeModal(SIGN_UP_MODAL))
    dispatch(closeModal(LOGIN_MODAL))
    dispatch(openModal(FORGOT_PASSWORD_MODAL))
    alert("You will remember it, don't worry!")
  },
  onSignUpClick: () => {
    dispatch(closeModal(LOGIN_MODAL))
    dispatch(openModal(SIGN_UP_MODAL))
  },
  onSubmit: (values) => {
    dispatch(signIn(values))
  },
})

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnectedLoginForm)

export default LoginContainer
