import { reduxForm } from 'redux-form'

import LoginForm from 'client/components/sessions/LoginForm'
import { LOGIN_FORM } from 'client/constants/forms'

/*
 * Is responsible for connecting a LoginForm to ReduxForm.
 */
const ConnectedLoginForm = reduxForm({
  form: LOGIN_FORM,
})(LoginForm)

export default ConnectedLoginForm
