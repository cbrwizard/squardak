import React from 'react'

import ConfirmLogoutContainer from 'client/containers/sessions/ConfirmLogoutContainer'
import ReduxModal from 'client/containers/lib/ReduxModal'
import { CONFIRM_LOGOUT_MODAL } from 'client/constants/modals'

export const ConfirmLogout = () => <ConfirmLogoutContainer />

const ConfirmLogoutContainerModal = ReduxModal(CONFIRM_LOGOUT_MODAL)(ConfirmLogout)
export default ConfirmLogoutContainerModal
