import { connect } from 'react-redux'

import AdvicePanel from 'client/components/game/AdvicePanel'
import getAdvicePanelMessagePath
  from 'client/selectors/getAdvicePanelMessagePath'
import getSquaresToCaptureLeft from 'client/selectors/getSquaresToCaptureLeft'

const mapStateToProps = state => ({
  squaresToCaptureLeft: getSquaresToCaptureLeft(state),
  squaresToCaptureMax: state.app.game.squaresToCaptureMax,
  textMessagePath: getAdvicePanelMessagePath(state),
})

const mapDispatchToProps = null
const AdvicePanelContainer = connect(mapStateToProps, mapDispatchToProps)(
  AdvicePanel
)

export default AdvicePanelContainer
