import { connect } from 'react-redux'

import { becomePatreon } from 'client/actions/users'
import ReturnTomorrow from 'client/components/game/ReturnTomorrow'

const mapStateToProps = state => ({
  isPatreon: state.app.session.user && state.app.session.user.isPatreon,
})

const mapDispatchToProps = dispatch => ({
  handlePatreonClick: () => {
    dispatch(becomePatreon())
  },
})
const ReturnTomorrowContainer = connect(mapStateToProps, mapDispatchToProps)(
  ReturnTomorrow
)

export default ReturnTomorrowContainer
