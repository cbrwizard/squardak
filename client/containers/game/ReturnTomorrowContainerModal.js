import React from 'react'

import ReduxModal from 'client/containers/lib/ReduxModal'
import { RETURN_TOMORROW_MODAL } from 'client/constants/modals'
import ReturnTomorrowContainer from './ReturnTomorrowContainer'

export const ReturnTomorrowModal = () => <ReturnTomorrowContainer />

const ReturnTomorrowContainerModal = ReduxModal(RETURN_TOMORROW_MODAL)(
  ReturnTomorrowModal
)
export default ReturnTomorrowContainerModal
