import { connect } from 'react-redux'

import { submitPlan } from 'client/actions/plans'
import CreatePlanButton from 'client/components/game/CreatePlanButton'
import getIsCreatePlanButtonEnabled
  from 'client/selectors/getIsCreatePlanButtonEnabled'
import getCreatePlanButtonColor from 'client/selectors/getCreatePlanButtonColor'

const mapStateToProps = state => ({
  buttonColor: getCreatePlanButtonColor(state),
  isEnabled: getIsCreatePlanButtonEnabled(state),
})

const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(submitPlan())
  },
})

const CreatePlanButtonContainer = connect(mapStateToProps, mapDispatchToProps)(
  CreatePlanButton
)

export default CreatePlanButtonContainer
