import { connect } from 'react-redux'

import SelectedInfo from 'client/components/game/SelectedInfo'
import getAnySquaresSelected from 'client/selectors/getAnySquaresSelected'

const mapStateToProps = state => ({
  anyIdsSelected: getAnySquaresSelected(state),
})

const mapDispatchToProps = null
const SelectedInfoContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(SelectedInfo)

export default SelectedInfoContainer
