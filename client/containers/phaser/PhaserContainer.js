import React from 'react'
import { array, bool, func, number, object, string } from 'prop-types'
import { Tooltip } from 'react-tippy'
import { injectIntl, intlShape } from 'react-intl'

import isProduction from 'shared/lib/isProduction'
import TooltipContent from '../../components/game/TooltipContent'

const propTypes = {
  canMakeTurnsToday: bool.isRequired,
  canSelectSquares: bool.isRequired,
  capturableSquaresPositions: array.isRequired,
  currentUser: object.isRequired,
  currentUserSquarePositions: array,
  // TODO: more properly check the type.
  Game: func.isRequired,
  hoveredSquareInfo: object.isRequired,
  intl: intlShape.isRequired,
  isGameLatestDataLoaded: bool.isRequired,
  onReady: func.isRequired,
  onSquareDeselect: func.isRequired,
  onSquareMouseOut: func.isRequired,
  onSquareMouseOver: func.isRequired,
  onSquareSelect: func.isRequired,
  parentDOMId: string.isRequired,
  positionCameraAtSquarePosition: string.isRequired,
  squaresCount: number.isRequired,
  territories: object.isRequired,
}

const defaultProps = {
  currentUserSquarePositions: [],
  parentDOMId: 'phaserRoot',
}

/**
 * Is responsible for making any Phaser Game work with React.
 * TODO: properly destroy the Phaser game on Unmount.
 * TODO: detect resize by itself or use a parent component for that, call
 * Game's handleResize on this event.
 * TODO: consider rendering not with an id, but with a .js-game-container class
 * TODO: this needs refactoring.
 */
class PhaserContainer extends React.Component {
  constructor(props) {
    super(props)
    this.state = { startedGame: null }
  }

  componentDidMount() {
    this.createGame({
      canMakeTurnsToday: this.props.canMakeTurnsToday,
      canSelectSquares: this.props.canSelectSquares,
      capturableSquaresPositions: this.props.capturableSquaresPositions,
      currentUser: this.props.currentUser,
      currentUserSquarePositions: this.props.currentUserSquarePositions,
      onReady: this.props.onReady,
      onSquareDeselect: this.props.onSquareDeselect,
      onSquareMouseOut: this.props.onSquareMouseOut,
      onSquareMouseOver: this.props.onSquareMouseOver,
      onSquareSelect: this.props.onSquareSelect,
      parentDOMId: this.props.parentDOMId,
      positionCameraAtSquarePosition: this.props.positionCameraAtSquarePosition,
      squaresCount: this.props.squaresCount,
      territories: this.props.territories,
    })
  }
  //
  // TODO: optimize this.
  // We watch for props changes and manually tell Phaser to update its
  // picsWarsState when any of the important state data changes.
  componentDidUpdate(prevProps) {
    this.checkAndHandleGameStatesChange(prevProps, [
      'canMakeTurnsToday',
      'canSelectSquares',
      'capturableSquaresPositions',
      'currentUserSquarePositions',
      'currentUser',
      'territories',
    ])
  }

  componentWillUnmount() {
    this.destroyGame()
  }

  checkAndHandleGameStatesChange(prevProps, stateKeys) {
    stateKeys.forEach(stateKey =>
      this.checkAndHandleGameStateChange(prevProps, stateKey)
    )
  }

  checkAndHandleGameStateChange(prevProps, stateKey) {
    if (this.state && this.state.startedGame) {
      if (this.props[stateKey] !== prevProps[stateKey]) {
        this.changeStartedGameState(stateKey, this.props[stateKey])
      }
    }
  }

  changeStartedGameState(key, value) {
    this.state.startedGame.changePicsWarsState(key, value)
  }

  createGame(options) {
    const { Game } = this.props

    // This shouldn't go to Redux/etc, because the Game is a function.
    const startedGame = new Game(options)
    this.setState({ startedGame })
    if (!isProduction) {
      window.picsWarsGame = startedGame
    }
  }

  destroyGame() {
    if (this.state.startedGame) {
      this.state.startedGame.destroy()
      window.picsWarsGame = null
    }
  }

  render() {
    const {
      intl,
      hoveredSquareInfo,
      isGameLatestDataLoaded,
      parentDOMId,
    } = this.props

    return (
      <Tooltip
        animation="none"
        useContext
        followCursor
        hideOnClick={false}
        theme="light"
        position="top"
        distance={25}
        html={
          <TooltipContent
            deselectableText={intl.formatMessage({ id: 'app.text.isDeselectable' })}
            capturableText={intl.formatMessage({ id: 'app.text.isCapturable' })}
            isClickableText={intl.formatMessage({ id: 'app.text.isClickable' })}
            hoverSquareLoadingText={intl.formatMessage({
              id: 'app.text.hoverSquareLoading',
            })}
            belongsToText={intl.formatMessage({ id: 'app.text.belongsTo' })}
            youText={intl.formatMessage({ id: 'app.text.you' })}
            hoverSquareNoOwnerText={intl.formatMessage({
              id: 'app.text.hoverSquareNoOwner',
            })}
            {...{ hoveredSquareInfo }}
            {...{ isGameLatestDataLoaded }}
          />
        }
      >
        <div className="h-height-full">
          <div id={parentDOMId} />
        </div>
      </Tooltip>
    )
  }
}

PhaserContainer.propTypes = propTypes
PhaserContainer.defaultProps = defaultProps

export default injectIntl(PhaserContainer)
