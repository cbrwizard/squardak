import { connect } from 'react-redux'

import {
  clearHoveredSquareInfo,
  removeSelectedSquarePosition,
  gameRendered,
  selectSquarePosition,
  setHoveredSquareInfo,
} from 'client/actions/game'
import PhaserContainer from 'client/containers/phaser/PhaserContainer'
import getCanMakeTurnsToday from 'client/selectors/getCanMakeTurnsToday'
import getCanSelectSquares from 'client/selectors/getCanSelectSquares'
import getCapturableSquaresPositions from 'client/selectors/getCapturableSquaresPositions'
import getIsWaitingForResults from 'client/selectors/getIsWaitingForResults'

const mapStateToProps = state => ({
  canMakeTurnsToday: getCanMakeTurnsToday(state),
  canSelectSquares: getCanSelectSquares(state),
  capturableSquaresPositions: getCapturableSquaresPositions(state),
  currentUser: state.app.session.user,
  currentUserSquarePositions: state.app.territories.current.squarePositions,
  hoveredSquareInfo: state.app.game.hoveredSquareInfo,
  isGameLatestDataLoaded: state.app.game.isLatestDataLoaded,
  isWaitingForResults: getIsWaitingForResults(state),
  positionCameraAtSquarePosition: state.app.game.positionCameraAtSquarePosition,
  squaresCount: state.app.game.squaresCount,
  territories: state.app.territories,
})

const mapDispatchToProps = dispatch => ({
  onReady: () => {
    dispatch(gameRendered())
  },
  onSquareDeselect: (square) => {
    dispatch(removeSelectedSquarePosition(square.picsWarsPosition))
  },
  onSquareMouseOut: () => {
    dispatch(clearHoveredSquareInfo())
  },
  onSquareMouseOver: (square) => {
    dispatch(
      setHoveredSquareInfo({
        ...square.record,
        belongsToCurrentUser: square.data.belongsToCurrentUser,
        capturable: square.data.capturable,
        dataLoaded: square.data.dataLoaded,
        selected: square.data.selected,
      })
    )
  },
  onSquareSelect: (square) => {
    dispatch(selectSquarePosition(square.picsWarsPosition))
  },
})

/**
 * Is responsible for connecting a PhaserContainer to Redux.
 */
const PhaserReduxContainer = connect(mapStateToProps, mapDispatchToProps)(
  PhaserContainer
)

export default PhaserReduxContainer
