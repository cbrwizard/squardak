import React from 'react'

import ReduxModal from 'client/containers/lib/ReduxModal'
import { IMAGE_MODAL } from 'client/constants/modals'
import ConnectedImageForm from 'client/containers/images/ConnectedImageForm'

/*
 * Is responsible for rendering the Image form container in a modal.
 */
export const ImageForm = () => <ConnectedImageForm />

const ConnectedImageFormModal = ReduxModal(IMAGE_MODAL)(ImageForm)
export default ConnectedImageFormModal
