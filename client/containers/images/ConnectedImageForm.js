/* eslint import/no-mutable-exports: 0 */

import { connect } from 'react-redux'
import { formValueSelector, reduxForm } from 'redux-form'

import ImageForm from 'client/components/images/ImageForm'
import { IMAGE_FORM } from 'client/constants/forms'
import { increaseStep } from 'client/actions/tutorial'
import { update } from '../../actions/images'
import getTutotialPositionForStep from '../../lib/getTutorialPositionForStep'

/*
 * Is responsible for connecting a ImageForm to ReduxForm.
 */
let ConnectedImageForm = reduxForm({
  enableReinitialize: true,
  form: IMAGE_FORM,
})(ImageForm)

const selector = formValueSelector(IMAGE_FORM)
const mapStateToProps = state => ({
  copiedImageUUID: selector(state, 'copiedImageUUID'),
  imageUUID: selector(state, 'imageUUID'),
  initialValues: {
    _id: state.app.session.user._id,
    copiedImageUUID: state.app.session.user.copiedImageUUID,
    copyImageFromUserNick: state.app.session.user.copyImageFromUserNick,
    imageUUID: state.app.session.user.imageUUID,
  },
  step: state.app.tutorial.step,
})

const mapDispatchToProps = dispatch => ({
  onImageChange: (currentStep) => {
    // TODO: fix this inconsistency
    if (currentStep === 11 || currentStep === 10) {
      dispatch(increaseStep(getTutotialPositionForStep(12)))
    }
  },
  onSubmit: (values) => {
    dispatch(update(values))
  },
})

ConnectedImageForm = connect(mapStateToProps, mapDispatchToProps)(
  ConnectedImageForm
)

export default ConnectedImageForm
