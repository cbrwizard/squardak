import { connect } from 'react-redux'

import { openModal } from 'client/actions/lib/modals'
import { IMAGE_MODAL } from 'client/constants/modals'
import ImageButton from 'client/components/images/ImageButton'
import { increaseStep } from 'client/actions/tutorial'
import getTutotialPositionForStep from '../../lib/getTutorialPositionForStep'

const mapStateToProps = state => ({
  step: state.app.tutorial.step,
})
const mapDispatchToProps = dispatch => ({
  onClick: (currentStep) => {
    dispatch(openModal(IMAGE_MODAL))

    // We need to let uploadcare load itself.
    setTimeout(() => {
      if (currentStep === 10) {
        dispatch(increaseStep(getTutotialPositionForStep(11)))
      }
    }, 300)
  },
})

const ImageButtonContainer = connect(mapStateToProps, mapDispatchToProps)(
  ImageButton
)

export default ImageButtonContainer
