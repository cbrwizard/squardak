import { connect } from 'react-redux'

import { fetchAll, send } from 'client/actions/messages'
import Chat from 'client/components/chat/Chat'

const mapStateToProps = state => ({
  isLoading: state.app.messages.isLoading,
  records: state.app.messages.records,
})

const mapDispatchToProps = dispatch => ({
  handleSubmit: (values) => {
    dispatch(send(values))
  },
  onLoad: () => {
    dispatch(fetchAll())
  },
})

const ChatContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Chat)

export default ChatContainer
