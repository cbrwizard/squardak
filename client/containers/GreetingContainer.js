import { connect } from 'react-redux'

import { logout } from 'client/actions/sessions'
import { fetchGameSession } from 'client/actions/game'
import { openModal } from 'client/actions/lib/modals'
import Greeting from 'client/components/greeting/Greeting'
import isMobile from 'client/lib/isMobile'
import {
  CONFIRM_LOGOUT_MODAL,
  LOGIN_MODAL,
  MOBILES_NOT_SUPPORTED_MODAL,
  SIGN_UP_MODAL,
} from 'client/constants/modals'

const mapStateToProps = state => ({
  isLoading: state.app.greeting.isLoading,
  session: state.app.session,
})

const mapDispatchToProps = dispatch => ({
  onLoginClick: () => {
    dispatch(openModal(LOGIN_MODAL))
  },
  onLogoutClick: (guest) => {
    if (guest) {
      dispatch(openModal(CONFIRM_LOGOUT_MODAL))
    } else {
      dispatch(logout())
    }
  },
  onSignUpClick: () => {
    dispatch(openModal(SIGN_UP_MODAL))
  },
  onUserSubmit: (values) => {
    if (isMobile()) {
      dispatch(openModal(MOBILES_NOT_SUPPORTED_MODAL))
    } else {
      dispatch(fetchGameSession(values))
    }
  },
  onVisitorSubmit: (values) => {
    if (isMobile()) {
      dispatch(openModal(MOBILES_NOT_SUPPORTED_MODAL))
    } else {
      // On a logged out user (=visitor) submit (=PLAY click) we want to register
      // a user with guest: true.
      dispatch(fetchGameSession({ guest: true, ...values }))
    }
  },
})

const GreetingContainer = connect(mapStateToProps, mapDispatchToProps)(Greeting)

export default GreetingContainer
