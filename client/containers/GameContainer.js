import { connect } from 'react-redux'

import Game from 'client/components/Game'

const mapStateToProps = state => ({
  isLatestDataLoaded: state.app.game.isLatestDataLoaded,
})
const mapDispatchToProps = null

/**
 * Is responsible for connecting a Game component to Redux.
 * Should handle game related stuff which is not specific only to Phaser.
 * For that check PhaserReduxContainer.
 * TODO: remove
 */
const GameContainer = connect(mapStateToProps, mapDispatchToProps)(Game)

export default GameContainer
