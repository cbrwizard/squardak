import { reduxForm } from 'redux-form'

import MessageForm from 'client/components/chat/MessageForm'
import { MESSAGE_FORM } from 'client/constants/forms'

/*
 * Is responsible for connecting a MessageForm to ReduxForm
 */
const ConnectedMessageForm = reduxForm({
  form: MESSAGE_FORM,
})(MessageForm)

export default ConnectedMessageForm
