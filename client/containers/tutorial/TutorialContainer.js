import { connect } from 'react-redux'

import { finishTutorial, increaseStep } from 'client/actions/tutorial'
import Tutorial from 'client/components/tutorial/Tutorial'
import getTutotialPositionForStep from '../../lib/getTutorialPositionForStep'

const mapStateToProps = state => ({
  left: state.app.tutorial.position.left,
  opened: state.app.tutorial.opened,
  step: state.app.tutorial.step,
  top: state.app.tutorial.position.top,
})

const mapDispatchToProps = dispatch => ({
  onNextClick: (stepToIncreaseTo) => {
    dispatch(increaseStep(getTutotialPositionForStep(stepToIncreaseTo)))
  },
  onSkipClick: () => {
    dispatch(finishTutorial())
  },
})

const TutorialContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Tutorial)

export default TutorialContainer
