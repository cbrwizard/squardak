import { connect } from 'react-redux'

import { increaseStep, resetTutorial } from 'client/actions/tutorial'
import TutorialButton from 'client/components/tutorial/TutorialButton'
import getTutotialPositionForStep from '../../lib/getTutorialPositionForStep'

const mapStateToProps = null
const mapDispatchToProps = dispatch => ({
  onClick: () => {
    dispatch(resetTutorial())
    dispatch(increaseStep(getTutotialPositionForStep(1)))
  },
})

const TutorialButtonContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(TutorialButton)

export default TutorialButtonContainer
