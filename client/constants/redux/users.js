const base = 'USERS'

export const BECOME_PATREON = `${base}/BECOME_PATREON`
// TODO: consider renaming to CREATE
export const SIGN_UP = `${base}/SIGN_UP`
export const UPDATE = `${base}/UPDATE`
export const UPDATE_USER_IN_SQUARES = `${base}/UPDATE_USER_IN_SQUARES`
