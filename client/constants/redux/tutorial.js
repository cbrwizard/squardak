const base = 'TUTORIAL'

export const CHANGE_POSITION = `${base}/CHANGE_POSITION`
export const INCREASE_STEP = `${base}/INCREASE_STEP`
export const FINISH_TUTORIAL = `${base}/FINISH_TUTORIAL`
export const RESET_TUTORIAL = `${base}/RESET_TUTORIAL`
