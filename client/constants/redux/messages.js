const base = 'MESSAGES'

export const FETCH_ALL = `${base}/FETCH_ALL`
export const SEND = `${base}/SEND`
export const SET_IS_LOADING = `${base}/SET_IS_LOADING`
export const SET_MESSAGES = `${base}/SET_MESSAGES`
