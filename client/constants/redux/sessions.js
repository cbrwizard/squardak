const base = 'SESSIONS'

export const CLEAR_USER = `${base}/CLEAR_USER`
export const FETCH_IS_LOGGED_IN = `${base}/FETCH_IS_LOGGED_IN`
export const FETCH_SESSION_DATA = `${base}/FETCH_SESSION_DATA`
// TODO: consider renaming to DELETE
export const LOG_OUT = `${base}/LOG_OUT`
export const SET_IS_LOGGED_IN = `${base}/SET_IS_LOGGED_IN`
export const SET_USER = `${base}/SET_USER`
// TODO: consider renaming to CREATE
export const SIGN_IN = `${base}/SIGN_IN`
