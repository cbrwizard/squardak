// For Modals

export const CONFIRM_LOGOUT_MODAL = 'confirmLogout'
export const FORGOT_PASSWORD_MODAL = 'forgotPassword'
export const LOGIN_MODAL = 'login'
export const IMAGE_MODAL = 'image'
export const MOBILES_NOT_SUPPORTED_MODAL = 'mobilesNotSupported'
export const RETURN_TOMORROW_MODAL = 'returnTomorrow'
export const SETTINGS_MODAL = 'settings'
export const SQUARE_SETTINGS_MODAL = 'squareSettings'
export const SIGN_UP_MODAL = 'signUp'
