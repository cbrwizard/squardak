import expect from 'expect'

import getWorldSize from 'client/phaser/lib/getWorldSize'

// Fix when learned about resolution.
xdescribe('getWorldSize', () => {
  const numberOfSquares = 9
  const squareSize = 10
  const squareBorderWidth = 1
  // const expected = (9 * 10) + (1 * 10)
  const expected = 100
  const actual = getWorldSize(numberOfSquares, squareSize, squareBorderWidth)

  expect(actual).toEqual(expected)
})
