import expect from 'expect'

import adaptSquareCoordinatesToPhaserCoordinates from 'client/phaser/lib/adaptSquareCoordinatesToPhaserCoordinates'

// TODO: fix when the MAGIC_NUMBER mystery is resolved.
xdescribe('adaptSquareCoordinatesToPhaserCoordinates', () => {
  describe('for the square 1:1', () => {
    const square = { column: 1, row: 1 }
    const squareSize = 2

    const expected = { x: 0, y: 0 }
    const actual = adaptSquareCoordinatesToPhaserCoordinates(square, squareSize)

    expect(actual).toEqual(expected)
  })

  describe('for the square 2:3', () => {
    const square = { column: 2, row: 3 }
    const squareSize = 2

    const expected = { x: 4, y: 8 }
    const actual = adaptSquareCoordinatesToPhaserCoordinates(square, squareSize)

    expect(actual).toEqual(expected)
  })
})
