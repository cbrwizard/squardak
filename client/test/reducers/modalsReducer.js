import expect from 'expect'

import { OPEN_MODAL, CLOSE_MODAL } from 'client/constants/lib/modals'
import modalsReducer from 'client/reducers/modalsReducer'

describe('modalsReducer', () => {
  describe('OPEN_MODAL', () => {
    it('returns a correct new state', () => {
      const stateBefore = { testModal: false }
      const action = {
        modalId: 'testModal',
        type: OPEN_MODAL,
      }
      const expectedStateAfter = { testModal: true }

      expect(modalsReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('CLOSE_MODAL', () => {
    it('returns a correct new state', () => {
      const stateBefore = { testModal: true }
      const action = {
        modalId: 'testModal',
        type: CLOSE_MODAL,
      }
      const expectedStateAfter = { testModal: false }

      expect(modalsReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('unknown action', () => {
    it('returns a correct new state', () => {
      const stateBefore = { testModal: true }
      const action = {
        modalId: 'testModal',
        type: 'unknown',
      }

      const expectedStateAfter = stateBefore

      expect(modalsReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })
})
