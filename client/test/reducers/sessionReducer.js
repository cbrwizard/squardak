import deepFreeze from 'deep-freeze'
import expect from 'expect'
import faker from 'faker'

import sessionReducer from 'client/reducers/sessionReducer'
import {
  CLEAR_USER,
  SET_IS_LOGGED_IN,
  SET_USER,
} from 'client/constants/redux/sessions'

describe('sessionsReducer', () => {
  describe('SET_IS_LOGGED_IN', () => {
    const stateBefore = { isLoggedIn: false }
    const action = {
      payload: true,
      type: SET_IS_LOGGED_IN,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { isLoggedIn: true }

    it('returns a correct new state', () => {
      expect(sessionReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_USER', () => {
    const stateBefore = { user: null }
    const newUser = {
      _id: faker.random.uuid(),
      nick: faker.hacker.noun(),
    }
    const action = {
      payload: newUser,
      type: SET_USER,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { user: newUser }

    it('returns a correct new state', () => {
      expect(sessionReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('CLEAR_USER', () => {
    const stateBefore = {
      user: {
        _id: faker.random.uuid(),
        nick: faker.hacker.noun(),
      }
    }
    const action = {
      type: CLEAR_USER,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { user: {} }

    it('returns a correct new state', () => {
      expect(sessionReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('unknown action', () => {
    const stateBefore = { isLoggedIn: true }
    const action = {
      type: 'unknown',
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = stateBefore

    it('returns a correct new state', () => {
      expect(sessionReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })
})
