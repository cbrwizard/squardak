import deepFreeze from 'deep-freeze'
import expect from 'expect'

import messagesReducer from 'client/reducers/messagesReducer'
import { SET_IS_LOADING, SET_MESSAGES } from 'client/constants/redux/messages'

describe('messagesReducer', () => {
  describe('SET_MESSAGES', () => {
    it('returns a correct new state', () => {
      const stateBefore = { records: [] }
      const action = {
        payload: [{ keke: 1 }],
        type: SET_MESSAGES,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = { records: [{ keke: 1 }] }

      expect(messagesReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_IS_LOADING', () => {
    const stateBefore = { isLoading: false }
    const newIsLoading = true
    const action = {
      payload: newIsLoading,
      type: SET_IS_LOADING,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { isLoading: newIsLoading }

    it('returns a correct new state', () => {
      expect(messagesReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('unknown action', () => {
    it('returns a correct new state', () => {
      const stateBefore = { messages: [] }
      const action = {
        type: 'unknown',
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = stateBefore

      expect(messagesReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })
})
