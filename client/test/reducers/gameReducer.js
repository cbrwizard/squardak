import deepFreeze from 'deep-freeze'
import expect from 'expect'
import faker from 'faker'

import gameReducer from 'client/reducers/gameReducer'
import {
  REMOVE_SELECTED_SQUARE_POSITION,
  REMOVE_ALL_SELECTED_SQUARES,
  RESET_GAME,
  CHANGE_PLANNING_STATE,
  ADD_SELECTED_SQUARE_POSITION,
  SET_CURRENT_TURNS_LEFT,
  REDUCE_CURRENT_TURNS_LEFT,
  SET_STARTED,
  SET_SQUARES_TO_CAPTURE_MAX,
  SET_HOVERED_INFO,
} from 'client/constants/redux/game'

describe('gameReducer', () => {
  describe('CHANGE_PLANNING_STATE', () => {
    const previousState = 'ready'
    const newState = 'processing'
    const stateBefore = { planningState: previousState }
    const action = {
      payload: newState,
      type: CHANGE_PLANNING_STATE,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = {
      planningState: newState,
    }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('REMOVE_SELECTED_SQUARE_POSITION', () => {
    const firstSelectedId = faker.random.uuid()
    const squareId = faker.random.uuid()
    const stateBefore = {
      selectedSquaresPositions: [firstSelectedId, squareId],
    }
    const action = {
      payload: firstSelectedId,
      type: REMOVE_SELECTED_SQUARE_POSITION,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = {
      selectedSquaresPositions: [squareId],
    }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('REMOVE_ALL_SELECTED_SQUARES', () => {
    const firstSelectedId = faker.random.uuid()
    const squareId = faker.random.uuid()
    const stateBefore = {
      selectedSquaresPositions: [firstSelectedId, squareId],
    }
    const action = {
      type: REMOVE_ALL_SELECTED_SQUARES,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = {
      selectedSquaresPositions: [],
    }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('RESET_GAME', () => {
    const firstSelectedId = faker.random.uuid()
    const squareId = faker.random.uuid()
    const stateBefore = {
      selectedSquaresPositions: [firstSelectedId, squareId],
    }
    const action = {
      type: RESET_GAME,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = {
      currentTurnsLeft: 0,
      hoveredSquareInfo: {},
      isFetchingGameSession: false,
      isLatestDataLoaded: false,
      planningState: 'ready', // 'ready' || 'processing'
      positionCameraAtSquarePosition: '',
      selectedSquaresPositions: [],
      squaresCount: 0,
      squaresToCaptureMax: 1,
      started: false,
    }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('ADD_SELECTED_SQUARE_POSITION', () => {
    const firstSelectedId = faker.random.uuid()
    const stateBefore = { selectedSquaresPositions: [firstSelectedId] }
    const squareId = faker.random.uuid()
    const action = {
      payload: squareId,
      type: ADD_SELECTED_SQUARE_POSITION,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = {
      selectedSquaresPositions: [firstSelectedId, squareId],
    }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_HOVERED_INFO', () => {
    const stateBefore = { hoveredSquareInfo: { owner: 'someone' } }
    const newHoveredInfo = { owner: 'None' }
    const action = {
      payload: newHoveredInfo,
      type: SET_HOVERED_INFO,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { hoveredSquareInfo: newHoveredInfo }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_CURRENT_TURNS_LEFT', () => {
    const stateBefore = { currentTurnsLeft: 3 }
    const newCurrentTurnsLeft = 2
    const action = {
      payload: newCurrentTurnsLeft,
      type: SET_CURRENT_TURNS_LEFT,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { currentTurnsLeft: newCurrentTurnsLeft }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('REDUCE_CURRENT_TURNS_LEFT', () => {
    const currentTurnsLeft = 3
    const stateBefore = { currentTurnsLeft }
    const reduceBy = 1
    const action = {
      payload: reduceBy,
      type: REDUCE_CURRENT_TURNS_LEFT,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { currentTurnsLeft: 2 }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_STARTED', () => {
    const stateBefore = { started: false }
    const newStarted = true
    const action = {
      payload: newStarted,
      type: SET_STARTED,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { started: newStarted }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('SET_SQUARES_TO_CAPTURE_MAX', () => {
    const stateBefore = { squaresToCaptureMax: 1 }
    const newSquaresToCaptureMax = 4
    const action = {
      payload: newSquaresToCaptureMax,
      type: SET_SQUARES_TO_CAPTURE_MAX,
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = { squaresToCaptureMax: newSquaresToCaptureMax }

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })

  describe('unknown action', () => {
    const stateBefore = true
    const action = {
      type: 'unknown',
    }
    deepFreeze(stateBefore)

    const expectedStateAfter = stateBefore

    it('returns a correct new state', () => {
      expect(gameReducer(stateBefore, action)).toEqual(expectedStateAfter)
    })
  })
})
