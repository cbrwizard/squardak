import deepFreeze from 'deep-freeze'
import expect from 'expect'

import territoriesReducer from 'client/reducers/territoriesReducer'
import {
  ADD_SQUARE_POSITIONS_TO_CURRENT,
  ADD_SQUARE_POSITIONS_TO_OTHER,
  REMOVE_SQUARE_POSITIONS_FROM_CURRENT,
  REMOVE_SQUARE_POSITIONS_FROM_OTHER,
  UPDATE_CURRENT,
  UPDATE_OTHER_BY_USER_ID,
} from 'client/constants/redux/territories'

describe('territoriesReducer', () => {
  describe('ADD_SQUARE_POSITIONS_TO_CURRENT', () => {
    it('returns a correct new state', () => {
      const oldSquarePositions = ['1-5', '6-7']
      const stateBefore = {
        current: {
          color: '#fff',
          squarePositions: oldSquarePositions,
        },
      }

      const squarePositionsToAdd = ['15-4', '1-4']
      const action = {
        payload: squarePositionsToAdd,
        type: ADD_SQUARE_POSITIONS_TO_CURRENT,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: '#fff',
          squarePositions: oldSquarePositions.concat(squarePositionsToAdd),
        },
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })

  describe('ADD_SQUARE_POSITIONS_TO_OTHER', () => {
    it('returns a correct new state', () => {
      const oldSquarePositions = ['1-5', '6-7']
      const ourUserId = '3'
      const stateBefore = {
        current: {
          color: '#fff',
          squarePositions: ['2-5', '9-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: '#444',
            squarePositions: oldSquarePositions,
            userId: ourUserId,
          },
        ],
      }

      const squarePositionsToAdd = ['15-4', '1-4']
      const action = {
        payload: { squarePositions: squarePositionsToAdd, userId: ourUserId },
        type: ADD_SQUARE_POSITIONS_TO_OTHER,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: '#fff',
          squarePositions: ['2-5', '9-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: '#444',
            squarePositions: oldSquarePositions.concat(squarePositionsToAdd),
            userId: ourUserId,
          },
        ],
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })

  describe('REMOVE_SQUARE_POSITIONS_FROM_CURRENT', () => {
    it('returns a correct new state', () => {
      const stateBefore = {
        current: {
          color: '#fff',
          squarePositions: ['1-5', '6-7', '15-4'],
        },
      }

      const squarePositionsToRemove = ['15-4', '1-5']
      const action = {
        payload: squarePositionsToRemove,
        type: REMOVE_SQUARE_POSITIONS_FROM_CURRENT,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: '#fff',
          squarePositions: ['6-7'],
        },
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })

  describe('REMOVE_SQUARE_POSITIONS_FROM_OTHER', () => {
    it('returns a correct new state', () => {
      const ourUserId = '3'
      const stateBefore = {
        current: {
          color: '#fff',
          squarePositions: ['2-5', '9-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: '#444',
            squarePositions: ['1-5', '6-7'],
            userId: ourUserId,
          },
        ],
      }

      const squarePositionsToRemove = ['1-5']
      const action = {
        payload: { squarePositions: squarePositionsToRemove, userId: ourUserId },
        type: REMOVE_SQUARE_POSITIONS_FROM_OTHER,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: '#fff',
          squarePositions: ['2-5', '9-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: '#444',
            squarePositions: ['6-7'],
            userId: ourUserId,
          },
        ],
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })

  describe('UPDATE_CURRENT', () => {
    it('returns a correct new state', () => {
      const oldColor = '#fff'
      const oldNick = 'Guy'
      const oldSquarePositions = ['1-5', '6-7']
      const stateBefore = {
        current: {
          color: oldColor,
          nick: oldNick,
          squarePositions: oldSquarePositions,
        },
      }

      const newColor = '#000'
      const action = {
        payload: { color: newColor },
        type: UPDATE_CURRENT,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: newColor,
          nick: oldNick,
          squarePositions: oldSquarePositions,
        },
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })

  describe('UPDATE_OTHER_BY_USER_ID', () => {
    it('returns a correct new state', () => {
      const ourUserId = '3'

      const stateBefore = {
        current: {
          color: '#fff',
          nick: 'Guy',
          squarePositions: ['1-5', '6-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: '#444',
            squarePositions: ['4-5', '2-7'],
            userId: ourUserId,
          },
        ],
      }

      const newColor = '#444'
      const newNick = 'Garl'
      const action = {
        payload: {
          attributes: { color: newColor, nick: newNick },
          userId: ourUserId,
        },
        type: UPDATE_OTHER_BY_USER_ID,
      }
      deepFreeze(stateBefore)

      const expectedStateAfter = {
        current: {
          color: '#fff',
          nick: 'Guy',
          squarePositions: ['1-5', '6-7'],
        },
        others: [
          {
            color: '#000',
            squarePositions: ['3-5', '1-7'],
            userId: '1',
          },
          {
            color: newColor,
            nick: newNick,
            squarePositions: ['4-5', '2-7'],
            userId: ourUserId,
          },
        ],
      }

      expect(territoriesReducer(stateBefore, action)).toEqual(
        expectedStateAfter
      )
    })
  })
})
