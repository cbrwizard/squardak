import {
  firstCapturableSquare,
  firstUsersSquare,
} from 'client/phaser/manipulators/manipulateGame'
import { SQUARE_SIZE } from '../../shared/constants/game'

/*
  Is responsible for returning a position for a tutorial tooltip for a certain step.
 */
const getTutotialPositionForStep = (step) => {
  switch (step) {
    case 1:
    case 2:
    case 3:
    case 13:
    case 15:
    case 16: {
      const coordinatesToFocusOn = firstUsersSquare().worldPosition
      return {
        left: coordinatesToFocusOn.x + SQUARE_SIZE,
        top: coordinatesToFocusOn.y + SQUARE_SIZE / 2,
      }
    }
    case 4:
    case 7: {
      const coordinatesToFocusOn = firstCapturableSquare().worldPosition
      return {
        left: coordinatesToFocusOn.x + SQUARE_SIZE,
        top: coordinatesToFocusOn.y + SQUARE_SIZE / 2,
      }
    }
    case 5:
    case 8: {
      const captureButtonElement = document.querySelector('.js-capture-button')
      const coordinatesToFocusOn = captureButtonElement.getBoundingClientRect()
      return {
        left: coordinatesToFocusOn.x + 120,
        top: coordinatesToFocusOn.y + 25,
      }
    }
    case 6:
    case 9: {
      const advicePanelElement = document.querySelector('.js-advice-panel')
      const coordinatesToFocusOn = advicePanelElement.getBoundingClientRect()
      return {
        left: coordinatesToFocusOn.x + 145,
        top: coordinatesToFocusOn.y + 25,
      }
    }
    case 10: {
      const element = document.querySelector('.js-image-button')
      const coordinatesToFocusOn = element.getBoundingClientRect()
      // Sizes are added so the arrow is right below the center of the button.
      return {
        left: coordinatesToFocusOn.x + 64,
        top: coordinatesToFocusOn.y + 22,
      }
    }
    case 11: {
      const element = document.querySelector('.uploadcare--widget__button')
      const coordinatesToFocusOn = element.getBoundingClientRect()
      return {
        left: coordinatesToFocusOn.x + 174,
        top: coordinatesToFocusOn.y + 18,
      }
    }
    case 12: {
      const element = document.querySelector('.js-image-submit-button')
      const coordinatesToFocusOn = element.getBoundingClientRect()
      return {
        left: coordinatesToFocusOn.x + 88,
        top: coordinatesToFocusOn.y + 18,
      }
    }
    case 14: {
      const element = document.querySelector('.js-settings-button')
      const coordinatesToFocusOn = element.getBoundingClientRect()
      return {
        left: coordinatesToFocusOn.x + 64,
        top: coordinatesToFocusOn.y + 22,
      }
    }
    default:
      return { left: 0, top: 0 }
  }
}

export default getTutotialPositionForStep
