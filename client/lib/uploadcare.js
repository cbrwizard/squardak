import { UPLOADCARE_CDN_PATH } from 'client/constants/lib/uploadcare'

/**
 * Is responsible for converting a url like
 * https://ucarecdn.com/e5f57312-aeef-4aa5-8809-0aae950f22f2/
 * to a UUID like
 * e5f57312-aeef-4aa5-8809-0aae950f22f2
 */
export const convertURLToUUID = url =>
  url && url.replace(UPLOADCARE_CDN_PATH, '').slice(0, -1)

/**
 * Is responsible for converting a uuid like
 * e5f57312-aeef-4aa5-8809-0aae950f22f2
 * to a url like
 * https://ucarecdn.com/e5f57312-aeef-4aa5-8809-0aae950f22f2/
 */
export const convertUUIDToURL = (url, options = {}) => {
  if (url) {
    const baseUrl = `${UPLOADCARE_CDN_PATH}/${url}/`
    if (options.resize) {
      return `${baseUrl}-/resize/${options.resize.width}x${options.resize.height}/`
    }
    return baseUrl
  }
}
