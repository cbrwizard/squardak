import store from 'client/store'
import { changePosition } from '../actions/tutorial'
import getTutotialPositionForStep from './getTutorialPositionForStep'

/**
 * Is responsible for triggering the repositioning of a tutorial window
 * when needed.
 */
const repositionTutorial = () => {
  const currentStep = store.getState().app.tutorial.step
  if (currentStep) {
    store.dispatch(changePosition(getTutotialPositionForStep(currentStep)))
  }
}

export default repositionTutorial
