/**
 * Returns a configurable function to validate file size.
 */
export const validateMaxFileSize = size => (fileInfo) => {
  if (fileInfo.size !== null && fileInfo.size > size) {
    throw new Error('fileMaximumSize')
  }
}

/**
 * Returns a configurable function to validate image dimensions.
 */
export const validateDimensions = (width, height) =>
  (fileInfo) => {
    const imageInfo = fileInfo.originalImageInfo
    if (imageInfo === null) {
      return
    }
    const heightExceeded = height && imageInfo.height > height
    if (width && imageInfo.width > width) {
      if (heightExceeded) {
        throw new Error('maxDimensions')
      } else {
        throw new Error('maxWidth')
      }
    }
    if (heightExceeded) {
      throw new Error('maxHeight')
    }
  }
