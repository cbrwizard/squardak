/* eslint camelcase: 0 */
/*
 * Is responsible for rendering the whole app.
 */
import React from 'react'
import { render as reactDomRender } from 'react-dom'
import injectTapEventPlugin from 'react-tap-event-plugin'
import javascript_time_ago from 'javascript-time-ago'
import ReactGA from 'react-ga'
// import { whyDidYouUpdate } from 'why-did-you-update'

import initializeGA from 'client/lib/initializeGA'

const isProduction = require('shared/lib/isProduction')

if (isProduction) {
  initializeGA()
} else {
  // Uncomment when debugging.
  // whyDidYouUpdate(React)
}

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin()

javascript_time_ago.locale(require('javascript-time-ago/locales/en'))
require('javascript-time-ago/intl-messageformat-global')
require('intl-messageformat/dist/locale-data/en')


/**
 * Is responsible for rendering the whole app in the window.
 * Also turns on Google Analytics.
 */
const render = (Root, store) => {
  if (isProduction) {
    ReactGA.set({ page: window.location.pathname + window.location.search })
    ReactGA.pageview(window.location.pathname + window.location.search)
  }

  return reactDomRender(
    <Root store={store} />,
    document.getElementById('root')
  )
}

export default render
