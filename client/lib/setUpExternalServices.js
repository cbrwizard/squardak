/**
 * Responsible for setting up all external services in a client root.
 */

import setUpRavenClient from 'client/lib/setUpRavenClient'

const setUpExternalServices = () => {
  setUpRavenClient()
}


export default setUpExternalServices
