##
# A dev-only dockerfile. Runs backend in the watch mode and relies on
# node_modules being in the volume. Doesn't compile frontend.

# Taken as latest
FROM debian:jessie

# Taken as latest
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 7.10.0

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# Run updates and install deps.
# These should come together to ensure that packages are up-to-date together.
RUN \
  apt-get update && apt-get install -y -q --no-install-recommends \
    apt-transport-https \
    build-essential \
    ca-certificates \
    g++ \
    gcc \
    git \
    make \
    python2.7\
    sudo\
    curl\
    wget &&\
  rm -rf /var/lib/apt/lists/* &&\
  apt-get -y autoclean

# Create a dir for nvm, might not be needed for root?
RUN mkdir /usr/local/nvm

# Set an env variable for HOME.
RUN mkdir /home/app
ENV HOME=/home/app

# Install nvm with node and npm
# https://github.com/creationix/nvm#install-script
# TODO: host this file locally.
RUN curl https://raw.githubusercontent.com/creationix/nvm/v0.31.2/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

# add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Create a directory beforehand so it's usable later.
RUN mkdir -p $HOME/squardak

# Change current directory to a project's one.
WORKDIR $HOME/squardak

# For simplicity, we just sync the installed packages from the host.

# Install pm2 so we can run our application.
RUN npm i -g pm2

# For production instead of using a volume copy all files at once.
ADD . $HOME/squardak

# Run the script to start the production server.
ENTRYPOINT ["npm", "run", "backend:dev"]
