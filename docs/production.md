# Setting up DO droplet for production use.

1. Create mongo one-click app on DO with adding a pre-saved ssh key.

2. ssh to it with `ssh root@YOUR_IP`

3. Create a `deploy` user
```
adduser deploy
```

4. Add it to sudoers
```
gpasswd -a deploy sudo
```

5. Configure access to new user: follow https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04 Option 2: Manually Install the Key.

6. Prevent root login: follow https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-14-04 Step Five � Configure SSH Daemon

7. On local run to copy keys.
```
scp ~/.ssh/id_rsa deploy@droplet-ip:~/.ssh/
scp ~/.ssh/id_rsa.pub deploy@droplet-ip:~/.ssh/
```

7. (optional) On ssh check connection
```
ssh -Tv git@bitbucket.org
```
or
```
ssh -T git@github.com
```

8. Add a swap file
```
sudo fallocate -l 4G /swapfile && sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile
```

Run this to check that a swap was applied:
```
swapon
```

9. Install some packages.
```
sudo apt-get update && sudo apt-get install -y -q --no-install-recommends redis-server nginx python2.7 make build-essential awscli
```

10. Update /etc/mongod.conf with contents of config/mongod.conf

23. Create mongo users as specified in README.md.

12. Install node
```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.5/install.sh | bash
```

13. Install specific node version
```
nvm install 8.6.0
```

14. Allow nginx to be used in ufw
```
sudo ufw allow 'Nginx HTTP'
```

15. Create a directory for apps and logs.
```
mkdir apps && mkdir logs
```

16. Inside clone project
```
git clone git@bitbucket.org:cbrwizard/squardak.git
```

17 Get the new env file from local to production. Run in local project folder.
```
scp ./config/.env deploy@46.101.197.239:~/apps/squardak/config
```

20. Install yarn
```
curl -o- -L https://yarnpkg.com/install.sh | bash
```

21. Install some global packages
```
npm i -g pm2 webpack@2.5.1 phantomjs-prebuilt lodash
```

22. Make python available
```
sudo ln -s /usr/bin/python2.7 /usr/bin/python
```

24. Restart mongo
```
sudo service mongod restart
```

18. Update /etc/nginx/nginx.conf with contents of config/nginx.example.conf, but change listen to 80.

19. Restart nginx.
```
sudo systemctl restart nginx
```

25. Comment lines in ~/.bashrc
```shell
# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac
```

26. Configure aws
Create a bucket on https://s3.console.aws.amazon.com/s3/home?region=eu-central-1# 
```
aws configure
```

17 Get the backup script from local to production. Run in local project folder.
```
scp ./cli/backup.sh deploy@46.101.224.23:~/apps/shouldibuybitcoin.today/cli/
```

27. Add this to crontab with `crontab -e`
```
0 0 * * * . /home/deploy/apps/squardak/cli.backup.sh > /home/deploy/apps/squardak/tmp/backup.log
```

28. Restore from backup (optionally)

29. Deploy from local:
```
yarn run deploy
```



# After restart:
1. Add swap again:
```
sudo swapon /swapfile
```
